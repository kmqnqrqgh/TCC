import sys

from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QDialog, QApplication, QMessageBox

from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class Login(QDialog):
    app = QApplication(sys.argv)
    login_ok = pyqtSignal()
    sinal_fechar = pyqtSignal()

    def __init__(self, controle: Controle):
        # noinspection PyArgumentList
        super(Login, self).__init__(None)
        load_qrc_ui(":/ui/login.ui", self)
        self.controle = controle

    def showEvent(self, _):
        self.txtSenha.clear()

    def closeEvent(self, _):
        self.sinal_fechar.emit()

    @pyqtSlot(name="on_btnEntrar_clicked")
    def on_btn_entrar_clicked(self):
        resp, msg = self.controle.login(self.txtUsuario.text(),
                                        self.txtSenha.text())
        if not resp:
            msgbox = QMessageBox(self)
            msgbox.critical(msgbox, "Erro", msg, QMessageBox.Ok)
        else:
            self.login_ok.emit()
            self.hide()
