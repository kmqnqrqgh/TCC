from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog, QMessageBox

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui, verificar_campos_em_branco


class AddUsuario(QDialog):
    tamanho_senha = 4
    niveis = ["Operador", "Operador especial"]

    def __init__(self, parent, controle: Controle, *_):
        super(AddUsuario, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_usuario.ui", self)
        self.btnExcluir.setVisible(False)
        self.btnConfirmar.setText("Cadastrar")
        self.setWindowTitle("Cadastrar usuário")
        self.__iniciar_combo_box()

    def __iniciar_combo_box(self):
        self.cbNivel.addItems(self.niveis)

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        if not verificar_campos_em_branco(self, self.txtNome.text(),
                                          self.txtSenha.text(),
                                          self.txtSenha2.text()):
            return
        msgbox = QMessageBox(self)
        if self.txtSenha.text() != self.txtSenha2.text():
            msgbox.critical(msgbox, "Erro", "Senha digitada incorretamente",
                            QMessageBox.Ok)
            return
        func = self.controle.cadastrar_usuario_operador_especial
        if self.cbNivel.currentText() == self.niveis[0]:
            func = self.controle.cadastrar_usuario_operador
        erro = ""
        if len(self.txtSenha.text()) < 4:
            erro = "Senha deve ter no mínimo 4 caracteres"
        if not erro:
            erro = func(self.txtNome.text(), self.txtSenha.text())
        if not erro:
            self.accept()
        else:
            msgbox.critical(msgbox, "Erro cadastrando usuário", erro,
                            QMessageBox.Ok)
