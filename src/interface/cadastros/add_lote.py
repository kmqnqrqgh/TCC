from PyQt5.QtCore import pyqtSlot, QDateTime
from PyQt5.QtWidgets import QMessageBox

from src.interface.classes_customizadas.janela_alterar_lotes import \
    AddAlterarLotes
from src.nucleo.controle import Controle


class AddLote(AddAlterarLotes):
    def __init__(self, parent, controle: Controle, *_):
        super(AddLote, self).__init__(parent, controle, *_)
        self.setWindowTitle("Cadastrar lotes")
        self.btnExcluir.setVisible(False)
        self.setModal(True)
        self.trLinhas.add_controle(self.controle)
        self.trLinhas.atualizar(True, self.linhas)
        self.atualizar_datas()

    @property
    def __datas_transicoes_epoch(self):
        return [QDateTime(i).toSecsSinceEpoch() for i in self.datas_transicoes]

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        planta = self.cbPlanta.currentText()
        if not planta:
            return
        colheita = self.deDataEspeculada.dateTime().toSecsSinceEpoch()
        if self.txtLote.text():
            erro = self.controle.cadastrar_lote(
                self.txtLote.text(), planta, self.linhas,
                self.__datas_transicoes_epoch, colheita,
                self.cbEstadosLote.currentText())
        else:
            erro = "O campo com código do lote não pode ficar em branco"
        if not erro:
            self.accept()
        else:
            msgbox = QMessageBox(self)
            msgbox.critical(msgbox, "Erro cadastrando lote", erro,
                            QMessageBox.Ok)
