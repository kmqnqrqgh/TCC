from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog, QMessageBox as Msgbox

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui, validador_numero, \
    verificar_campos_em_branco


class AddValvula(QDialog):
    def __init__(self, parent, controle: Controle, *_):
        super(AddValvula, self).__init__(parent, *_)
        load_qrc_ui(":/ui/add_valvula.ui", self)
        self.setModal(True)
        self.setWindowTitle("Cadastrar válvula")
        self.btnExcluir.setVisible(False)  # tornar visivel quando for alterar
        self.lbl_id.setVisible(False)
        self.lbl_aviso.setVisible(False)
        self.lblID.setVisible(False)
        self.controle = controle
        self.btnConfirmar.setText("Cadastrar")
        self.txtNumeroSIM.setValidator(validador_numero)
        self.atualizar()

    def atualizar(self):
        self.cbTalhao.clear()
        self.cbBomba.clear()
        talhoes = self.controle.listar_talhoes()
        bombas = self.controle.listar_bombas()
        if not talhoes or not bombas:
            self.btnConfirmar.setEnabled(False)
            return
        for talhao in talhoes:  # type: [str]
            self.cbTalhao.addItem(talhao)
        for bomba, *_ in bombas:  # type: [(str, int)]
            self.cbBomba.addItem(bomba)

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        if not verificar_campos_em_branco(self, self.txtNumeroSIM.text()):
            return
        erro = self.controle.cadastrar_valvula(
            int(self.txtNumeroSIM.text()), self.cbBomba.currentText(),
            self.cbTalhao.currentText())
        if not erro:
            self.accept()
            return
        msgbox = Msgbox(self)
        msgbox.critical(msgbox, "Erro cadastrando válvula", erro, Msgbox.Ok)
