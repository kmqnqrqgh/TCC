from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog, QMessageBox

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui, verificar_campos_em_branco


class AddPlanta(QDialog):
    num_fases = 5

    def __init__(self, parent, controle: Controle, *_):
        super(AddPlanta, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_planta.ui", self)
        self.btnExcluir.setVisible(False)
        self.btnAlterar.setVisible(False)
        self.setWindowTitle("Cadastrar planta")

    # todo: usar dados persistentes nos campos de adição de planta
    # todo: UX -> mostrar soma das porcentagens

    def __erro(self, msg):
        msgbox = QMessageBox(self)
        msgbox.critical(msgbox, "Erro cadastrando planta", msg,
                        QMessageBox.Ok)

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        if not verificar_campos_em_branco(self, self.txtNomePlanta.text()):
            return
        dados = []
        nome_planta = self.txtNomePlanta.text()
        soma_proporcoes = 0
        for fase in range(self.num_fases):
            kc: float = getattr(self, "spKc" + str(fase)).value()
            profund: int = getattr(self, "spProfundidade" + str(fase)).value()
            proporc: int = getattr(self, "spProporcao" + str(fase)).value()
            dados.append((nome_planta, fase, kc, profund, proporc))
            soma_proporcoes += proporc
        erro = self.controle.cadastrar_planta(nome_planta)
        if not erro:
            erro = self.controle.cadastrar_dados_plantas(dados)
            if not erro:
                self.accept()
                return
            self.__erro(erro)
            return
        self.__erro(erro)
