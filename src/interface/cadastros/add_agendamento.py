from PyQt5.QtCore import pyqtSlot, QTime, QDateTime
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QDialog, QMessageBox

from src.interface.valvulas import Valvulas
from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui, formato_data_qt


class AddAgendamento(QDialog):
    def __init__(self, parent, controle: Controle, *_):
        super(AddAgendamento, self).__init__(parent, *_)
        load_qrc_ui(":/ui/novo_agendamento.ui", self)
        self.setModal(True)
        self.setWindowTitle("Novo agendamento")
        self.controle = controle
        self.teInicio.setTime(QTime(19, 0))
        self.__limite_valvulas = 1000
        self.__inicializar_tabela()
        self.__valvulas = []
        self.atualizar_data_calendario()
        self.clCalendario.setMinimumDate(QDateTime().currentDateTime().date())

    def __inicializar_tabela(self):
        titulos = ["Válvula", "Numero SIM", "Talhão", "Bomba"]
        tamanhos = [(0, 80), (1, 120), (2, 120), (3, 120)]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.tbValvulas.setModel(modelo)
        for tamanho in tamanhos:
            self.tbValvulas.horizontalHeader().resizeSection(*tamanho)

    def __atualizar_limite_valvulas(self):
        bombas = [bomba for valvula, _, bomba, _
                  in self.controle.listar_valvulas()
                  if valvula in self.__valvulas]
        minimo = 1000  # nunca haveria tantas valvulas pra 1 bomba
        for nome, _, max_valvulas, *_ in self.controle.listar_bombas():
            if nome in bombas and max_valvulas < minimo:
                minimo = max_valvulas
        self.__limite_valvulas = minimo

    def __add_valvula(self, id_valvula):
        id_valvula = int(id_valvula)
        if id_valvula in self.__valvulas:
            return
        for itens in self.controle.listar_valvulas():
            if itens[0] == id_valvula:
                self.tbValvulas.model().appendRow(
                    [QStandardItem(str(i)) for i in itens])
                self.__valvulas.append(id_valvula)
                break

    @pyqtSlot(name="on_btnRemover_clicked")
    def btn_remover(self):
        indice = self.tbValvulas.currentIndex()
        if not indice.isValid():
            return
        modelo = self.tbValvulas.model()
        self.__valvulas.pop(indice.row())
        modelo.takeRow(indice.row())

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        if len(self.__valvulas) >= self.__limite_valvulas:
            msgbox = QMessageBox(self)
            msg = "Limite de válvulas por bomba atingido em uma ou mais bombas"
            msgbox.critical(msgbox, "erro", msg, QMessageBox.Ok)
            return
        janela_valvulas = Valvulas(self, self.controle, True)
        janela_valvulas.sinal_valvula_selecionada.connect(self.__add_valvula)
        janela_valvulas.show()
        self.__atualizar_limite_valvulas()

    @pyqtSlot(name="on_clCalendario_selectionChanged")
    def atualizar_data_calendario(self):
        self.lblData.setText(self.clCalendario.selectedDate().toString(
            formato_data_qt))

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        msgbox = QMessageBox(self)
        if not self.__valvulas:
            msgbox.critical(msgbox, "Aviso",
                            "Pelo menos 1 válvula deve ser selecionada",
                            QMessageBox.Ok)
            return
        inicio = QDateTime(self.clCalendario.selectedDate()).toSecsSinceEpoch()
        inicio += self.teInicio.time().msecsSinceStartOfDay() / 1000
        fim = inicio + self.teDuracao.time().msecsSinceStartOfDay() / 1000
        erro = self.controle.adicionar_agendamento(inicio, fim, self.__valvulas,
                                                   [])
        if not erro:
            self.accept()
        else:
            msgbox.critical(msgbox, "Erro adicionando agendamento", erro,
                            QMessageBox.Ok)
