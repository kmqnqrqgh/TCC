from PyQt5.QtCore import pyqtSlot, QDate
from PyQt5.QtWidgets import QMessageBox, QDialog

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui


class AddMetereologia(QDialog):
    def __init__(self, parent, controle: Controle, *_):
        super(AddMetereologia, self).__init__(parent, *_)
        load_qrc_ui(":/ui/add_metereologia.ui", self)
        self.setModal(True)
        self.setWindowTitle("Adicionar dados metereológicos")
        self.controle = controle
        self.deData.setDate(QDate().currentDate())
        # todo: limitar data com 1 semana para o passado e futuro

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        data = self.deData.dateTime().toSecsSinceEpoch()
        erro = self.controle.adicionar_metereologia(data, self.spETRef.value(),
                                                    self.spChuva.value())
        if not erro:
            self.accept()
        else:
            msgbox = QMessageBox(self)
            msgbox.critical(msgbox, "Erro adicionando dados metereológicos",
                            erro, QMessageBox.Ok)

