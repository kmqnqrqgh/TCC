from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog, QMessageBox

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui, verificar_campos_em_branco


class AddTalhao(QDialog):
    def __init__(self, parent, controle: Controle, *_):
        super(AddTalhao, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_talhao.ui", self)
        self.btnExcluir.setVisible(False)
        self.setWindowTitle("Cadastrar talhão")

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        if not verificar_campos_em_branco(self, self.txtNomeTalhao.text()):
            return
        erro = self.controle.cadastrar_talhao(self.txtNomeTalhao.text())
        if not erro:
            self.accept()
        else:
            msgbox = QMessageBox(self)
            msgbox.critical(msgbox, "Erro cadastrando talhão", erro,
                            QMessageBox.Ok)
