from PyQt5.QtCore import pyqtSlot, QObject
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QDialog

from src.interface.valvulas import Valvulas
from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui


class Linha(QObject):
    def __init__(self, num_local, vazao, cap_armz):
        super(Linha, self).__init__()
        self.numero_local = num_local
        self.vazao = vazao
        self.cap_armz = cap_armz
        self.linha = [QStandardItem(str(num_local)),
                      QStandardItem(str(vazao)),
                      QStandardItem(str(cap_armz))]


class AddLinhas(QDialog):
    def __init__(self, parent, controle: Controle, *_):
        super(AddLinhas, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_linha.ui", self)
        self.setWindowTitle("Cadastrar linhas")
        self.__iniciar_combobox_valvulas()
        self.setModal(True)
        self.linhas: [Linha] = []
        self.__inicializar_tabela()
        self.lblValvula.setVisible(False)
        self.btnAlterar.setVisible(False)

    def __inicializar_tabela(self):
        tamanhos = [(0, 100), (1, 120), (2, 220)]
        titulos = ["Numero local", "Vazão do bico",
                   "Capacidade de armazenamento"]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.tbLinhas.setModel(modelo)
        for tamanho in tamanhos:
            self.tbLinhas.horizontalHeader().resizeSection(*tamanho)

    def __iniciar_combobox_valvulas(self):
        for valvula, _, _, _ in self.controle.listar_valvulas():
            self.cbValvula.addItem(str(valvula))

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        for linha in self.linhas:
            self.controle.cadastrar_linha(int(self.cbValvula.currentText()),
                                          linha.vazao, linha.cap_armz,
                                          linha.numero_local)
            self.accept()

    def definir_valvula(self, valvula):
        self.cbValvula.setCurrentIndex(self.cbValvula.findText(valvula))

    @pyqtSlot(name="on_btnProcurar_clicked")
    def btn_procurar(self):
        janela_procurar = Valvulas(self, self.controle, True)
        janela_procurar.sinal_valvula_selecionada.connect(self.definir_valvula)
        janela_procurar.show()

    @pyqtSlot(str, name="on_cbValvula_currentTextChanged")
    def cb_valvula(self, id_valvula):
        _, _, bomba, nome_talhao = self.controle.dados_valvula(int(id_valvula))
        self.lblTalhao.setText(nome_talhao)
        self.lblBomba.setText(bomba)

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        self.linhas.append(Linha(round(self.spNumLocal.value(), 2),
                                 round(self.spVazao.value(), 2),
                                 round(self.spCapacidade.value(), 2)))
        self.spNumLocal.setValue(self.spNumLocal.value() + 1)
        self.tbLinhas.model().appendRow(self.linhas[-1].linha)

    @pyqtSlot(name="on_btnRemover_clicked")
    def btn_remover(self):
        indice = self.tbLinhas.currentIndex()
        if not indice.isValid():
            return
        modelo: QStandardItemModel = self.tbLinhas.model()
        num_local = int(modelo.item(indice.row(), 0).text())
        vazao = float(modelo.item(indice.row(), 1).text())
        cap_armz = float(modelo.item(indice.row(), 2).text())
        for linha in self.linhas:
            if linha.numero_local == num_local and linha.vazao == vazao \
                    and linha.cap_armz == cap_armz:
                self.linhas.remove(linha)
                break
        self.tbLinhas.model().takeRow(indice.row())
