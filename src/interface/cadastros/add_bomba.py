from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog, QMessageBox

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui, validador_numero, \
    verificar_campos_em_branco


class AddBomba(QDialog):
    def __init__(self, parent, controle: Controle, *_):
        super(AddBomba, self).__init__(parent, *_)
        load_qrc_ui(":/ui/add_bomba.ui", self)
        self.setModal(True)
        self.setWindowTitle("Cadastrar bomba")
        self.btnExcluir.setVisible(False)  # tornar visivel somente no AltBomba
        self.controle = controle
        self.btnConfirmar.setText("Cadastrar")
        self.txtNumeroSIM.setValidator(validador_numero)

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        hora_inicio = self.teInicio.time().msecsSinceStartOfDay() / 1000
        hora_fim = self.teFim.time().msecsSinceStartOfDay() / 1000
        delay = self.teDelay.time().msecsSinceStartOfDay() / 1000
        if hora_inicio > hora_fim:
            hora_fim += 86400  # 24 * 60 * 60 = 1 dia

        if not verificar_campos_em_branco(self, self.txtNumeroSIM.text(),
                                          self.txtNome.text()):
            return
        erro = self.controle.cadastrar_bomba(
            self.txtNome.text(), int(self.txtNumeroSIM.text()),
            self.spMaxValvulas.value(), hora_inicio, hora_fim, delay)
        if not erro:
            self.accept()
        else:
            msgbox = QMessageBox(self)
            msgbox.critical(msgbox, "Erro cadastrando bomba", erro,
                            QMessageBox.Ok)

