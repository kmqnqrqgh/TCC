from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QDialog, QMessageBox

from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class PrimeiroUso(QDialog):
    sinal_ok = pyqtSignal()

    def __init__(self, controle: Controle, *args):
        super(PrimeiroUso, self).__init__(*args)
        load_qrc_ui(":/ui/primeiro_uso.ui", self)
        self.controle = controle

    @pyqtSlot(name="on_btnDefinir_clicked")
    def on_btn_definir_clicked(self):
        if self.txtSenha.text() == self.txtSenha_2.text():
            self.sinal_ok.emit()
        if not self.controle.definir_senha_inicial(self.txtSenha.text()):
            msgbox = QMessageBox(self)
            msg = "Redefinição da senha do administrador não permitida"
            msgbox.critical(msgbox, "Erro", msg, QMessageBox.Ok)
        self.close()

    def checar(self, senha1, senha2):
        if senha1 == senha2:
            if len(senha1) >= 4:
                self.btnDefinir.setEnabled(True)
                self.lblErro.setVisible(False)
                return
            else:
                self.lblErro.setText("A senha precisa pelo menos 4 caracteres")
        else:
            self.lblErro.setText("Senha repetida incorretamente")
        self.btnDefinir.setEnabled(False)
        self.lblErro.setVisible(True)

    @pyqtSlot(str, name="on_txtSenha_textChanged")
    def on_senha_editada(self, texto):
        self.checar(texto, self.txtSenha_2.text())

    @pyqtSlot(str, name="on_txtSenha_2_textChanged")
    def on_senha2_editada(self, texto):
        self.checar(self.txtSenha.text(), texto)
