from PyQt5.QtCore import pyqtSlot, QModelIndex, pyqtSignal
from PyQt5.QtWidgets import QDialog, QListWidgetItem

from src.interface.cadastros.add_talhao import AddTalhao
from src.interface.alteracoes.alterar_talhao import AlterarTalhao
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class Talhoes(QDialog):
    sinal_talhao_alterado = pyqtSignal()

    def __init__(self, parent, controle: Controle, *_):
        super(Talhoes, self).__init__(parent, *_)
        load_qrc_ui(":/ui/talhoes.ui", self)
        self.controle = controle
        self.setWindowTitle("Talhões cadastrados")
        self.atualizar()

    def __atualizacao_interna(self):
        self.sinal_talhao_alterado.emit()
        self.atualizar()

    def atualizar(self):
        self.lstTalhoes.clear()
        talhoes = self.controle.listar_talhoes()
        if talhoes is None:
            return
        for talhao in talhoes:
            self.lstTalhoes.addItem(talhao)

    @pyqtSlot(QModelIndex, name="on_lstTalhoes_clicked")
    def lst_talhoes_item_click(self, indice):
        self.btnAlterar.setEnabled(indice.isValid())

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        janela_add_talhao = AddTalhao(self, self.controle)
        janela_add_talhao.show()
        janela_add_talhao.accepted.connect(self.__atualizacao_interna)

    def __alterar_talhao(self, nome_talhao):
        janela_alt_talhao = AlterarTalhao(self, self.controle, nome_talhao)
        janela_alt_talhao.accepted.connect(self.__atualizacao_interna)
        self.btnAlterar.setEnabled(False)
        janela_alt_talhao.show()

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice = self.lstTalhoes.currentIndex()
        if indice.isValid():
            self.__alterar_talhao(self.lstTalhoes.item(indice.row()).text())

    @pyqtSlot(QListWidgetItem, name="on_lstTalhoes_itemDoubleClicked")
    def lst_talhoes_click_duplo(self, item: QListWidgetItem):
        self.__alterar_talhao(item.text())
