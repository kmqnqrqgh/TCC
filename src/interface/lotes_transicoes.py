from typing import List

from PyQt5.QtCore import QDate, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QDialog

from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class LotesTransicoes(QDialog):
    sinal_def_transicoes = pyqtSignal(object)
    num_transicoes = 5

    def __init__(self, parent, controle: Controle, datas: List[QDate], colheita,
                 *_):
        super(LotesTransicoes, self).__init__(parent, *_)
        load_qrc_ui(":/ui/lotes_transicoes.ui", self)
        self.setModal(True)
        self.controle = controle

        # definir data minima pra primeira fase
        self.deDataEspeculada0.setMinimumDate(datas[0])

        for i in range(self.num_transicoes):
            widget = getattr(self, "deDataEspeculada" + str(i))
            widget.setDate(datas[i])

            # definir data maxima
            widget.setMaximumDate(colheita)
            if i < self.num_transicoes - 1:
                prox = getattr(self, "deDataEspeculada" + str(i + 1))

                def func_def_min_data(p):
                    # p = proxima caixa de selecao de data
                    def alt_data(data):
                        self.data_alterada(p, data)
                    return alt_data
                func_def_min_data(prox)(datas[i])
                widget.dateChanged.connect(func_def_min_data(prox))

    # noinspection PyMethodMayBeStatic
    def data_alterada(self, proximo_ctl, data: QDate):
        """
        Define a data mínima para a próxima caixa de seleção de data
        :param proximo_ctl: próxima caixa de seleção de data
        :param data: data selecionada
        :return:
        """
        proximo_ctl.setMinimumDate(data)

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        datas = []
        for i in range(self.num_transicoes):
            datas.append(getattr(self, "deDataEspeculada" + str(i)).date())
        self.sinal_def_transicoes.emit(datas)
        self.accept()
