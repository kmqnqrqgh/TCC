from datetime import datetime

from PyQt5.QtCore import pyqtSlot, pyqtSignal, QModelIndex, QDate, QDateTime
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QDialog

from src.interface.cadastros.add_lote import AddLote
from src.interface.alteracoes.alterar_lote import AlterarLote
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui, formato_data, formato_data_qt


class Lotes(QDialog):
    sinal_lote_alterado = pyqtSignal()

    titulos = ["Lote", "Planta", "Data Plantio", "Data Colheita",
               "Estado atual", "Fase", "Total de linhas"]

    @staticmethod
    def indice(texto):
        if texto in Lotes.titulos:
            return Lotes.titulos.index(texto)
        raise Exception(f"Título não definido: {texto}")

    def __init__(self, parent, controle: Controle, *_):
        super(Lotes, self).__init__(parent, *_)
        load_qrc_ui(":/ui/lotes.ui", self)
        self.controle = controle
        self.setWindowTitle("Lotes")
        self.__inicializar_tabela()
        self.filtros = {
            "Por lote": self._definir_filtrar_por_lote,
            "Por planta": self._definir_filtrar_por_planta,
            "Por data plantio": self._definir_filtrar_por_plantio,
            "Por data colheita": self._definir_filtrar_por_colheita,
            "Por estado atual": self._definir_filtrar_por_estado
        }
        self.__inicializar_combobox_tipos_filtros()
        self.atualizar()
        self.dtDe.setDate(QDate().currentDate())
        self.dtAte.setDate(QDate().currentDate().addMonths(1))

    # inicializações

    def __atualizacao_interna(self):
        self.sinal_lote_alterado.emit()
        self.atualizar()

    def __inicializar_combobox_tipos_filtros(self):
        self.cbTipoFiltro.addItems(self.filtros)

    def __inicializar_tabela(self):
        tamanhos = [(0, 120), (1, 70), (2, 100), (3, 100), (4, 100), (5, 80),
                    (6, 100)]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(self.titulos)
        self.tbLotes.setModel(modelo)
        for tamanho in tamanhos:
            self.tbLotes.horizontalHeader().resizeSection(*tamanho)

    # filtros

    def __redefinir_slot_cbfiltrar(self, slot):
        self.cbFiltrar.currentTextChanged.connect(lambda: None)
        self.cbFiltrar.currentTextChanged.disconnect()
        self.cbFiltrar.currentTextChanged.connect(slot)

    def __redefinir_slots_dateedit(self, slot):
        self.dtDe.dateChanged.connect(lambda: None)
        self.dtDe.dateChanged.disconnect()
        self.dtDe.dateChanged.connect(
            lambda date: slot(QDateTime(date).toSecsSinceEpoch(),
                              QDateTime(self.dtAte.date()).toSecsSinceEpoch()))
        self.dtAte.dateChanged.connect(lambda: None)
        self.dtAte.dateChanged.disconnect()
        self.dtAte.dateChanged.connect(
            lambda date: slot(QDateTime(self.dtDe.date()).toSecsSinceEpoch(),
                              QDateTime(date).toSecsSinceEpoch()))

    def __iter_modelo(self, indice):
        modelo = self.tbLotes.model()
        for linha in range(modelo.rowCount()):
            dado = modelo.item(linha, indice).text()
            if not dado:
                continue
            yield linha, dado

    def __definir_filtro(self, indice, slot):
        self.wdgFiltroData.setVisible(False)
        self.cbFiltrar.setVisible(True)
        self.cbFiltrar.clear()
        self.cbFiltrar.setEditable(True)
        lotes = self.controle.listar_lotes()
        if not lotes:
            return
        dados = {i[indice] for i in self.controle.listar_lotes()}
        self.cbFiltrar.addItems(dados)
        self.cbFiltrar.setCurrentText("")
        self.__redefinir_slot_cbfiltrar(slot)

    def _definir_filtrar_por_lote(self):
        self.__definir_filtro(self.indice("Lote"), self.__filtrar_lotes)
        self.__filtrar_lotes(self.cbFiltrar.currentText())

    def _definir_filtrar_por_planta(self):
        self.__definir_filtro(self.indice("Planta"), self.__filtrar_plantas)
        self.__filtrar_lotes(self.cbFiltrar.currentText())

    def _definir_filtrar_por_plantio(self):
        self.wdgFiltroData.setVisible(True)
        self.cbFiltrar.setVisible(False)
        self.__redefinir_slots_dateedit(self.__filtrar_data_plantio)
        self.__filtrar_data_plantio(
            QDateTime(self.dtDe.date()).toSecsSinceEpoch(),
            QDateTime(self.dtAte.date()).toSecsSinceEpoch()
        )

    def _definir_filtrar_por_colheita(self):
        self.wdgFiltroData.setVisible(True)
        self.cbFiltrar.setVisible(False)
        self.__redefinir_slots_dateedit(self.__filtrar_data_colheita)
        self.__filtrar_data_colheita(
            QDateTime(self.dtDe.date()).toSecsSinceEpoch(),
            QDateTime(self.dtAte.date()).toSecsSinceEpoch()
        )

    def _definir_filtrar_por_estado(self):
        self.wdgFiltroData.setVisible(False)
        self.cbFiltrar.setVisible(True)
        self.cbFiltrar.setEditable(False)
        self.cbFiltrar.clear()
        lotes = self.controle.listar_lotes()
        if not lotes:
            return
        self.cbFiltrar.addItems(self.controle.listar_estados_lote())
        self.__redefinir_slot_cbfiltrar(self.__filtrar_estados)
        self.__filtrar_estados(self.cbFiltrar.currentText())

    def __filtrar_lotes(self, texto):
        for linha, lote in self.__iter_modelo(self.indice("Lote")):
            esconder = texto not in lote
            self.tbLotes.setRowHidden(linha, esconder)

    def __filtrar_plantas(self, texto):
        for linha, planta in self.__iter_modelo(self.indice("Planta")):
            esconder = texto not in planta
            self.tbLotes.setRowHidden(linha, esconder)

    def __filtrar_data_plantio(self, inicio, fim):
        for linha, data_plantio in self.__iter_modelo(self.indice(
                "Data Plantio")):
            data_plantio = QDateTime().fromString(
                data_plantio, formato_data_qt).toSecsSinceEpoch()
            esconder = not (inicio <= data_plantio <= fim)
            self.tbLotes.setRowHidden(linha, esconder)

    def __filtrar_data_colheita(self, inicio, fim):
        for linha, data_colheita in self.__iter_modelo(self.indice(
                "Data Colheita")):
            data_plantio = QDateTime().fromString(
                data_colheita, formato_data_qt).toSecsSinceEpoch()
            esconder = not (inicio <= data_plantio <= fim)
            self.tbLotes.setRowHidden(linha, esconder)

    def __filtrar_estados(self, texto):
        for linha, estado in self.__iter_modelo(
                self.indice("Estado atual")):
            esconder = texto != estado
            self.tbLotes.setRowHidden(linha, esconder)

    # slots

    def __add_item(self, *vals):
        self.tbLotes.model().appendRow(QStandardItem(str(i)) for i in vals)
        return

    def atualizar(self):
        self.tbLotes.model().setRowCount(0)
        dados = self.controle.listar_lotes()
        self.filtros[self.cbTipoFiltro.currentText()]()
        if dados is None:
            return
        for lote, planta, plantio, colheita, estado, fase_atual, linhas in dados:
            fase_atual = fase_atual + 1 if fase_atual is not None else "-"
            plantio = datetime.fromtimestamp(plantio).strftime(formato_data)
            colheita = datetime.fromtimestamp(colheita).strftime(formato_data)
            self.__add_item(lote, planta, plantio, colheita, estado,
                            fase_atual, linhas)

    @pyqtSlot(str, name="on_cbTipoFiltro_currentIndexChanged")
    def cb_tipofiltro(self, opcao):
        self.filtros[opcao]()

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        janela_add_lote = AddLote(self, self.controle)
        janela_add_lote.accepted.connect(self.atualizar)
        janela_add_lote.show()

    def __alterar_lote(self, indice):
        lote = self.tbLotes.model().item(indice.row(), 0).text()
        janela_alterar_lote = AlterarLote(self, self.controle, lote)
        janela_alterar_lote.accepted.connect(self.__atualizacao_interna)
        janela_alterar_lote.show()

    @pyqtSlot(QModelIndex, name="on_tbLotes_doubleClicked")
    def tr_linha_click_duplo(self, indice):
        if indice.isValid():
            self.__alterar_lote(indice)

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice = self.tbLotes.currentIndex()
        self.__alterar_lote(indice)

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        self.close()

    @pyqtSlot(str, name="on_cbFiltro_currentTextChanged")
    def cb_filtro(self, texto):
        pass
