from PyQt5.QtCore import QRect, pyqtSlot, pyqtSignal, QPropertyAnimation, \
    QEasingCurve
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow

from src.interface.agendamentos import Agendamentos
from src.interface.alteracoes.alterar_usuario import AlterarUsuario
from src.interface.classes_customizadas.dialogo_backup import Backup
from src.interface.enviar_msg import EnviarMsg
from src.interface.historico_irrigacoes import HistoricoIrrigacoes
from src.interface.linhas import Linhas
from src.interface.lotes import Lotes
from src.interface.metereologia import Metereologia
from src.interface.monitoramento import Monitoramento
from src.interface.plantas import Plantas
from src.interface.talhoes import Talhoes
from src.interface.usuarios import Usuarios
from src.interface.valvulas import Valvulas
from src.nucleo.controle import Controle
from .bombas import Bombas
from .ui_util import load_qrc_ui, sobre_nos, nova_dica


class Principal(QMainWindow):
    sinal_logout = pyqtSignal()
    sinal_fechar = pyqtSignal()

    def __init__(self, controle: Controle, *args):
        super(Principal, self).__init__(*args)
        load_qrc_ui(":/ui/principal.ui", self)
        self.setWindowIcon(QIcon(":/imagens/imagens/logo.svg"))
        self.controle = controle
        self.anim_cadastro = QPropertyAnimation(
            self.widgetCadastros, b"geometry")
        self.anim_alteracao = QPropertyAnimation(
            self.widgetAlteracoes, b"geometry")
        self.definir_animacao(self.widgetCadastros, self.anim_cadastro)
        self.definir_animacao(self.widgetAlteracoes, self.anim_alteracao)
        self.widgetCadastros.setVisible(False)
        self.widgetAlteracoes.setVisible(False)

        self.dialogo_backup_salvar = Backup(self, self.controle)
        self.dialogo_backup_restaurar = Backup(self, self.controle, True)
        self.janela_bomba = Bombas(self, self.controle)
        self.janela_talhoes = Talhoes(self, self.controle)
        self.janela_valvulas = Valvulas(self, self.controle)
        self.janela_plantas = Plantas(self, self.controle)
        self.janela_usuarios = Usuarios(self, self.controle)
        self.janela_linhas = Linhas(self, self.controle)
        self.janela_lotes = Lotes(self, self.controle)
        self.janela_metereolgoia = Metereologia(self, self.controle)
        self.janela_historico = HistoricoIrrigacoes(self, self.controle)
        self.janela_agendamentos = Agendamentos(self, self.controle)
        self.janela_monitoramento = Monitoramento(self, self.controle)

        self.janela_talhoes.sinal_talhao_alterado.connect(self.atualizar_tudo)
        self.janela_valvulas.sinal_valvula_alterada.connect(self.atualizar_tudo)
        self.janela_bomba.sinal_bomba_alterada.connect(self.atualizar_tudo)
        self.janela_plantas.sinal_planta_alterada.connect(self.atualizar_tudo)
        self.janela_linhas.sinal_linha_alterada.connect(self.atualizar_tudo)
        self.janela_lotes.sinal_lote_alterado.connect(self.atualizar_tudo)
        self.janela_agendamentos.sinal_agendamento_alterado.connect(
            self.atualizar_tudo)
        self.janela_metereolgoia.sinal_metereologia_alterada.connect(
            self.atualizar_tudo)

    def atualizar_tudo(self):
        self.janela_bomba.atualizar()
        self.janela_valvulas.atualizar()
        self.janela_talhoes.atualizar()
        self.janela_linhas.atualizar()
        self.janela_lotes.atualizar()
        self.janela_metereolgoia.atualizar()
        self.janela_agendamentos.atualizar()

    # Definir titulo do banner da janela principal
    def showEvent(self, _):
        self.lblDica.setText(nova_dica())
        self.lblTitulo.setText(f"{self.controle.usuario} - Painel de controle")

    def closeEvent(self, *args, **kwargs):
        self.sinal_fechar.emit()

    def definir_animacao(self, widget, anim: QPropertyAnimation):
        parent = widget.parent()
        rect = parent.rect()
        px = rect.x()
        py = rect.y()
        ph = self.height() * 2
        anim.setEasingCurve(QEasingCurve.OutCubic)
        anim.setDuration(500)
        anim.setStartValue(QRect(px, py, 0, ph))
        anim.setEndValue(QRect(px, py, 200, ph))

    def exibir(self, btn, widget, anim, estado):
        btns = [self.btnCadastros, self.btnAlteracoes]
        widgets = [self.widgetCadastros, self.widgetAlteracoes]

        btns.remove(btn)
        widgets.remove(widget)

        for botao in btns:
            botao.setChecked(False)
        for widget_ in widgets:
            widget_.setVisible(False)

        if estado:
            widget.setVisible(True)
            anim.finished.connect(lambda: None)
            anim.finished.disconnect()
            anim.setDirection(QPropertyAnimation.Forward)
        else:
            anim.setDirection(QPropertyAnimation.Backward)
            anim.finished.connect(lambda: widget.setVisible(False))
        anim.start()

    @pyqtSlot(bool, name="on_btnCadastros_clicked")
    def btn_cadastro(self, estado):
        self.exibir(self.btnCadastros, self.widgetCadastros,
                    self.anim_cadastro, estado)

    @pyqtSlot(bool, name="on_btnAlteracoes_clicked")
    def btn_alteracoes(self, estado):
        self.exibir(self.btnAlteracoes, self.widgetAlteracoes,
                    self.anim_alteracao, estado)

    @pyqtSlot(name="on_btnCadastrarBomba_clicked")
    def btn_bombas(self):
        self.janela_bomba.atualizar()
        self.janela_bomba.show()

    @pyqtSlot(name="on_btnCadastrarTalhao_clicked")
    def btn_talhoes(self):
        self.janela_talhoes.atualizar()
        self.janela_talhoes.show()

    @pyqtSlot(name="on_btnCadastrarValvula_clicked")
    def btn_valvulas(self):
        self.janela_valvulas.atualizar()
        self.janela_valvulas.show()

    @pyqtSlot(name="on_btnCadastrarPlanta_clicked")
    def btn_plantas(self):
        self.janela_plantas.atualizar()
        self.janela_plantas.show()

    @pyqtSlot(name="on_btnCadastrarLinhas_clicked")
    def btn_linhas(self):
        self.janela_linhas.atualizar()
        self.janela_linhas.show()

    @pyqtSlot(name="on_btnCadastrarLote_clicked")
    def btn_lotes(self):
        self.janela_lotes.atualizar()
        self.janela_lotes.show()

    @pyqtSlot(name="on_btnVerTodos_clicked")
    def btn_ver_usuarios(self):
        self.janela_usuarios.atualizar()
        self.janela_usuarios.show()

    @pyqtSlot(name="on_btnLogout_clicked")
    def btn_logout(self):
        self.controle.logout()
        self.sinal_logout.emit()

    @pyqtSlot(name="on_btnHistorico_clicked")
    def btn_historico(self):
        self.janela_historico.atualizar()
        self.janela_historico.show()

    @pyqtSlot(name="on_btnAlterarSenha_clicked")
    def btn_alterar_senha(self):
        janela_alterar_senha = AlterarUsuario(self, self.controle.usuario,
                                              self.controle, True)
        janela_alterar_senha.setModal(True)
        janela_alterar_senha.show()

    @pyqtSlot(name="on_btnMetereologia_clicked")
    def metereologia(self):
        self.janela_metereolgoia.atualizar()
        self.janela_metereolgoia.show()

    @pyqtSlot(name="on_btnAgendamentos_clicked")
    def agendamentos(self):
        self.janela_agendamentos.atualizar()
        self.janela_agendamentos.show()

    @pyqtSlot(name="on_btnStatusSistema_clicked")
    def status_sistema(self):
        self.janela_monitoramento.show()

    @pyqtSlot(name="on_actionSalvar_triggered")
    def backup_salvar(self):
        self.dialogo_backup_salvar.mostrar()

    @pyqtSlot(name="on_actionRestaurar_triggered")
    def backup_restaurar(self):
        self.dialogo_backup_restaurar.mostrar()

    @pyqtSlot(name="on_actionSobre_triggered")
    def menu_sobre_nos(self):
        sobre_nos(self)

    @pyqtSlot(name="on_actionSair_triggered")
    def menu_sair(self):
        self.close()

    @pyqtSlot(name="on_actionNovoAgendamento_triggered")
    def novo_agendamento(self):
        self.janela_agendamentos.btn_adicionar()

    @pyqtSlot(name="on_actionEnviarMensagem_triggered")
    def enviar_mensagem_manual(self):
        janela_enviar_msg = EnviarMsg(self, self.controle)
        janela_enviar_msg.setModal(True)
        janela_enviar_msg.show()
