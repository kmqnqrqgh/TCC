from PyQt5.QtCore import pyqtSlot, QModelIndex, pyqtSignal
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QDialog
from datetime import datetime

from src.nucleo.controle import Controle
from src.interface.cadastros.add_bomba import AddBomba
from src.interface.alteracoes.alterar_bomba import AlterarBomba
from .ui_util import load_qrc_ui, formato_hora


class Bombas(QDialog):
    sinal_bomba_alterada = pyqtSignal()

    def __init__(self, parent, controle: Controle, *_):
        super(Bombas, self).__init__(parent, *_)
        load_qrc_ui(":/ui/bombas.ui", self)
        self.controle = controle
        self.__inicializar_tabela()
        self.atualizar()

    def __inicializar_tabela(self):
        titulos = ["Nome", "Número SIM", "Valvulas simultâneas",
                   "Horario inicial", "Horario final", "Pressurização"]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.tbBombas.setModel(modelo)

    def __add_item(self, params):
        items = [QStandardItem(str(i)) for i in params]
        self.tbBombas.model().appendRow(items)
        return

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        janela_add_bomba = AddBomba(self, self.controle)
        janela_add_bomba.show()
        janela_add_bomba.accepted.connect(self.__atualizacao_interna)

    @pyqtSlot(QModelIndex, name="on_tbBombas_clicked")
    def tb_bombas_sel_item(self, indice: QModelIndex):
        self.btnAlterar.setEnabled(indice.isValid())

    def __alterar_bomba(self, indice):
        nome = self.tbBombas.model().item(indice.row(), 0).text()
        janela_alterar_bomba = AlterarBomba(self, self.controle, nome)
        janela_alterar_bomba.accepted.connect(self.__atualizacao_interna)
        janela_alterar_bomba.show()
        self.btnAlterar.setEnabled(False)

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice: QModelIndex = self.tbBombas.currentIndex()
        if indice.isValid():
            self.__alterar_bomba(indice)

    @pyqtSlot(QModelIndex, name="on_tbBombas_doubleClicked")
    def tb_bombas_click_duplo(self, indice):
        if indice.isValid():
            self.__alterar_bomba(indice)

    def __atualizacao_interna(self):
        self.sinal_bomba_alterada.emit()
        self.atualizar()

    @staticmethod
    def __converter_hora(hora):
        return datetime.strftime(datetime.utcfromtimestamp(hora), formato_hora)

    def atualizar(self):
        self.tbBombas.model().setRowCount(0)
        bombas = self.controle.listar_bombas()
        if bombas is None:
            return
        for nome, sim, max_valvulas, hr_inicial, hr_final, delay in bombas:
            hr_inicial = self.__converter_hora(hr_inicial)
            hr_final = self.__converter_hora(hr_final)
            delay = self.__converter_hora(delay)
            self.__add_item((nome, sim, max_valvulas, hr_inicial, hr_final,
                             delay))
