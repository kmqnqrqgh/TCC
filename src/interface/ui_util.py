import os
import tempfile
import random

from PyQt5.QtCore import QFile, QRegExp, QTime
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtWidgets import QMessageBox
from PyQt5.uic import loadUi


def load_qrc_ui(res, *args):
    stream = QFile(res)
    stream.open(QFile.ReadOnly)
    try:
        loadUi(stream, *args)
    finally:
        stream.close()


validador_numero = QRegExpValidator(QRegExp(r"\d{1,13}"))

formato_data_qt = "dd/MM/yyyy"
formato_data = "%d/%m/%Y"
formato_hora = "%H:%M:%S"

_testando = False  # True durante testes obviamente. não edite isso.


def qtime(secs):
    return QTime().fromMSecsSinceStartOfDay((secs % 86400) * 1000)


def sobre_nos(parent):
    msgbox = QMessageBox(parent)
    titulo = "Sobre nós"
    texto = "TCC Irrigação Automatizada\nProjeto de conclusão de curso do 8° " \
            "semestre da turma de Ciência da Computação de 2015/1 (CC8P17)"
    msgbox.information(msgbox, titulo, texto, QMessageBox.Ok)


def verificar_campos_em_branco(parent, *campos):
    for campo in campos:
        if campo:
            continue
        titulo = "Aviso"
        texto = "Nenhum campo pode ficar em branco"
        msgbox = QMessageBox(parent)
        msgbox.warning(msgbox, titulo, texto, QMessageBox.Ok)
        return False
    return True


def arquivo_volatil(nome):
    """
    Um arquivo volatil é usado para preservar um dado de forma temporária, como
    campos de preenchimento de cadastro, para facilitar a repetição do
    preenchimento de varios dados similares
    :param nome: nome do arquivo
    :return: caminho para o arquivo temporario
    """
    return os.path.join(tempfile.gettempdir(), nome)


def msgbox_confirmar_alteracao(parent):
    if _testando:
        return True
    msgbox = QMessageBox(parent)
    titulo = "Confirmar alteração"
    return True if msgbox.warning(msgbox, titulo, titulo, QMessageBox.Cancel,
                                  QMessageBox.Ok) == QMessageBox.Ok else False


dicas = [
    "Letras sublinhadas nos botões funcionam como atalho ao apertar, por "
    "exemplo: Alt+D para mostrar cadastros.",

    "A sequência dos menus de cadastro seguem a ordem de cadastro necessária "
    "para o funcionamento do sistema",

    "Caso esteja cadastrando ou alterando um dado e não queira prosseguir, "
    "clique no 'X' da janela, ou pressione a tecla 'Esc' do teclado",

    "As janelas que permitem edição dos dados podem ter seus itens editados "
    "com 2 cliques sob o item"
]


def nova_dica():
    return random.choice(dicas)
