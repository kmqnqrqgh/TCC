from PyQt5.QtCore import pyqtSlot, pyqtSignal, QModelIndex
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import QDialog

from src.interface.cadastros.add_linha import AddLinhas
from src.interface.alteracoes.alterar_linha import AlterarLinhas
from src.interface.valvulas import Valvulas
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class Linhas(QDialog):
    sinal_linha_alterada = pyqtSignal()
    sinal_linhas_selecionadas = pyqtSignal(list)

    titulos = ["ID linha", "Numero local", "Vazão do bico",
               "Capacidade de armazenamento"]

    def __init__(self, parent, controle: Controle, selecao=False, *_):
        super(Linhas, self).__init__(parent, *_)
        load_qrc_ui(":/ui/linhas.ui", self)
        self.controle = controle
        self.trLinhas.add_controle(self.controle)
        self.setWindowTitle("Linhas cadastradas")
        self.__inicializar_tabelas()
        self.atualizar()
        self.trLinhas.selectionModel().currentChanged.connect(
            self.tr_linha_selecionada)
        self.selecao = selecao
        if selecao:
            self.btnAdicionar.setVisible(False)
            self.btnAlterar.setVisible(False)
            self.btnOK.setText("Atribuir")

    def __atualizacao_interna(self):
        self.sinal_linha_alterada.emit()
        self.atualizar()

    def __inicializar_tabela(self):
        titulos = ["Talhão/Válvula", *self.titulos]
        tamanhos = [(0, 150), (0, 100), (1, 70), (2, 120), (3, 120)]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.trLinhas.setModel(modelo)
        for tamanho in tamanhos:
            self.trLinhas.header().resizeSection(*tamanho)

    def __inicializar_tabelas(self):
        self.__inicializar_tabela()

    def atualizar_lista_talhoes(self):
        self.cbTalhao.clear()
        talhoes = self.controle.listar_talhoes()
        if talhoes is None:
            return
        for talhao in talhoes:
            self.cbTalhao.addItem(talhao)
        self.cbTalhao.setCurrentText("")

    def atualizar(self):
        self.trLinhas.atualizar()
        self.atualizar_lista_talhoes()

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        janela_add_linhas = AddLinhas(self, self.controle)
        janela_add_linhas.accepted.connect(self.__atualizacao_interna)
        janela_add_linhas.show()

    def __atualizar_dados_valvula(self, valvula):
        dados = self.controle.dados_valvula(valvula)
        if not dados:
            return
        _, num_sim, bomba, _ = dados
        self.lblValvula.setText(str(valvula))
        self.lblNumeroSIM.setText(str(num_sim))
        self.lblBomba.setText(str(bomba))

    def __alterar_linhas(self, indice):
        if not indice.isValid():
            return
        valvula = self.trLinhas.valvula_selecionada(indice)
        if not valvula:
            return
        self.btnAlterar.setEnabled(True)
        valvula = int(valvula)
        janela_alterar_linha = AlterarLinhas(self, self.controle, valvula)
        janela_alterar_linha.show()
        janela_alterar_linha.accepted.connect(self.atualizar)

    def __selecionar_linhas(self, indice):
        linhas = self.trLinhas.linhas_selecionadas(indice)
        if linhas:
            self.sinal_linhas_selecionadas.emit(linhas)
            self.accept()

    @pyqtSlot(QModelIndex, name="on_trLinhas_doubleClicked")
    def tr_linha_click_duplo(self, indice):
        if self.selecao:
            self.__selecionar_linhas(indice)
        else:
            self.__alterar_linhas(indice)

    def tr_linha_selecionada(self, indice, _):
        self.btnAlterar.setEnabled(True)
        self.__atualizar_dados_valvula(
            self.trLinhas.valvula_selecionada(indice))

    @pyqtSlot(name="on_btnProcurar_clicked")
    def btn_procurar(self):
        janela_valvulas = Valvulas(self, self.controle, True)
        janela_valvulas.sinal_valvula_selecionada.connect(
            self.trLinhas.procurar_valvula)
        janela_valvulas.show()

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        if self.selecao:
            self.__selecionar_linhas(self.trLinhas.currentIndex())
        else:
            self.accept()

    @pyqtSlot(str, name="on_cbTalhao_currentTextChanged")
    def cb_talhao(self, texto):
        modelo = self.trLinhas.model()
        for linha in range(modelo.rowCount()):
            nome_talhao = modelo.item(linha, 0).text()
            if not len(nome_talhao):
                continue
            esconder = texto not in nome_talhao
            self.trLinhas.setRowHidden(linha, QModelIndex(), esconder)

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice = self.trLinhas.currentIndex()
        self.__alterar_linhas(indice)
