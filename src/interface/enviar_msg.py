from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog, QMessageBox

from src.interface.valvulas import Valvulas
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class EnviarMsg(QDialog):
    def __init__(self, parent, controle: Controle, *_):
        super(EnviarMsg, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/enviar_msg.ui", self)
        self.setWindowTitle("Enviar comando manualmente")
        self.__get_sim = self.__get_sim_valvula
        self.__enviar_msg_bomba = False
        self.__tipos = {
            "valvula": self.__definir_combobox_valvulas,
            "bomba": self.__definir_combobox_bombas
        }
        self.__inicialiar_comboboxes()

    # todo: listar comandos da bomba

    def __get_sim_bomba(self, texto):
        sim = [sim for bomba, sim, *_ in self.controle.listar_bombas()
               if str(bomba) == texto]
        if sim:
            self.lblSIM.setText(str(sim[0]))

    def __get_sim_valvula(self, texto):
        sim = [sim for valvula, sim, _, _ in self.controle.listar_valvulas()
               if str(valvula) == texto]
        if sim:
            self.lblSIM.setText(str(sim[0]))

    def __definir_combobox_valvulas(self):
        self.__enviar_msg_bomba = False
        self.__get_sim = self.__get_sim_valvula
        self.btnProcurar.setVisible(True)
        self.cbElementos.clear()
        dados = self.controle.listar_valvulas()
        if not dados:
            return
        for valvula, _, _, _ in self.controle.listar_valvulas():
            self.cbElementos.addItem(str(valvula))

    def __definir_combobox_bombas(self):
        self.__enviar_msg_bomba = True
        self.__get_sim = self.__get_sim_bomba
        self.btnProcurar.setVisible(False)
        self.cbElementos.clear()
        dados = self.controle.listar_bombas()
        if not dados:
            return
        for nome, numero_sim, *_ in dados:
            self.cbElementos.addItem(nome)

    def __inicialiar_comboboxes(self):
        self.cbComando.addItems(list(self.controle.comandos_para_valvulas))
        self.cbTipo.addItems(list(self.__tipos))

    def __definir_valvula_selecionada(self, valvula):
        valvula = str(valvula)
        self.cbElementos.setCurrentIndex(self.cbElementos.findText(valvula))

    @pyqtSlot(name="on_btnProcurar_clicked")
    def btn_procurar(self):
        janela_valvulas = Valvulas(self, self.controle, True)
        janela_valvulas.sinal_valvula_selecionada.connect(
            self.__definir_valvula_selecionada)
        janela_valvulas.show()

    @pyqtSlot(name="on_btnEnviar_clicked")
    def btn_enviar(self):
        elemento = self.cbElementos.currentText()
        if not elemento:
            return
        erro = self.controle.enviar_mensagem(
            elemento, self.cbComando.currentText(), self.__enviar_msg_bomba)
        msgbox = QMessageBox(self)
        if not erro:
            msgbox.information(msgbox, "Status", "Enviado com sucesso",
                               QMessageBox.Ok)
            self.accept()
        else:
            msgbox.critical(msgbox, "Erro enviando comando", erro,
                            QMessageBox.Ok)

    @pyqtSlot(str, name="on_cbTipo_currentTextChanged")
    def cb_tipo(self, texto):
        self.__tipos[texto]()

    @pyqtSlot(str, name="on_cbElementos_currentTextChanged")
    def cb_valvulas(self, texto):
        self.__get_sim(texto)
