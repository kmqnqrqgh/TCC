from PyQt5.QtCore import QDateTime, QDate, pyqtSlot

from src.interface.classes_customizadas.janela_alterar_lotes \
    import AddAlterarLotes
from src.nucleo.controle import Controle


class AlterarLote(AddAlterarLotes):
    def __init__(self, parent, controle: Controle, nome_lote, *_):
        super(AlterarLote, self).__init__(parent, controle, *_)
        self.controle = controle
        self.txtLote.setText(nome_lote)
        self.txtLote.setEnabled(False)
        self.setWindowTitle(f"Alterar lote \"{nome_lote}\"")
        self.lote: str = nome_lote
        self.__inicializar_dados()
        self.avisar_ao_alterar_datas = True

    def __inicializar_dados(self):
        dados = self.controle.listar_lotes()
        if dados is None:
            return
        _, planta, data_plantio, data_colheita, _, _, _ = [
            i for i in dados if i[0] == self.lote][0]
        self.cbPlanta.setCurrentIndex(self.cbPlanta.findText(planta))
        self.planta_original = self.cbPlanta.currentIndex()

        # todo: testar melhor alteração dos dados
        data_plantio = QDateTime().fromSecsSinceEpoch(data_plantio).date()
        self.data_original_plantio = data_plantio
        self.deDataPlantio.setDate(data_plantio)

        data_colheita = QDateTime().fromSecsSinceEpoch(data_colheita).date()
        self.data_original_colheita = data_colheita
        self.deDataEspeculada.setDate(data_colheita)

        linhas = self.controle.linhas_lote(self.lote)
        self.linhas = linhas
        self.trLinhas.atualizar(True, linhas)

        self.datas_transicoes = [QDate(QDateTime().fromSecsSinceEpoch(
            data_mudanca).date())
            for fase, data_mudanca in self.controle.fases_lote(self.lote)]

    @pyqtSlot(name="on_btnExcluir_clicked")
    def btn_excluir(self):
        self.excluir("lote", False, "", lambda x: None,
                     self.controle.excluir_lote, self.lote, True)

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        planta = self.cbPlanta.currentText()
        if not planta:
            return
        colheita = self.deDataEspeculada.dateTime().toSecsSinceEpoch()
        self.alterar("lote", self.controle.alterar_lote, self.txtLote.text(),
                     planta, self.linhas, self.datas_transicoes_epoch,
                     colheita, self.cbEstadosLote.currentText())
