from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QStandardItemModel, QStandardItem

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui
from src.interface.classes_customizadas.janela_alterar import AlterarGenerico


class AlterarLinhas(AlterarGenerico):
    def __init__(self, parent, controle: Controle, valvula, *_):
        super(AlterarLinhas, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_linha.ui", self)
        self.setWindowTitle("Alterar linhas")
        self.setModal(True)
        self.btnProcurar.setVisible(False)
        self.btnAdicionar.setVisible(False)
        self.cbValvula.setVisible(False)
        self.valvula = valvula
        self.linhas_removidas: [int] = []
        self.linhas_alteradas: {int: (int, float, float, int)} = {}
        self.__inicializar_tabela()
        self.__inicializar_dados()
        _, _, self.bomba, self.talhao = self.controle.dados_valvula(valvula)
        self.lblTalhao.setText(self.talhao)
        self.lblBomba.setText(self.bomba)
        self.lblValvula.setText(str(valvula))
        self.lbl_novas_linhas.setText("Linhas")
        self.btnConfirmar.setText("Confirmar alteração das linhas")
        self.tbLinhas.selectionModel().currentChanged.connect(
            self.tb_linha_selecionada)

    def __inicializar_tabela(self):
        tamanhos = [(0, 40), (1, 100), (2, 100), (3, 220)]
        titulos = ["ID", "Numero local", "Vazão do bico",
                   "Capacidade de armazenamento"]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.tbLinhas.setModel(modelo)
        for tamanho in tamanhos:
            self.tbLinhas.horizontalHeader().resizeSection(*tamanho)

    def __inicializar_dados(self):
        dados = self.controle.listar_linhas()
        if not dados:
            return
        for linha, valvula, talhao, bomba, num_local, cap_armz, vazao in dados:
            itens = (linha, num_local, vazao, cap_armz)
            itens = [QStandardItem(str(i)) for i in itens if
                     valvula == self.valvula]
            if itens:
                self.tbLinhas.model().appendRow(itens)
        self.tbLinhas.setCurrentIndex(self.tbLinhas.model().item(0).index())

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice = self.tbLinhas.currentIndex()
        if not indice.isValid():
            return
        modelo = self.tbLinhas.model()
        num_local = self.spNumLocal.value()
        cap_armz = round(self.spCapacidade.value(), 2)
        vazao = round(self.spVazao.value(), 2)
        id_linha = int(modelo.item(indice.row()).text())
        self.linhas_alteradas[id_linha] = (self.valvula, vazao,
                                           cap_armz, num_local)
        modelo.item(indice.row(), 1).setText(str(num_local))
        modelo.item(indice.row(), 2).setText(str(vazao))
        modelo.item(indice.row(), 3).setText(str(cap_armz))

    def tb_linha_selecionada(self, indice, _):
        linha = indice.row()
        modelo = self.tbLinhas.model()
        self.spNumLocal.setValue(int(modelo.item(linha, 1).text()))
        self.spVazao.setValue(float(modelo.item(linha, 2).text()))
        self.spCapacidade.setValue(float(modelo.item(linha, 3).text()))

    def __alterar(self):
        for linha in self.linhas_alteradas:
            erro = self.controle.alterar_linha(
                *[linha, *self.linhas_alteradas[linha]])
            if erro:
                return erro
        for linha in self.linhas_removidas:
            self.controle.excluir_linha(linha)

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        self.alterar("linha(s)", self.__alterar)

    @pyqtSlot(name="on_btnRemover_clicked")
    def btn_remover(self):
        indice = self.tbLinhas.currentIndex()
        if not indice.isValid():
            return
        modelo = self.tbLinhas.model()
        linha = int(modelo.item(indice.row()).text())
        self.linhas_alteradas.pop(linha, 0)
        self.linhas_removidas.append(linha)
        modelo.takeRow(indice.row())
