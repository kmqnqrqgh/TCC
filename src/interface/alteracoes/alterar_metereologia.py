from datetime import datetime
from PyQt5.QtCore import pyqtSlot, QDateTime

from src.interface.classes_customizadas.janela_alterar import AlterarGenerico
from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui, formato_data_qt


class AlterarMetereologia(AlterarGenerico):
    def __init__(self, parent, controle: Controle, data, *_):
        super(AlterarMetereologia, self).__init__(parent, *_)
        load_qrc_ui(":/ui/add_metereologia.ui", self)
        self.setModal(True)
        data_fmt = datetime.fromtimestamp(data).strftime(formato_data_qt)
        self.setWindowTitle(f"Alterar dados metereológicos de {data_fmt}")
        self.deData.setEnabled(False)
        self.deData.setDate(QDateTime().fromSecsSinceEpoch(data).date())
        self.controle = controle
        self.data = data
        self.__inicializar_dados()

    def __inicializar_dados(self):
        dados = [(et, chuva) for data, et, chuva in
                 self.controle.listar_metereologia() if data == self.data]
        if not dados:
            return
        et, chuva = dados[0]
        self.spETRef.setValue(et)
        self.spChuva.setValue(chuva)

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        data = self.deData.dateTime().toSecsSinceEpoch()
        et_ref = self.spETRef.value()
        chuva = self.spChuva.value()
        self.alterar("dados metereológicos", self.controle.alterar_metereologia,
                     data, et_ref, chuva)
