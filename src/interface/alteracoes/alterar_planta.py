from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QMessageBox as Msgbox, QInputDialog

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui
from src.interface.classes_customizadas.janela_alterar import AlterarGenerico


class AlterarPlanta(AlterarGenerico):
    sinal_nome_alterado = pyqtSignal()

    def __init__(self, parent, controle: Controle, nome_planta, *_):
        super(AlterarPlanta, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_planta.ui", self)
        self.setModal(True)
        self.nome_planta = nome_planta
        self.__def_titulo()
        self.txtNomePlanta.setText(nome_planta)
        self.txtNomePlanta.setEnabled(False)
        self.carregar_dados()

    def __def_titulo(self):
        self.setWindowTitle(f"Alterar planta \"{self.nome_planta}\"")

    def carregar_dados(self):
        dados = [i for i in self.controle.listar_dados_plantas()
                 if i[0] == self.nome_planta]
        for fase, (nome_planta, fase, kc, profundidade,
                   proporcao) in enumerate(dados):
            if 4 < fase < 0:
                continue
            getattr(self, "spKc" + str(fase)).setValue(kc)
            getattr(self, "spProfundidade" + str(fase)).setValue(profundidade)
            getattr(self, "spProporcao" + str(fase)).setValue(proporcao)

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        # noinspection PyArgumentList
        janela = QInputDialog(self)
        janela.setInputMode(QInputDialog.TextInput)
        # noinspection PyArgumentList
        nome, ok = janela.getText(self, "Alterar nome", "Novo nome:",
                                  text=self.nome_planta)
        if ok and nome != self.nome_planta:
            erro = self.controle.alterar_nome_planta(self.nome_planta, nome)
            if not erro:
                self.nome_planta = nome
                self.txtNomePlanta.setText(nome)
                self.sinal_nome_alterado.emit()
                self.__def_titulo()
            else:
                msgbox = Msgbox(self)
                titulo = "Erro alterando nome da planta"
                msgbox.warning(msgbox, titulo, erro, Msgbox.Ok)

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        dados = []
        nome_planta = self.txtNomePlanta.text()
        for fase in range(5):
            kc: float = getattr(self, "spKc" + str(fase)).value()
            profund: int = getattr(self, "spProfundidade" + str(fase)).value()
            propor: float = getattr(self, "spProporcao" + str(fase)).value()
            dados.append((nome_planta, fase, kc, profund, propor))
        self.alterar("planta", self.controle.alterar_dados_plantas, dados)

    @pyqtSlot(name="on_btnExcluir_clicked")
    def btn_excluir(self):
        msgbox = Msgbox(self)
        titulo = "Confirmar exclusão"
        msg = f"Excluir planta \"{self.nome_planta}\"?"
        if msgbox.warning(msgbox, titulo, msg, Msgbox.No,
                          Msgbox.Yes) == Msgbox.Yes:
            n = self.controle.dependencias_planta(self.nome_planta)
            if n:
                msg = f"{n} lote(s) dependem desta planta, deseja prosseguir?"
                if msgbox.warning(msgbox, titulo, msg, Msgbox.No,
                                  Msgbox.Yes) == Msgbox.Yes:
                    self.controle.excluir_planta(self.nome_planta)
                    self.accept()
                    return
            self.controle.excluir_planta(self.nome_planta)
            self.accept()
