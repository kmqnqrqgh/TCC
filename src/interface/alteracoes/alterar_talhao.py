from PyQt5.QtCore import pyqtSlot

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui
from src.interface.classes_customizadas.janela_alterar import AlterarGenerico


class AlterarTalhao(AlterarGenerico):
    def __init__(self, parent, controle: Controle, nome_talhao, *_):
        super(AlterarTalhao, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_talhao.ui", self)
        self.txtNomeTalhao.setText(nome_talhao)
        self.setWindowTitle(f"Alterar talhão \"{nome_talhao}\"")
        self.nome_talhao = nome_talhao

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        self.alterar("talhão", self.controle.alterar_talhao, self.nome_talhao,
                     self.txtNomeTalhao.text())

    @pyqtSlot(name="on_btnExcluir_clicked")
    def btn_excluir(self):
        self.excluir("talhão", True, "válvula", self.controle.
                     dependencias_talhao, self.controle.deletar_talhao,
                     self.nome_talhao, True)
