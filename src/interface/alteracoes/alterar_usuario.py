from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMessageBox as MsgBox, QInputDialog, QLineEdit

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui
from src.interface.classes_customizadas.janela_alterar import AlterarGenerico


class AlterarUsuario(AlterarGenerico):
    niveis = ["Operador", "Operador especial"]

    def __init__(self, pai, usuario, controle: Controle, nova_senha=False, *_):
        super(AlterarUsuario, self).__init__(pai, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_usuario.ui", self)
        self.txtNome.setEnabled(False)
        self.txtNome.setText(usuario)
        self.nova_senha = nova_senha
        if not nova_senha:
            self.txtSenha.setEnabled(False)
            self.txtSenha.setText("*" * 8)
            self.txtSenha2.setEnabled(False)
            self.txtSenha2.setText("*" * 8)
            self.setWindowTitle("Alterar usuário")
        else:
            self.setWindowTitle("Alterar senha")
            self.cbNivel.setEnabled(False)
            self.btnExcluir.setVisible(False)
        self.usuario = usuario
        self.btnConfirmar.setText("Alterar")
        self.__iniciar_combo_box()

    def __iniciar_combo_box(self):
        self.cbNivel.addItems(self.niveis)

    def __alterar_senha(self):
        msgbox = MsgBox(self)
        if not self.txtSenha.text() == self.txtSenha2.text():
            msgbox.critical(msgbox, "erro", "As senhas não correspondem",
                            MsgBox.Ok)
            return
        elif len(self.txtSenha2.text()) < 4:
            msgbox.critical(msgbox, "erro", "Senha pequena demais",
                            MsgBox.Ok)
            return
        # noinspection PyArgumentList
        janela = QInputDialog(self)
        janela.setInputMode(QInputDialog.TextInput)
        # noinspection PyArgumentList
        senha, ok = janela.getText(self, "Autorizar alteração de senha",
                                   "Digite sua senha atual:",
                                   QLineEdit.Password)
        if ok:
            self.alterar("senha", self.controle.alterar_senha_usuario_atual,
                         self.txtSenha.text(), senha)

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        if self.nova_senha:
            self.__alterar_senha()
            return
        else:
            pass
        operador = False
        if self.cbNivel.currentText() == self.niveis[0]:
            operador = True

        self.alterar("usuário", self.controle.alterar_nivel_acesso,
                     self.usuario, operador)

    @pyqtSlot(name="on_btnExcluir_clicked")
    def btn_excluir(self):
        self.excluir("usuario", False, "", lambda x: 0,
                     self.controle.excluir_usuario,
                     self.usuario, False)
