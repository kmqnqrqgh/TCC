from PyQt5.QtCore import pyqtSlot

from src.nucleo.controle import Controle
from src.interface.ui_util import (load_qrc_ui, validador_numero, qtime)
from src.interface.classes_customizadas.janela_alterar import AlterarGenerico


class AlterarBomba(AlterarGenerico):
    def __init__(self, parent, controle: Controle, nome_bomba, *_):
        super(AlterarBomba, self).__init__(parent, *_)
        load_qrc_ui(":/ui/add_bomba.ui", self)
        self.setModal(True)
        self.setWindowTitle(f"Alterar bomba \"{nome_bomba}\"")
        self.nome_bomba = nome_bomba
        self.txtNome.setText(nome_bomba)
        self.btnExcluir.setVisible(True)
        self.controle = controle
        self.btnConfirmar.setText("Alterar")
        self.txtNumeroSIM.setValidator(validador_numero)
        self.__inicializar_dados()

    def __inicializar_dados(self):
        sim, max_valv, auto_inicio, auto_fim, delay = self.controle.dados_bomba(
            self.nome_bomba)
        self.txtNumeroSIM.setText(str(sim))
        self.spMaxValvulas.setValue(max_valv)
        self.teDelay.setTime(qtime(delay))
        self.teInicio.setTime(qtime(auto_inicio))
        self.teFim.setTime(qtime(auto_fim))

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_confirmar(self):
        hora_inicio = self.teInicio.time().msecsSinceStartOfDay() / 1000
        hora_fim = self.teFim.time().msecsSinceStartOfDay() / 1000
        delay = self.teDelay.time().msecsSinceStartOfDay() / 1000
        if hora_inicio > hora_fim:
            hora_fim += 86400  # 24 * 60 * 60 = 1 dia
        self.alterar("bomba", self.controle.alterar_bomba, self.nome_bomba,
                     self.txtNome.text(), int(self.txtNumeroSIM.text()),
                     self.spMaxValvulas.value(), hora_inicio, hora_fim, delay)

    @pyqtSlot(name="on_btnExcluir_clicked")
    def btn_excluir(self):
        self.excluir("bomba", True, "válvula", self.controle.dependencias_bomba,
                     self.controle.deletar_bomba, self.nome_bomba, True)
