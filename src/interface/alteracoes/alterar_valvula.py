from PyQt5.QtCore import pyqtSlot

from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui, validador_numero
from src.interface.classes_customizadas.janela_alterar import AlterarGenerico


class AlterarValvula(AlterarGenerico):
    def __init__(self, parent, controle: Controle, id_valvula, *_):
        super(AlterarValvula, self).__init__(parent, *_)
        load_qrc_ui(":/ui/add_valvula.ui", self)
        self.setModal(True)
        self.setWindowTitle(f"Alterar válvula \"{id_valvula}\"")
        self.controle = controle
        self.id_valvula = id_valvula
        self.btnConfirmar.setText("Alterar")
        self.txtNumeroSIM.setValidator(validador_numero)
        self.__popular_combo_box()
        self.__preencher_dados()

    def __popular_combo_box(self):
        self.cbTalhao.clear()
        self.cbBomba.clear()
        talhoes = self.controle.listar_talhoes()
        bombas = self.controle.listar_bombas()
        if not talhoes or not bombas:
            self.btnConfirmar.setEnabled(False)
            return
        for talhao in talhoes:  # type: [str]
            self.cbTalhao.addItem(talhao)
        for bomba, *_ in bombas:  # type: [(str, int)]
            self.cbBomba.addItem(bomba)

    def __preencher_dados(self):
        (id_valvula, numero_sim, bomba, nome_talhao) = [
            d for d in self.controle.listar_valvulas()
            if d[0] == self.id_valvula
        ][0]
        self.lblID.setText(str(id_valvula))
        self.txtNumeroSIM.setText(str(numero_sim))
        self.cbTalhao.setCurrentIndex(self.cbTalhao.findText(nome_talhao))
        self.cbBomba.setCurrentIndex(self.cbBomba.findText(bomba))

    @pyqtSlot(name="on_btnConfirmar_clicked")
    def btn_alterar(self):
        if not self.txtNumeroSIM.text():
            self.txtNumeroSIM.setText("0")

        self.alterar("válvula", self.controle.alterar_valvula, self.id_valvula,
                     int(self.txtNumeroSIM.text()), self.cbBomba.currentText(),
                     self.cbTalhao.currentText())

    @pyqtSlot(name="on_btnExcluir_clicked")
    def btn_excluir(self):
        self.excluir("válvula", True, "linha",
                     self.controle.dependencias_valvula,
                     self.controle.deletar_valvula, self.id_valvula, False)
