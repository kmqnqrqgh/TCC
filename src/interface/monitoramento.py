from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog, QWidget
from .ui_util import load_qrc_ui
from src.interface.classes_customizadas.groupbox_viz_lote import VizLote
from src.interface.classes_customizadas.flowlayout import FlowLayout


class Monitoramento(QDialog):
    def __init__(self, parent, controle, *_):
        super(Monitoramento, self).__init__(parent, *_)
        load_qrc_ui(":/ui/monitoramento.ui", self)
        self.controle = controle
        self.iniciar_layout()

    def iniciar_layout(self):
        # noinspection PyArgumentList
        widget = QWidget()
        widget.setLayout(FlowLayout(spacing=20))
        self.scrVizLotes.setWidget(widget)

    def showEvent(self, _):
        self.atualizar()

    def atualizar(self):
        self.iniciar_layout()
        for lote, *_ in self.controle.listar_lotes():
            viz_lote = VizLote(self, self.controle, lote)
            viz_lote.setMinimumWidth(400)
            self.scrVizLotes.widget().layout().addWidget(viz_lote)
        # self.scrollAreaWidgetContents.layout().addStretch()

    @pyqtSlot(name="on_btnFechar_clicked")
    def btn_fechar(self):
        self.close()
