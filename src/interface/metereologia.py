from datetime import datetime

from PyQt5.QtCore import pyqtSlot, QModelIndex, pyqtSignal, QDate, QDateTime
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QDialog

from src.interface.cadastros.add_metereolgia import AddMetereologia
from src.interface.alteracoes.alterar_metereologia import AlterarMetereologia
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui, formato_data, formato_data_qt


class Metereologia(QDialog):
    sinal_metereologia_alterada = pyqtSignal()

    def __init__(self, parent, controle: Controle, *_):
        super(Metereologia, self).__init__(parent, *_)
        load_qrc_ui(":/ui/dados_metereologicos.ui", self)
        self.controle = controle
        self.__inicializar_tabela()
        self.__itens = []
        self.atualizar()
        self.dtDe.setDate(QDate().currentDate().addMonths(-1))
        self.dtAte.setDate(QDate().currentDate().addMonths(1))

    def __inicializar_tabela(self):
        titulos = ["Data", "Evapotransp. Ref.", "Chuva (mm)"]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.tbHistorico.setModel(modelo)

    def __add_item(self, data, et_ref, chuva):
        data = QDateTime().fromSecsSinceEpoch(data).toString(formato_data_qt)
        params = data, et_ref, chuva
        items = [QStandardItem(str(i)) for i in params]
        self.tbHistorico.model().appendRow(items)

    def __filtrar(self, inicio, fim):
        modelo = self.tbHistorico.model()
        for linha in range(modelo.rowCount()):
            data = QDate().fromString(modelo.item(linha, 0).text(),
                                      formato_data_qt)
            esconder = not (inicio <= data <= fim)
            self.tbHistorico.setRowHidden(linha, esconder)

    @pyqtSlot(QDate, name="on_dtDe_dateChanged")
    def dt_data_inicio(self, data):
        self.__filtrar(data, self.dtAte.date())

    @pyqtSlot(QDate, name="on_dtAte_dateChanged")
    def dt_data_fim(self, data):
        self.__filtrar(self.dtDe.date(), data)

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        janela_metereologia = AddMetereologia(self, self.controle)
        janela_metereologia.show()
        janela_metereologia.accepted.connect(self.__atualizacao_interna)

    @pyqtSlot(QModelIndex, name="on_tbHistorico_clicked")
    def tb_historico_sel_item(self, indice: QModelIndex):
        self.btnAlterar.setEnabled(indice.isValid())

    def __alterar_metereologia(self, indice):
        data = self.__itens[indice.row()]
        # data = self.tbHistorico.model().item(indice.row(), 0).text()
        janela_metereologia = AlterarMetereologia(self, self.controle, data)
        janela_metereologia.accepted.connect(self.__atualizacao_interna)
        janela_metereologia.show()
        self.btnAlterar.setEnabled(False)

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        self.accept()

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice: QModelIndex = self.tbHistorico.currentIndex()
        if indice.isValid():
            self.__alterar_metereologia(indice)

    @pyqtSlot(QModelIndex, name="on_tbHistorico_doubleClicked")
    def tb_historico_click_duplo(self, indice):
        if indice.isValid():
            self.__alterar_metereologia(indice)

    def __atualizacao_interna(self):
        self.sinal_metereologia_alterada.emit()
        self.atualizar()

    def atualizar(self):
        self.tbHistorico.model().setRowCount(0)
        self.__itens.clear()
        dados = self.controle.listar_metereologia()
        if dados is None:
            return
        for data, et_ref, chuva in dados:
            self.__itens.append(data)
            self.__add_item(data, et_ref, chuva)
