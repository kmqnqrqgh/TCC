from PyQt5.QtCore import pyqtSlot, pyqtSignal, QModelIndex
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QDialog, QMessageBox as MsgBox

from src.interface.cadastros.add_usuario import AddUsuario
from src.interface.alteracoes.alterar_usuario import AlterarUsuario
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class Usuarios(QDialog):
    sinal_usuario_alterado = pyqtSignal()

    def __init__(self, parent, controle: Controle, *args):
        super(Usuarios, self).__init__(parent, *args)
        load_qrc_ui(":/ui/usuarios.ui", self)
        self.controle = controle
        self.__inicializar_tabela()
        self.atualizar()

    @pyqtSlot(name="on_btnNovoUsuario_clicked")
    def novo_usuario_clicked(self):
        janela_novo_usuario = AddUsuario(self, self.controle)
        janela_novo_usuario.accepted.connect(self.__atualizacao_interna)
        janela_novo_usuario.show()

    @pyqtSlot(name="on_btnAjuda_clicked")
    def ajuda_clicked(self):
        msgbox = MsgBox(self)
        msg = "Operador: Permissão somente de leitura aos dados\n" \
              "Operador especial: Permissão de leitura do sistema e acesso " \
              "de escrita para agendamentos manuais\n\n" \
              "Todos os usuários podem alterar a própria senha, o " \
              "administrador tem controle absoluto de todos os dados.\n" \
              "Não é possivel ler senhas salvas, somente redefiní-las."
        msgbox.information(msgbox, "Ajuda", msg, MsgBox.Ok)

    def __inicializar_tabela(self):
        titulos = ["Nome", "Nivel acesso"]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.tbUsuarios.setModel(modelo)

    def __atualizacao_interna(self):
        self.sinal_usuario_alterado.emit()
        self.atualizar()

    def __add_item(self, nome, nivel_acesso):
        items = [QStandardItem(nome), QStandardItem(str(nivel_acesso))]
        self.tbUsuarios.model().appendRow(items)

    def atualizar(self):
        self.tbUsuarios.model().setRowCount(0)
        usuarios = self.controle.listar_usuarios()
        if usuarios is None:
            return
        for nome, nivel_acesso in usuarios:
            self.__add_item(nome, nivel_acesso)

    @pyqtSlot(QModelIndex, name="on_tbUsuarios_clicked")
    def tb_usuarios_sel_item(self, indice: QModelIndex):
        self.btnAlterar.setEnabled(indice.isValid())

    def __alterar_usuario(self, indice):
        nome = self.tbUsuarios.model().item(indice.row(), 0).text()
        # não permitir edição do admin
        if nome == self.controle.nome_usuario_admin:
            return
        janela_alterar_usuario = AlterarUsuario(self, nome, self.controle)
        janela_alterar_usuario.accepted.connect(self.__atualizacao_interna)
        janela_alterar_usuario.show()
        self.btnAlterar.setEnabled(False)

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice: QModelIndex = self.tbUsuarios.currentIndex()
        if indice.isValid():
            self.__alterar_usuario(indice)

    @pyqtSlot(QModelIndex, name="on_tbUsuarios_doubleClicked")
    def tb_usuarios_click_duplo(self, indice):
        if indice.isValid():
            self.__alterar_usuario(indice)
