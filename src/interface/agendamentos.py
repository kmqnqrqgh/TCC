import time
from typing import Dict, List

from PyQt5.QtCore import pyqtSlot, QModelIndex, pyqtSignal
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QDialog, QMessageBox

from src.interface.cadastros.add_agendamento import AddAgendamento
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui, formato_hora, formato_data


class Agendamentos(QDialog):
    sinal_agendamento_alterado = pyqtSignal()

    def __init__(self, parent, controle: Controle, *_):
        super(Agendamentos, self).__init__(parent, *_)
        load_qrc_ui(":/ui/agendamentos.ui", self)
        self.controle = controle
        self.__inicializar_tabela()
        self.atualizar()
        self.filtros = {
            "por Componente": self._filtrar_por_componente,
            "por Data": lambda x: None,
            "por Tipo": lambda x: None
        }
        self.__inicializar_combobox()

    def __inicializar_tabela(self):
        titulos = ["ID", "Componentes", "Data", "Inicio", "Fim", "Tipo",
                   "Confirmação"]
        tamanhos = [(0, 10), (1, 140), (2, 80), (3, 70), (4, 70), (5, 100)]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.tbAgendamentos.setModel(modelo)
        self.tbAgendamentos.setColumnHidden(0, True)
        for tamanho in tamanhos:
            self.tbAgendamentos.horizontalHeader().resizeSection(*tamanho)

    def __inicializar_combobox(self):
        self.cbFiltrar.addItems(self.filtros)

    def __add_item(self, params):
        items = [QStandardItem(str(i)) for i in params]
        self.tbAgendamentos.model().appendRow(items)
        return

    # todo: fazer outros tipos de filtro
    def _filtrar_por_componente(self, texto):
        modelo = self.tbAgendamentos.model()
        for linha in range(modelo.rowCount()):
            campo = modelo.item(linha, 1).text()
            texto = texto.lower()
            if not len(campo):
                continue
            esconder = texto not in campo
            self.tbAgendamentos.setRowHidden(linha, esconder)

    @pyqtSlot(str, name="on_txtFiltrar_textEdited")
    def txt_filtrar(self, texto):
        self.filtros[self.cbFiltrar.currentText()](texto)

    @pyqtSlot(name="on_btnNovo_clicked")
    def btn_adicionar(self):
        janela_add_agendamento = AddAgendamento(self, self.controle)
        janela_add_agendamento.accepted.connect(self.__atualizacao_interna)
        janela_add_agendamento.show()

    def excluir_agendamento(self, indice):
        msgbox = QMessageBox(self)
        resp = msgbox.warning(msgbox, "Confirmar exclusão",
                              "Deseja realmente excluir este agendamento?",
                              QMessageBox.No, QMessageBox.Yes)
        if resp == QMessageBox.No:
            return
        item = self.tbAgendamentos.model().item(indice.row(), 0).text()
        self.controle.excluir_agendamento(int(item))
        self.atualizar()

    @pyqtSlot(name="on_btnDeletar_clicked")
    def btn_alterar(self):
        indice: QModelIndex = self.tbAgendamentos.currentIndex()
        if indice.isValid():
            self.excluir_agendamento(indice)

    def __atualizacao_interna(self):
        self.sinal_agendamento_alterado.emit()
        self.atualizar()

    @pyqtSlot(name="on_btnFechar_clicked")
    def btn_fechar(self):
        self.close()

    @staticmethod
    def __fmt_hora(secs):
        # return datetime.fromtimestamp(secs).strftime(formato_hora)
        return time.strftime(formato_hora, time.localtime(secs))

    def __agrupar_dados(self, dados):
        # agendamento: [valvulas | bombas], data, inicio, fim, manual, confirm
        agendamentos: Dict[int: List[str], int, int, int, str, str] = {}

        for id_agendamento, valvula, bomba, hora_inicio, hora_fim, \
                manual, confirmado in dados:
            if id_agendamento in agendamentos:
                if valvula is not None:
                    agendamentos[id_agendamento][0].append(valvula)
                if bomba is not None:
                    agendamentos[id_agendamento][0].append(bomba)
            else:
                lst = [valvula] if valvula is not None else []
                if bomba:
                    lst.append(bomba)
                data = time.strftime(formato_data, time.localtime(hora_inicio))
                hora_fim = self.__fmt_hora(hora_fim)
                hora_inicio = self.__fmt_hora(hora_inicio)
                manual = "Manual" if manual else "Automático"
                confirmado = "Confirmado" if confirmado else "Não confirmado"

                agendamentos[id_agendamento] = (lst, data, hora_inicio,
                                                hora_fim, manual, confirmado)
        for i in agendamentos:
            itens, *dados = agendamentos[i]
            txt = "Bomba(s): " if isinstance(itens[0], str) else "Valvula(s): "
            txt = f"{txt} {'; '.join(str(i) for i in itens)}"
            yield [QStandardItem(str(j)) for j in [i, txt, *dados]]

    def atualizar(self):
        self.tbAgendamentos.model().setRowCount(0)
        dados = self.controle.listar_agendamentos()
        if dados is None:
            return
        for itens in self.__agrupar_dados(dados):
            self.tbAgendamentos.model().appendRow(itens)
