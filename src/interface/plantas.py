from PyQt5.QtCore import pyqtSlot, QModelIndex, pyqtSignal
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QDialog

from src.interface.cadastros.add_planta import AddPlanta
from src.interface.alteracoes.alterar_planta import AlterarPlanta
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class Plantas(QDialog):
    sinal_planta_alterada = pyqtSignal()
    sinal_planta_selecionada = pyqtSignal(str)
    opcao_pesquisa_diferenciar_maiusculas = False

    def __init__(self, parent, controle: Controle, selecao=False, *_):
        super(Plantas, self).__init__(parent, *_)
        load_qrc_ui(":/ui/plantas.ui", self)
        self.controle = controle
        self.setWindowTitle("Plantas cadastradas")
        if selecao:
            self.btnOK.setEnabled(False)
            self.btnNovaPlanta.setVisible(False)
            self.btnAlterar.setVisible(False)
            self.setModal(True)
        self.editavel = not selecao
        self.__inicializar_arvore()
        self.atualizar()

    def __atualizacao_interna(self):
        self.sinal_planta_alterada.emit()
        self.atualizar()

    def __inicializar_arvore(self):
        titulos = ["Plantas", "Fase", "Kc", "Profundidade da raiz",
                   "Proporção da fase"]
        tamanhos = [(0, 150), (1, 80), (2, 80), (3, 140), (4, 130)]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.trPlantas.setModel(modelo)
        for tamanho in tamanhos:
            self.trPlantas.header().resizeSection(*tamanho)

    @staticmethod
    def __add_dado_planta(raiz: QStandardItem, fase, kc, profundidade,
                          proporcao):
        raiz.appendRow([
            QStandardItem(""),
            QStandardItem(str(fase + 1)),
            QStandardItem(str(kc)),
            QStandardItem(str(profundidade)),
            QStandardItem(str(proporcao) + "%")
        ])

    def atualizar(self):
        self.trPlantas.model().setRowCount(0)
        plantas = {}
        dados = self.controle.listar_dados_plantas()
        if not dados:
            return
        for planta, fase, kc, profundidade, proporcao in dados:
            if planta in plantas:
                self.__add_dado_planta(plantas[planta], fase, kc, profundidade,
                                       proporcao)
                continue
            plantas[planta] = QStandardItem(planta)
            self.__add_dado_planta(plantas[planta], fase, kc, profundidade,
                                   proporcao)
            self.trPlantas.model().appendRow(plantas[planta])

    @pyqtSlot(str, name="on_txtProcurar_textEdited")
    def txt_pesquisar(self, texto):
        modelo = self.trPlantas.model()
        for linha in range(modelo.rowCount()):
            nome_planta = modelo.item(linha, 0).text()
            if not self.opcao_pesquisa_diferenciar_maiusculas:
                nome_planta = nome_planta.lower()
                texto = texto.lower()
            if not len(nome_planta):
                continue
            esconder = texto not in nome_planta
            self.trPlantas.setRowHidden(linha, QModelIndex(), esconder)

    @pyqtSlot(QModelIndex, name="on_trPlantas_clicked")
    def lst_plantas_item_click(self, indice):
        self.btnAlterar.setEnabled(indice.isValid())
        self.btnOK.setEnabled(indice.isValid())

    @pyqtSlot(name="on_btnNovaPlanta_clicked")
    def btn_nova_planta(self):
        janela_add_planta = AddPlanta(self, self.controle)
        janela_add_planta.show()
        janela_add_planta.accepted.connect(self.__atualizacao_interna)

    def __planta_selecionada(self, indice):
        model = self.trPlantas.model()
        item = model.itemFromIndex(indice)
        if item.parent():
            item = item.parent()
        return model.item(item.row(), 0).text()

    def __alterar_planta(self, indice):
        janela_alt_talhao = AlterarPlanta(self, self.controle,
                                          self.__planta_selecionada(indice))
        janela_alt_talhao.accepted.connect(self.__atualizacao_interna)
        janela_alt_talhao.sinal_nome_alterado.connect(
            self.__atualizacao_interna)
        self.btnAlterar.setEnabled(False)
        janela_alt_talhao.show()

    def __selecionar(self, indice):
        self.btnAlterar.setEnabled(False)
        if self.editavel or not indice.isValid():
            self.close()
            return
        self.sinal_planta_selecionada.emit(self.__planta_selecionada(indice))
        self.accept()

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice = self.trPlantas.currentIndex()
        if indice.isValid():
            self.__alterar_planta(indice)

    @pyqtSlot(QModelIndex, name="on_trPlantas_doubleClicked")
    def tr_plantas_click_duplo(self, indice: QModelIndex):
        if self.editavel:
            self.__alterar_planta(indice)
        else:
            self.__selecionar(indice)

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        self.__selecionar(self.trPlantas.currentIndex())
