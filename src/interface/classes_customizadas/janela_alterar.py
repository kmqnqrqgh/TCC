from types import FunctionType

from PyQt5.QtWidgets import QDialog, QMessageBox

from src.interface.ui_util import msgbox_confirmar_alteracao


class AlterarGenerico(QDialog):
    """
    Base para janelas de alteração que possuem métodos de exclusão e alteração
    """

    def __erro(self, obj, func, alt=True, *args, **kwargs):
        str_ = "alterando" if alt else "excluindo"
        msg_erro = func(*args, **kwargs)
        if not msg_erro:
            self.accept()
        else:
            msgbox = QMessageBox(self)
            msgbox.critical(msgbox, f"Erro {str_} {obj}", msg_erro,
                            QMessageBox.Ok)

    def alterar(self, obj: str, func: FunctionType, *args, **kwargs):
        """
        Exibe um erro com o resultado da função func
        :param obj: nome do tipo do objeto
        :param func: função a ser chamada
        :param args: argumentos pra func
        :param kwargs: argumentos pra func
        :return:
        """
        if msgbox_confirmar_alteracao(self):
            self.__erro(obj, func, True, *args, **kwargs)

    def excluir(self, obj_str: str, fem: bool, depend_obj: str,
                depend_func: FunctionType, del_func: FunctionType,
                nome_obj: str, inutil: bool):
        """
        Função genérica para exclusão de objetos, os parâmetros são usados para
        a geração adequada dos textos das janelas de confirmação e a chamada dos
        métodos responsáveis pela execução da exclusão (del_func)

        :param obj_str: nome do tipo de objeto
        :param fem: objeto com nome feminino (essa, desta, da, esse, deste, do)
        :param depend_obj: nome do tipo da dependencia do objeto
        :param depend_func: função de checagem de dependencia
        :param del_func: função de exclusão
        :param nome_obj: nome do objeto a ser excluido
        :param inutil: mensagem de tornar inutilizavel ou ser excluido
        :return:
        """
        msgbox = QMessageBox(self)
        ess_, dest_, d_ = "aaa" if fem else "eeo"
        inut = "ficarão inutilizaveis" if inutil else f"serão excluíd{d_}s"
        titulo = f"Confirmar exclusão d{d_} {obj_str} \"{nome_obj}\""
        msg = f"Deseja realmente excluir ess{ess_} {obj_str}?"
        if msgbox.warning(msgbox, titulo, msg, QMessageBox.No,
                          QMessageBox.Yes) == QMessageBox.Yes:
            n = depend_func(nome_obj)
            if n:
                msg = f"{n} {depend_obj}(s) que dependem dest{dest_} " \
                      f"{obj_str} {inut}, prosseguir?"
                if msgbox.warning(msgbox, titulo, msg, QMessageBox.No,
                                  QMessageBox.Yes) == QMessageBox.Yes:
                    self.__erro(obj_str, del_func, False, nome_obj)
            else:
                self.__erro(obj_str, del_func, False, nome_obj)
