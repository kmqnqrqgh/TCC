from PyQt5.QtCore import QModelIndex, Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QTreeView

from src.nucleo.controle import Controle


class TreeviewLinhas(QTreeView):
    """
    QTreeView customizada para reduzir a repetição de código no uso das
    janelas linha.ui e add_lote.ui
    """

    def __init__(self, parent, *_):
        super(TreeviewLinhas, self).__init__(parent)
        self.__inicializar_tabela()
        self.controle: Controle = None
        self.setSortingEnabled(True)

    def add_controle(self, controle):
        self.controle = controle

    def __inicializar_tabela(self):
        tamanhos = [(0, 150), (0, 100), (1, 70), (2, 120), (3, 120)]
        titulos = ["Talhão/Válvula", "Linha", "Numero local", "Vazão do bico",
                   "Cap. Armz."]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.setModel(modelo)
        for tamanho in tamanhos:
            self.header().resizeSection(*tamanho)

    def valvula_selecionada(self, indice: QModelIndex):
        modelo = self.model()
        item = modelo.itemFromIndex(indice.siblingAtColumn(0))
        if not item:
            return
        if item.child(0):
            # Talhao
            #   |- Valvula      <--- click
            #   |     |-Linha
            return item.text() if item.parent() else None
        else:
            # Talhao
            #   |- Valvula
            #   |     |-Linha   <--- click
            if not item.parent():
                return
            return item.parent().text() if item.parent().parent() else None

    def linhas_selecionadas(self, indice: QModelIndex):
        modelo = self.model()
        item = modelo.itemFromIndex(indice.siblingAtColumn(0))
        if not item:
            return
        if item.child(0):
            if not item.parent():
                # Talhao        <--- click
                #   |- Valvula
                #   |     |-Linha
                talhao_selecionado = item.text()
                return [linha for linha, _, talhao, _, _, _, _
                        in self.controle.listar_linhas()
                        if talhao == talhao_selecionado]
            # Talhao
            #   |- Valvula      <--- click
            #   |     |-Linha
            valvula_selecionada = int(item.text())
            return [linha for linha, valvula, _, _, _, _, _
                    in self.controle.listar_linhas()
                    if valvula == valvula_selecionada]
        else:
            # Talhao
            #   |- Valvula
            #   |     |-Linha   <--- click
            if not item.parent():
                return
            return [int(modelo.itemFromIndex(indice.siblingAtColumn(1))
                        .text())] if item.parent().parent() else None

    def procurar_valvula(self, valvula):
        """
        Procura e seleciona "valvula", se for encontrada
        :param valvula: valvula a procurar
        :return:
        """
        modelo = self.model()
        itens = modelo.findItems(valvula, Qt.MatchRecursive)
        itens = [i for i in itens if i.parent() is not None]  # remover raiz
        if not itens:
            return
        self.scrollTo(itens[0].index())
        self.setCurrentIndex(itens[0].index())

    def atualizar(self, filtrar=False, filtro=()):
        self.model().setRowCount(0)
        dados = self.controle.listar_linhas()
        if filtrar:
            dados = [i for i in dados if i[0] in filtro]
        if not dados:
            return
        raiz_talhao = {}
        raiz_valvula = {}
        for linha, valvula, talhao, bomba, num_local, cap_armz, vazao in dados:
            if talhao not in raiz_talhao:
                raiz_talhao[talhao] = QStandardItem(talhao)
                self.model().appendRow(raiz_talhao[talhao])
            if valvula not in raiz_valvula:
                raiz_valvula[valvula] = QStandardItem(str(valvula))
                raiz_talhao[talhao].appendRow([
                    raiz_valvula[valvula],
                    *[QStandardItem() for _ in range(4)]])  # 4 itens em branco
            itens = [linha, num_local, vazao, cap_armz]
            raiz_valvula[valvula].appendRow(
                QStandardItem(str(i)) for i in ["", *itens])
        self.expandAll()
