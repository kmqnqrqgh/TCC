from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QFileDialog
from datetime import datetime


class Backup(QObject):
    filtro = "*.bak"
    sufixo = filtro[1:]

    def __init__(self, parent, controle, restaurar=False):
        super().__init__()
        self.controle = controle
        self.dialog = QFileDialog(parent)
        self.dialog.setModal(True)
        self.dialog.setOption(QFileDialog.DontUseNativeDialog)
        self.dialog.setNameFilter(self.filtro)
        self.dialog.setDefaultSuffix(self.sufixo)
        padrao = datetime.strftime(datetime.now(), "BACKUP_%Y%m%d")
        self.dialog.selectFile(padrao)
        if restaurar:
            self.dialog.setAcceptMode(QFileDialog.AcceptOpen)
            self.dialog.fileSelected.connect(self.restaurar)
        else:
            self.dialog.setAcceptMode(QFileDialog.AcceptSave)
            self.dialog.fileSelected.connect(self.salvar)

    def mostrar(self):
        self.dialog.show()

    def salvar(self, url):
        if not url:
            return
        self.controle.salvar_backup(url)

    def restaurar(self, url):
        if not url:
            return
        self.controle.restaurar_backup(url)
