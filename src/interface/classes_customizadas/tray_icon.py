from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QSystemTrayIcon, QMenu, QAction


class IconeTray(QSystemTrayIcon):
    sinal_sair_do_sistema = pyqtSignal()
    sinal_mostrar_janela = pyqtSignal()

    def __init__(self, parent):
        super(IconeTray, self).__init__(parent)
        self.setIcon(QIcon(":/imagens/imagens/logo.svg"))
        self.setToolTip("Sistema de irrigação")
        self.__menu = QMenu()
        self.__acao_sair = QAction(QIcon(
            ":/icones/icones/icons8-cross-mark.png"), "Sair do sistema", self)
        self.__acao_abrir = QAction("Mostrar", self)
        self.__criar_menu()

    def __criar_menu(self):
        self.setContextMenu(self.__menu)
        self.__menu.addAction(self.__acao_abrir)
        self.__menu.addAction(self.__acao_sair)
        self.__acao_sair.triggered.connect(self.sinal_sair_do_sistema)
        self.__acao_abrir.triggered.connect(self.sinal_mostrar_janela)
