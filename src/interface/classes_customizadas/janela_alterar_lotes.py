from typing import List

from PyQt5.QtCore import pyqtSlot, QDateTime, QDate
from PyQt5.QtWidgets import QMessageBox

from src.interface.classes_customizadas.janela_alterar import AlterarGenerico
from src.interface.linhas import Linhas
from src.interface.lotes_transicoes import LotesTransicoes
from src.nucleo.controle import Controle
from src.interface.ui_util import load_qrc_ui


class AddAlterarLotes(AlterarGenerico):
    def __init__(self, parent, controle: Controle, *_):
        super(AddAlterarLotes, self).__init__(parent, *_)
        self.controle = controle
        load_qrc_ui(":/ui/add_lote.ui", self)
        # self.setWindowTitle("Cadastrar lotes")
        # self.btnExcluir.setVisible(False)

        # usado pra restaurar data caso usuario clique em "não" ao alterar
        # a data do calendário
        self.data_original_plantio: QDate = self.deDataPlantio.date()
        self.data_original_colheita: QDate = self.deDataEspeculada.date()
        self.planta_original: int = 0

        # evitar msgbox na inicialização
        self.avisar_ao_alterar_datas = False
        self.__ignorar_alteracao = False
        self.setModal(True)
        self.trLinhas.add_controle(self.controle)
        self.linhas: [int] = []
        self.datas_transicoes: List[QDate] = []
        self.__inicializar_combobox()
        self.atualizar()
        self.deDataPlantio.setDateTime(QDateTime().currentDateTime())
        self.deDataEspeculada.setDate(self.deDataPlantio.date().addMonths(1))
        self.deDataPlantio.dateChanged.connect(self.datedit_data_plantio)
        self.deDataEspeculada.dateChanged.connect(self.datedit_data_colheita)
        # self.avisar_ao_alterar_datas = True

    def __inicializar_combobox(self):
        estados = self.controle.listar_estados_lote()
        self.cbEstadosLote.addItems(estados)
        plantas = self.controle.listar_plantas()
        if not plantas:
            return
        self.cbPlanta.addItems(plantas)

    @property
    def datas_transicoes_epoch(self):
        return [QDateTime(i).toSecsSinceEpoch() for i in self.datas_transicoes]

    def __proporcoes_fase(self, planta):
        return [i[4] for i in self.controle.listar_dados_plantas()
                if i[0] == planta]

    @staticmethod
    def __calcular_proporcoes(data_plantacao: int, data_colheita: int,
                              proporcoes: list):
        diferenca = data_colheita - data_plantacao
        soma = 0
        resultado = []
        for proporcao in proporcoes:
            resultado.append(data_plantacao + diferenca * (soma / 100))
            soma += proporcao
        return resultado

    def __calc_datas_transicoes(self):
        proprorcoes = self.__proporcoes_fase(self.cbPlanta.currentText())
        plantio = self.deDataPlantio.dateTime().toSecsSinceEpoch()
        colheita = self.deDataEspeculada.dateTime().toSecsSinceEpoch()
        return [QDateTime().fromSecsSinceEpoch(i).date() for i in
                self.__calcular_proporcoes(plantio, colheita, proprorcoes)]

    def __inicializar_dados(self):
        pass

    def atualizar_datas(self, *_):
        self.datas_transicoes = self.__calc_datas_transicoes()

    def confirmar_alt_data(self):
        if self.__ignorar_alteracao:
            return False
        if not self.avisar_ao_alterar_datas:
            return True
        msgbox = QMessageBox(self)
        titulo = "Confirmar alteração das datas"
        texto = "Isto irá causar uma atualização nas datas de transição, " \
                "deseja continuar?"
        resposta = True if msgbox.warning(
            msgbox, titulo, texto, QMessageBox.No,
            QMessageBox.Yes) == QMessageBox.Yes else False
        self.avisar_ao_alterar_datas = not resposta
        return resposta

    def __add_linhas(self, linhas):
        self.linhas = list(set(self.linhas + linhas))
        self.atualizar()

    def atualizar(self):
        self.trLinhas.atualizar(True, self.linhas)

    def datedit_data_plantio(self, data: QDate):
        if data == self.data_original_plantio:
            return
        # workaround: mensagem de confirmação aparece duas vezes, problema do qt
        self.__ignorar_alteracao = not self.__ignorar_alteracao
        if self.confirmar_alt_data():
            self.deDataEspeculada.setMinimumDate(data)
            self.data_original_plantio = data
            self.atualizar_datas()
        else:
            self.deDataPlantio.setDate(self.data_original_plantio)

    def datedit_data_colheita(self, data: QDate):
        if data == self.data_original_colheita:
            return
        # workaround: mensagem de confirmação aparece duas vezes, problema do qt
        self.__ignorar_alteracao = not self.__ignorar_alteracao
        if self.confirmar_alt_data():
            self.data_original_colheita = data
            self.atualizar_datas()
        else:
            self.deDataEspeculada.setDate(self.data_original_colheita)

    @pyqtSlot(int, name="on_cbPlanta_currentIndexChanged")
    def cb_planta(self, indice):
        if self.planta_original == indice:
            return
        # workaround: mensagem de confirmação aparece duas vezes, problema do qt
        # self.__ignorar_alteracao = not self.__ignorar_alteracao
        if self.confirmar_alt_data():
            self.atualizar_datas()
        else:
            self.cbPlanta.setCurrentIndex(self.planta_original)

    def def_datas_transicoes(self, datas):
        self.datas_transicoes = datas
        self.avisar_ao_alterar_datas = True

    @pyqtSlot(name="on_btnAlterarTransicoes_clicked")
    def btn_altear_transicoes(self):
        if not self.datas_transicoes:
            # quando não existe uma planta na combobox, e não é possivel pegar
            # as proporções de fase
            return

        janela_alterar_transicoes = LotesTransicoes(
            self, self.controle, self.datas_transicoes,
            self.data_original_colheita)

        janela_alterar_transicoes.sinal_def_transicoes.connect(
            self.def_datas_transicoes)

        janela_alterar_transicoes.show()

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        janela_linhas = Linhas(self, self.controle, True)
        janela_linhas.sinal_linhas_selecionadas.connect(self.__add_linhas)
        janela_linhas.show()

    @pyqtSlot(name="on_btnRemover_clicked")
    def btn_remover(self):
        linhas = self.trLinhas.linhas_selecionadas(self.trLinhas.currentIndex())
        if not linhas:
            return
        self.linhas = list(set(self.linhas) - set(linhas))
        self.atualizar()
