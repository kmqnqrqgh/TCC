from PyQt5.Qt import Qt
from PyQt5.QtWidgets import QGroupBox, QFormLayout, QLabel, QProgressBar, QFrame
from src.nucleo.controle import Controle
from datetime import datetime
from src.interface.ui_util import formato_data


class VizLote(QGroupBox):
    def __init__(self, parent, controle: Controle, nome_lote):
        super(VizLote, self).__init__(parent)
        self.setTitle(f"Lote: {nome_lote}")
        self.lote = nome_lote
        self.controle = controle
        self.atualizar_dados()

    def __progresso_lote(self, plantio, colheita):
        pb = QProgressBar(self)
        total_dias = colheita - plantio
        agora = int(datetime.now().timestamp())
        dias = agora - plantio

        # calcular numero em dias
        total_dias = int(total_dias / 86400)  # 60 * 60 * 24
        dias = int(dias / 86400)
        restantes = total_dias - dias

        pb.setFormat(f"%v de {total_dias} dias ({restantes} restantes)")
        pb.setMaximum(total_dias)
        pb.setValue(dias)
        return pb

    @staticmethod
    def __data(plantio):
        return datetime.strftime(datetime.fromtimestamp(plantio), formato_data)

    def __progresso_saldo(self, cap_armz, saldo):
        cap_armz = round(cap_armz, 2)
        saldo = round(saldo, 2)

        pb = QProgressBar(self)

        porcentagem = int(round(saldo / cap_armz * 100))

        pb.setFormat(f"{saldo} / {cap_armz} ({porcentagem}%)")
        pb.setMaximum(cap_armz)
        pb.setValue(saldo)
        return pb

    def __agrupar_dados_monitoramento(self):
        dados = self.controle.dados_monitoramento(self.lote)
        agrupado = {}
        for valvula, numero_linha, cap_armz, saldo in dados:
            if valvula not in agrupado:
                agrupado[valvula] = [(numero_linha, cap_armz, saldo)]
            else:
                agrupado[valvula].append((numero_linha, cap_armz, saldo))
        return agrupado

    def valvulas(self):
        # todo: colocar data da ultima irrigação?
        dados = self.__agrupar_dados_monitoramento()
        for valvula in dados.keys():
            gb = QGroupBox(f"Válvula: {valvula}", self)
            layout = QFormLayout()
            gb.setLayout(layout)
            # criar titulos e a linha separadora
            lbl_titulos = (QLabel("Linha", self),
                           QLabel("Saldo do solo"))
            linha = QFrame(self, Qt.Widget)
            linha.setFrameStyle(QFrame.HLine)
            linha.setFrameShadow(QFrame.Sunken)
            # adicionar ao layout
            layout.addRow(*lbl_titulos)
            layout.addRow(linha)
            # adicionar linhas e barras de progresso
            for numero_linha, cap_armz, saldo in dados[valvula]:
                lbl_saldo = (QLabel(str(numero_linha)),
                             self.__progresso_saldo(cap_armz, saldo))
                layout.addRow(*lbl_saldo)
            self.layout().addRow(gb)

    def atualizar_dados(self):
        layout = QFormLayout(self)
        talhao, planta, data_plantio, data_colheita, estado_atual, fase_atual, \
            total_linhas = self.controle.dados_lote(self.lote)

        lbl_talhao = (QLabel("Talhão:", self),
                      QLabel(talhao, self))

        lbl_planta = (QLabel("Planta:", self),
                      QLabel(planta, self))

        lbl_progresso = (QLabel("Progresso do lote:", self),
                         self.__progresso_lote(data_plantio, data_colheita))

        lbl_data_plantio = (QLabel("Data plantio:", self),
                            QLabel(self.__data(data_plantio)))

        lbl_data_colheita = (QLabel("Data colheita:", self),
                             QLabel(self.__data(data_colheita)))

        layout.addRow(*lbl_planta)
        layout.addRow(*lbl_talhao)
        layout.addRow(*lbl_progresso)
        layout.addRow(*lbl_data_plantio)
        layout.addRow(*lbl_data_colheita)
        self.valvulas()
        self.setLayout(layout)
