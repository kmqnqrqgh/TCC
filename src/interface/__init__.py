from . import qrc

# o arquivo qrc.py contem os elementos da pasta recursos/interfaces. É
# necessário atualizar sempre que houver uma alteração em um dos arquivos ui da
# pasta /recursos/interface. Para atualizar basta executar o script atualizar na
# raiz do projeto (se houver uma venv local com a ultima versão do PyQt5
# instalada)

# Referências relevantes:

# https://doc.qt.io/qt-5/resources.html
# https://wiki.python.org/moin/PyQt/simple5
# http://pyqt.sourceforge.net/Docs/PyQt5/pyqt4_differences.html#pyrcc5
# http://pyqt.sourceforge.net/Docs/PyQt4/resources.html#pyrcc4
