import logging
import sys

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QApplication, QDesktopWidget

# noinspection PyUnresolvedReferences
from src.interface.classes_customizadas.tray_icon import IconeTray
from src.interface.janela_principal import Principal
from src.interface.login import Login
from src.interface.primeiro_uso import PrimeiroUso
from src.nucleo.controle import Controle

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Interface(QObject):
    app = QApplication(sys.argv)

    def __init__(self):
        super(Interface, self).__init__()
        self.app.setStyle("fusion")
        self.controle = Controle()
        self.janela_login = Login(self.controle)
        self.janela_primeiro_uso = PrimeiroUso(self.controle)
        self.janela_principal = Principal(self.controle)
        self.icone_tray = IconeTray(self)
        self.icone_tray.show()

        self.centralizar()

        self.icone_tray.sinal_sair_do_sistema.connect(self.app.quit)
        self.icone_tray.sinal_mostrar_janela.connect(self.restaurar)
        self.janela_principal.sinal_logout.connect(self.logout)
        self.janela_primeiro_uso.sinal_ok.connect(self.primeiro_uso_ok)
        self.janela_login.login_ok.connect(self.login_ok)

        self.avisado = False
        self.janela_principal.sinal_fechar.connect(self.avisar_minimizado)
        self.janela_login.sinal_fechar.connect(self.avisar_minimizado)

        if self.controle.redefinicao_senha_admin_necessaria:
            self.janela_primeiro_uso.show()
        else:
            self.janela_login.show()

        self.app.setQuitOnLastWindowClosed(False)

        # todo: implementar configurações persistentes
        # todo: criar manual usando sphinx
        sys.exit(self.app.exec_())

    def __del__(self):
        self.controle.finalizar()

    def avisar_minimizado(self):
        if not self.avisado:
            self.icone_tray.showMessage("Aviso",
                                        "Executando em segundo plano")
            self.avisado = True

    # sinal emitido pelo janela_primeiro_uso.sinal_ok
    def primeiro_uso_ok(self):
        self.janela_login.show()

    # sinal emitido pela janela de login
    def login_ok(self):
        logger.info(f"usuario autenticado: {self.controle.usuario}")
        self.janela_principal.show()

    # sinal emitido pela janela principal
    def logout(self):
        self.janela_principal.hide()
        self.janela_login.show()

    # sinal emitido pelo menu do tray de mostrar a janela
    def restaurar(self):
        if self.controle.usuario:
            self.janela_principal.show()
        else:
            self.janela_login.show()

    def centralizar(self):
        self.janela_principal.move(
            QDesktopWidget().availableGeometry().center() / 2)
