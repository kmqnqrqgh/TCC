from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog

from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class HistoricoIrrigacoes(QDialog):
    def __init__(self, parent, controle: Controle,*_):
        super(HistoricoIrrigacoes, self).__init__(parent, *_)
        load_qrc_ui(":/ui/historico.ui", self)
        self.setModal(True)
        self.setWindowTitle(f"Histórico de irrigações")
        self.controle = controle

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        self.accept()

    def atualizar(self):
        pass
