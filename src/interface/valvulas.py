from PyQt5.QtCore import pyqtSlot, QModelIndex, pyqtSignal
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QDialog

from src.interface.cadastros.add_valvula import AddValvula
from src.interface.alteracoes.alterar_valvula import AlterarValvula
from src.nucleo.controle import Controle
from .ui_util import load_qrc_ui


class Valvulas(QDialog):
    sinal_valvula_alterada = pyqtSignal()
    sinal_valvula_selecionada = pyqtSignal(str)

    def __init__(self, parent, controle: Controle, selecao=False, *_):
        """
        Janela que mostra todas as valvulas cadastradas
        :param parent: janela pai
        :param controle: classe controle
        :param selecao: quando a janela é usada p/ procura de valvulas
        :param _: flags
        """
        super(Valvulas, self).__init__(parent, *_)
        load_qrc_ui(":/ui/valvulas.ui", self)
        self.setWindowTitle("Valvulas")
        self.controle = controle
        if selecao:
            self.btnOK.setEnabled(False)
            self.btnAdicionar.setVisible(False)
            self.btnAlterar.setVisible(False)
            self.setModal(True)
        self.editavel = not selecao
        self.__inicializar_tabela()
        # todo: salvar opção
        self.op_pesquisa = {
            "por Talhão": self.pesquisar_por_talhao,
            "por Numero SIM": self.pesquisar_por_num_sim,
            "por Bomba": self.pesquisar_por_bomba
        }
        self.__inicializar_opcoes_pesquisa()
        self.atualizar()

    def __inicializar_tabela(self):
        titulos = ["ID", "Número SIM", "Bomba associada", "Talhão"]
        tamanhos = [(0, 80), (1, 120), (2, 120), (3, 120)]
        modelo = QStandardItemModel()
        modelo.setHorizontalHeaderLabels(titulos)
        self.tbValvulas.setModel(modelo)
        for tamanho in tamanhos:
            self.tbValvulas.horizontalHeader().resizeSection(*tamanho)

    def __inicializar_opcoes_pesquisa(self):
        for opcao in self.op_pesquisa:
            self.cbProcurarPor.addItem(opcao)

    def __atualizacao_interna(self):
        self.sinal_valvula_alterada.emit()
        self.atualizar()

    def pesquisar_por_talhao(self, texto):
        self.__filtrar(texto, 3)

    def pesquisar_por_num_sim(self, texto):
        self.__filtrar(texto, 1)

    def pesquisar_por_bomba(self, texto):
        self.__filtrar(texto, 2)

    def __filtrar(self, texto, coluna):
        modelo = self.tbValvulas.model()
        for linha in range(modelo.rowCount()):
            valor_campo = modelo.item(linha, coluna).text()
            if not len(valor_campo):
                continue
            esconder = texto not in valor_campo
            self.tbValvulas.setRowHidden(linha, esconder)

    def procurar(self, texto):
        self.op_pesquisa[self.cbProcurarPor.currentText()](texto)

    @pyqtSlot(str, name="on_cbProcurarPor_currentTextChanged")
    def cb_procurar_por(self, _):
        self.procurar(self.txtProcurar.text())

    @pyqtSlot(str, name="on_txtProcurar_textEdited")
    def txt_procurar(self, texto):
        self.procurar(texto)

    def atualizar(self):
        self.tbValvulas.model().setRowCount(0)
        valvulas = self.controle.listar_valvulas()
        if valvulas is None:
            return
        for item in valvulas:
            itens = [QStandardItem(str(i)) for i in item]
            self.tbValvulas.model().appendRow(itens)

    def __alterar_valvula(self, indice):
        if not self.editavel:
            return
        id_valvula = int(self.tbValvulas.model().item(indice.row(), 0).text())
        janela_alterar_valvula = AlterarValvula(self, self.controle, id_valvula)
        janela_alterar_valvula.accepted.connect(self.__atualizacao_interna)
        janela_alterar_valvula.show()
        self.btnAlterar.setEnabled(False)

    def __selecionar(self, indice):
        self.btnAlterar.setEnabled(False)
        self.btnOK.setEnabled(False)
        if self.editavel:
            self.close()
            return
        id_valvula = self.tbValvulas.model().item(indice.row(), 0).text()
        self.sinal_valvula_selecionada.emit(id_valvula)
        self.accept()

    @pyqtSlot(QModelIndex, name="on_tbValvulas_clicked")
    def tb_bombas_sel_item(self, indice: QModelIndex):
        self.btnAlterar.setEnabled(indice.isValid())
        self.btnOK.setEnabled(indice.isValid())

    @pyqtSlot(QModelIndex, name="on_tbValvulas_doubleClicked")
    def tb_valvulas_click_duplo(self, indice):
        if indice.isValid():
            if self.editavel:
                self.__alterar_valvula(indice)
            else:
                self.__selecionar(indice)

    @pyqtSlot(name="on_btnAlterar_clicked")
    def btn_alterar(self):
        indice: QModelIndex = self.tbValvulas.currentIndex()
        if indice.isValid():
            self.__alterar_valvula(indice)

    @pyqtSlot(name="on_btnOK_clicked")
    def btn_ok(self):
        if self.editavel:
            self.close()
        indice: QModelIndex = self.tbValvulas.currentIndex()
        if indice.isValid():
            self.__selecionar(indice)

    @pyqtSlot(name="on_btnAdicionar_clicked")
    def btn_adicionar(self):
        janela_add_valvulas = AddValvula(self, self.controle)
        janela_add_valvulas.accepted.connect(self.__atualizacao_interna)
        janela_add_valvulas.show()
        self.btnAlterar.setEnabled(False)
