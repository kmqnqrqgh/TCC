import logging.handlers
from src.interface.interface import Interface
from src.nucleo.misc.util import arquivo_config

logging.basicConfig(
    format='%(asctime)s %(name)s %(funcName)s %(threadName)-s %(levelname)s: '
           '%(message)s',
    datefmt="%Y-%m-%d %H:%M:%S",
    handlers=[logging.StreamHandler(),
              logging.handlers.RotatingFileHandler(
                  arquivo_config(f"log", "logs"),
                  mode='w',
                  encoding="UTF-8",
                  maxBytes=1024*1024)])

if __name__ == "__main__":
    # todo: adicionar método de verificação de multipla instância.
    # usar porta 43951:
    # max(1025, int("".join(str(ord(i)) for i in "TCC")) & 0xafff)
    Interface()
