import threading
import logging
from datetime import datetime
from typing import Dict, Tuple, List

from src.nucleo.banco.banco import Banco

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Processamento:
    intervalo = 5  # segundos

    def __init__(self, controle):
        self.__thread: threading.Thread = None
        self.__banco: Banco = None
        self.__flag_saida = threading.Event()
        self.__aviso_sem_dados = False
        self.controle = controle
        self.iniciar()

    def parar(self):
        self.__flag_saida.set()

    @staticmethod
    def __comparar_com_hoje(epoch1):
        return datetime.fromtimestamp(epoch1).strftime("%Y%m%d") == \
            datetime.now().strftime("%Y%m%d")

    def __calcular(self):
        dados_metereologicos = self.__banco.metereologia()
        if not dados_metereologicos:
            if not self.__aviso_sem_dados:
                logger.warning("não há dados metereológicos para se fazer os "
                               "cálculos de saldo. adicione para atualizar o "
                               "estado do sistema")
                self.__aviso_sem_dados = True
            return
        dados = ()
        for dat, et, chuv in dados_metereologicos:
            if self.__comparar_com_hoje(dat):
                dados = dat, et, chuv
                break
        if not dados:
            return
        data_met, et_referencia, chuva = dados
        deficits: Dict[int: float] = {}  # valvula: maior_deficit
        for linha, valvula, kc, lote, saldo, saldo_max, ultima_atualizacao in \
                self.__banco.estados_lotes_pendentes():
            # calculo do novo saldo
            novo_saldo = saldo - et_referencia * kc + chuva
            # limitar novo saldo ao limite de capacidade do solo
            novo_saldo = saldo_max if novo_saldo > saldo_max else novo_saldo
            # evitar saldo negativo
            novo_saldo = 0 if novo_saldo < 0 else novo_saldo
            # limitar casas decimais
            novo_saldo = round(novo_saldo, 2)
            self.__banco.definir_saldo_estado_sistema(linha, novo_saldo)
            logger.debug(f"Atualização do saldo: linha={linha}, "
                         f"saldo={saldo}, novo_saldo={novo_saldo}")
            deficit = round(saldo_max - novo_saldo, 2)
            if valvula not in deficits:
                deficits[valvula] = deficit
            else:
                deficits[valvula] = max(deficits[valvula], deficit)
        tuplas = [i for i in zip(deficits.keys(), deficits.values())]
        ordenados = sorted(tuplas, key=lambda x: x[1], reverse=True)
        self.__agrupar_valvulas(ordenados)

    def __agrupar_valvulas(self, valvulas: List[Tuple[int, float]]):
        bombas = self.__banco.bombas()
        if not bombas or not valvulas:
            return
        valvulas_enumeradas = {}
        bombas = {nome: max_valvulas for nome, _, max_valvulas, *_ in bombas}
        for nome in bombas:
            for id_valvula, deficit in valvulas:
                valvula, _, bomba, _ = self.__banco.valvula(id_valvula)
                if nome == bomba:
                    if nome not in valvulas_enumeradas:
                        valvulas_enumeradas[nome] = [(id_valvula, deficit)]
                    else:
                        valvulas_enumeradas[nome].append((id_valvula, deficit))
        for bomba in valvulas_enumeradas:
            limite = bombas[bomba]
            valvulas_enumeradas[bomba] = valvulas_enumeradas[bomba][:limite]
        print(valvulas)
        print(valvulas_enumeradas)
        # todo: testar melhor

    @property
    def thread_viva(self):
        return self.__thread.is_alive()

    def iniciar(self):
        logger.info("iniciando processamento em segundo plano")
        self.__flag_saida.clear()
        self.__thread = threading.Thread(target=self.__loop)
        self.__thread.setName("thread_processamento")
        self.__thread.start()

    def __loop(self):
        while not self.__flag_saida.wait(timeout=self.intervalo):
            self.__banco = Banco(True)  # uma conexão com o sqlite não pode ser
            self.__calcular()           # compartilhada com outras threads
            self.__banco = None
        logger.info("processamento em segundo plano parado.")
