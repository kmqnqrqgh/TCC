"""
Realiza patches no banco de dados para manter os dados consistentes e o banco
de dados funcional. As alterações são em cascata, isso significa que se o banco
estiver na versão 1 e precisar ser atualizada para versão 7, serão aplicadas em
etapas individuais as atualizações para a versão 2, então 3, então  4, etc.
consequentemente quanto mais antiga a versão do banco de dados, mais alterações
serão necessárias.
"""
import logging
import sqlite3
import datetime
from typing import Union

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


__msg_erro = "erro durante execução da atualização:"
__msg_sucesso = "atualizado com sucesso"


def __v0_para_v1(con: sqlite3.Connection):
    # demonstração, isso não será usado de verdade

    logger.info("atualizando da versão 0 para versão 1")
    try:
        con.executescript("""
            CREATE TABLE IF NOT EXISTS tipos_eventos (
                evento INTEGER PRIMARY KEY,
                descricao TEXT
            );
            CREATE TABLE IF NOT EXISTS historico (
                data INTEGER PRIMARY KEY,
                evento INTEGER REFERENCES tipos_eventos(evento),
                descricao TEXT
            );
            INSERT OR IGNORE INTO tipos_eventos (evento, descricao) VALUES 
                (0, "Abrir válvula(s)"),
                (1, "Fechar válvula(s)");
            PRAGMA user_version = 1;
        """)
    except sqlite3.OperationalError as e:
        logger.error(__msg_erro + str(e))
        con.rollback()
        return
    con.commit()


def __v1_para_v2(con: sqlite3.Connection):
    # demonstração, isso não será usado de verdade

    logger.info("atualizando da versão 1 para 2")
    try:
        con.executescript("ALTER TABLE bombas "
                          "ADD COLUMN max_valvulas_simultaneas INTEGER;"
                          "PRAGMA user_version = 2")
    except sqlite3.OperationalError as e:
        logger.error(__msg_erro + str(e))
        con.rollback()
        return
    con.commit()


def __v2_para_v3(con: sqlite3.Connection):
    # changelog: adicionar campos "auto_irrigacao_inicio" e "auto_irrigacao_fim"
    # na tabela "bombas"

    logger.info("atualizando para versão 3")
    try:
        with con:
            agora = datetime.datetime.now().timestamp()
            tabela_antiga = f"__bombas_{int(agora)}"
            con.execute(f"alter table bombas rename to '{tabela_antiga}'")
            con.executescript("create table bombas ( "
                              "nome TEXT PRIMARY KEY,"
                              "numero_sim INTEGER,"
                              "max_valvulas_simultaneas INTEGER,"
                              "auto_irrigacao_inicio INTEGER NOT NULL,"
                              "auto_irrigacao_fim INTEGER NOT NULL"
                              ");")
            con.execute("insert into bombas select nome, numero_sim, "
                        "max_valvulas_simultaneas, 79200, 100800 "
                        f"from {tabela_antiga}")
            con.execute(f"drop table {tabela_antiga}")

            con.execute("PRAGMA user_version = 3")
    except sqlite3.OperationalError as e:
        erro = __msg_erro + str(e)
        logger.error(erro)
        return erro
    except sqlite3.IntegrityError as e:
        erro = __msg_erro + str(e)
        logger.error(erro)
        return erro
    con.commit()


def __v3_para_v4(con: sqlite3.Connection):
    # changelog: adicionar campo "delay_pressurizacao" na tabela "bombas"

    logger.info("atualizando para versão 4")
    try:
        with con:
            agora = datetime.datetime.now().timestamp()
            tabela_antiga = f"__bombas_{int(agora)}"
            con.execute(f"alter table bombas rename to '{tabela_antiga}'")
            con.executescript("create table bombas ( "
                              "nome TEXT PRIMARY KEY,"
                              "numero_sim INTEGER,"
                              "max_valvulas_simultaneas INTEGER,"
                              "auto_irrigacao_inicio INTEGER NOT NULL,"
                              "auto_irrigacao_fim INTEGER NOT NULL,"
                              "delay_pressurizacao INTEGER NOT NULL"
                              ");")
            con.execute("insert into bombas select nome, numero_sim, "
                        "max_valvulas_simultaneas, 79200, 100800, 600 "
                        f"from {tabela_antiga}")
            con.execute(f"drop table {tabela_antiga}")

            con.execute("PRAGMA user_version = 4")
    except sqlite3.OperationalError as e:
        erro = __msg_erro + str(e)
        logger.error(erro)
        return erro
    except sqlite3.IntegrityError as e:
        erro = __msg_erro + str(e)
        logger.error(erro)
        return erro
    con.commit()


__atualizacao = {
    (0, 1): __v0_para_v1,
    (1, 2): __v1_para_v2,
    (2, 3): __v2_para_v3,
    (3, 4): __v3_para_v4
}


def __aplicar_parcial(ver_anterior, nova_versao, conexao: sqlite3.Connection):
    if (ver_anterior, nova_versao) not in __atualizacao:
        erro = f"Método de atualização da versão {ver_anterior} para " \
               f"{nova_versao} não disponível"
        logger.error(erro)
        return erro
    return __atualizacao[(ver_anterior, nova_versao)](conexao)


def aplicar_atualizacao(ver_atual: int, nova_versao: int,
                        conexao: sqlite3.Connection) -> Union[None, str]:
    """
    aplica atualizações disponíveis
    :param ver_atual: versão atual do banco de dados
    :param nova_versao: versão posterior do banco de dados
    :param conexao: objeto de conexão do sqlite
    :return: msg de erro, se houver
    """
    atual = ver_atual
    nova = atual + 1
    while atual < nova_versao:
        erro = __aplicar_parcial(atual, nova, conexao)
        if erro:
            return erro
        atual = nova
        nova += 1
    logger.info(__msg_sucesso)
