import sqlite3
import bz2


def backup(con: sqlite3.Connection, arquivo):
    buffer = []
    from .banco import Banco
    # noinspection PyTypeChecker
    for i in con.iterdump():
        if i.startswith("INSERT INTO") and Banco.Tabelas.usuarios.name not in i:
            buffer.append("INSERT OR IGNORE" + i[6:])
    str_buffer = "\n".join(buffer)
    with open(arquivo, "wb") as f:
        f.write(bz2.compress(str_buffer.encode()))


def restaurar(con: sqlite3.Connection, arquivo):
    with open(arquivo, "rb") as f:
        # noinspection PyBroadException
        try:
            script = bz2.decompress(f.read()).decode()
            con.execute("pragma foreign_keys = 0")
            con.executescript(script)
            con.execute("pragma foreign_keys = 1")
        except sqlite3.OperationalError:
            con.rollback()
            return False
        con.commit()
        return True
