import errno
import logging
import os
import sqlite3
import sys

from datetime import datetime
from enum import Enum
from src.nucleo.misc.util import arquivo_config
from .scripts import criar_db
from src.nucleo.criptografia.cripto import Senhas
from src.nucleo.banco import backup

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Permissoes(Enum):
    """
    Permissões são concedidas pela categoria da operação:

    Existem 3 categorias:
        Dados estáticos;
        Dados dinâmicos;
        Dados de usuario.

    Dados de usuários são as chaves de acesso (senhas salvas pelo bcrypt), nomes
    e permissões de cada usuário só podem ser alterados pelo administrador.
    Os privilégios de cada tipo de usuário seguem um nivel hierárquico
    básico: Administrador > Operador especial > Operador.

    Dados estáticos não são alterados, por exemplo: linhas, talhões, bombas,
    valvulas, plantas, etc.

    Dados dinâmicos são aqueles que sofrem alterações constantes, como
    por exemplo: lotes, agendamentos, dados metereologicos, etc.

    Todos os usuários tem permissão de leitura dos dados.
    """
    # add, remover, modificar: linhas, talhoes, bombas, valvulas, etc
    AlterarDadosEstaticos = 0

    # add, remover, modificar: lotes, agendamentos, dados meteriologicos
    AlterarDadosDinamicos = 1

    # add, remover, modificar: usuarios, senhas
    AlterarDadosUsuarios = 2

    @property
    def tabelas(self):
        if self.name == self.AlterarDadosDinamicos.name:
            return [Banco.Tabelas.agendamentos, Banco.Tabelas.fases_lote,
                    Banco.Tabelas.metereologia, Banco.Tabelas.lotes]

        elif self.name == self.AlterarDadosEstaticos.name:
            return [Banco.Tabelas.bombas, Banco.Tabelas.dados_plantas,
                    Banco.Tabelas.linhas, Banco.Tabelas.plantas,
                    Banco.Tabelas.talhoes, Banco.Tabelas.valvulas,
                    Banco.Tabelas.todas]

        elif self.name == self.AlterarDadosUsuarios.name:
            return [Banco.Tabelas.usuarios, Banco.Tabelas.niveis_usuarios]


class NiveisAcesso(Enum):
    admin = "admin"
    operador_especial = "operador especial"
    operador = "operador"


_niveis_acessos = {
    NiveisAcesso.admin: [Permissoes.AlterarDadosUsuarios,
                         Permissoes.AlterarDadosEstaticos,
                         Permissoes.AlterarDadosDinamicos],

    NiveisAcesso.operador_especial: [Permissoes.AlterarDadosDinamicos],

    NiveisAcesso.operador: []
}


class EstadosLote(Enum):
    # é preciso saber qual estado torna um lote ativo de fato
    # estar ativo é uma condição booleana, e vários tipos conflitam com isso
    # porisso é necessário o segundo parametro.
    # caso contrario, ficar comparando tudo com o texto "ativo" seria gambiarra

    # Tipo = "nome interno", usar em estado_sistema
    Finalizado = "finalizado", False
    Cancelado = "cancelado", False
    Planejado = "planejado", False
    Ativo = "ativo", True


class Banco:
    versao = 4
    nome_arquivo = "dados.db"
    erro_alteracao = "Erro fazendo alteração."
    formato_data_hora = "%H:%M %d/%m/%Y"
    formato_data = "%d/%m/%Y"
    timeout = 600

    class Tabelas(Enum):
        bombas, linhas, lotes, talhoes, valvulas, plantas, agendamentos, \
            metereologia, fases_lote, estado_sistema, dados_plantas, usuarios, \
            niveis_usuarios, todas = range(14)

    # todo: fazer uma fila rodando numa thread pra fazer queries
    def __init__(self, pular_verificacao=False):
        self.__redefinicao = False
        self.arquivo_db = arquivo_config(Banco.nome_arquivo)
        if not pular_verificacao:
            self.verificacao_inicial()
        self.conexao = sqlite3.connect(self.arquivo_db, self.timeout)
        self.exec("PRAGMA foreign_keys = 1;")

    def exec(self, query, *args):
        return self.conexao.execute(query, args)

    def __del__(self):
        self.conexao.close()

    @property
    def redefinicao_necessaria(self):  # redefinição de senha do administrador
        return self.__redefinicao

    def definir_admin(self, senha):
        if self.__redefinicao:
            existente = self.exec(
                "select nome from usuarios where nome = 'admin'").fetchone()
            if existente:
                logger.warning("redefinindo senha de administrador")
            chave = Senhas(senha).chave
            self.exec("insert or replace into usuarios values ('admin', ?, "
                      "'admin')", chave)
            self.conexao.commit()
            self.__redefinicao = False
            return True
        else:
            logger.error("redefinição da senha do administrador não permitida")

    def verificar_credenciais(self, usuario, senha):
        try:
            cursor = self.conexao.cursor()
            # ativar chaves estrangeiras obrigatórias, senão o sqlite não mostra
            # erros de integridade por falta do relacionamento obrigatório
            cursor.execute("PRAGMA foreign_keys = 1;")
            cursor.execute("select nome, chave, nivel_acesso from usuarios "
                           "where nome = ?", (usuario,))
            resultado = cursor.fetchone()
        except sqlite3.OperationalError as e:
            logger.critical(f"Erro de operação no banco de dados: {e}")
            return
        if not resultado:
            return
        else:
            nome, chave, nivel_acesso = resultado
            if not Senhas.verificar(chave, senha):
                return
            else:
                return nivel_acesso

    def __criar_do_zero(self):
        logger.info(f"criando arquivo do banco de dados: {self.arquivo_db}")
        try:
            conexao = sqlite3.connect(self.arquivo_db)
            cursor = conexao.cursor()
            cursor.execute("PRAGMA foreign_keys = 1;")
            cursor.executescript(criar_db)

            for nv in _niveis_acessos:
                cursor.execute("insert into niveis_usuarios values (?)",
                               (nv.value,))
            for estados in EstadosLote:
                cursor.execute("insert into estados_lote values (?, ?)",
                               estados.value)
            cursor.execute("update propriedades_internas set criacao_ok = 1")
            conexao.commit()

        except sqlite3.OperationalError as e:
            logger.error(f"erro na criação do banco de dados: {e}")
            sys.exit(errno.EINVAL)  # argumento invalido
        self.__redefinicao = True
        return conexao

    def __verificar_criacao(self, conexao):
        if not conexao.execute("select criacao_ok from propriedades_internas"
                               " where id = 0").fetchone()[0]:
            logger.critical("Arquivo de banco de dados criado com problemas. "
                            "Tentando recriar banco de dados...")
            os.remove(self.arquivo_db)
            self.__criar_do_zero()
        admin_existe = conexao.execute(
            "select nome from usuarios where nome = 'admin'").fetchone()
        if not admin_existe:
            self.__redefinicao = True

    def __atualizar_versao(self):
        conexao = sqlite3.connect(self.arquivo_db)
        versao = conexao.execute("pragma user_version").fetchone()[0]
        if versao < self.versao:
            from . import atualizacao
            erro = atualizacao.aplicar_atualizacao(versao, self.versao, conexao)
            if not erro:
                self.versao = versao
            else:
                logger.error(erro)
        elif versao > self.versao:
            logger.critical("Versão do banco de dados é posterior ao do "
                            f"sistema: atual:{self.versao} banco:{versao}")
            sys.exit(errno.EINVAL)  # argumento invalido
        return conexao

    def verificacao_inicial(self):
        """
        Verifica se o arquivo do banco de dados existe, e tenta criar se ainda
        não existir.
        :return: None
        """
        if not os.path.isfile(self.arquivo_db):
            conexao = self.__criar_do_zero()
        else:
            conexao = self.__atualizar_versao()
            self.__verificar_criacao(conexao)
        conexao.close()

    def verificar_permissao(self, usuario, tabela: Tabelas, escrita=False):
        nivel = self.exec("select nivel_acesso from usuarios where nome = ?",
                          usuario).fetchone()
        if not nivel:  # nivel = None se 'nome' não for encontrado
            logger.critical(f"Usuario não cadastrado '{usuario}' tentando "
                            f"acessar {str(tabela)}")
            return False
        # fetchone retorna uma tupla mesmo que tenha só 1 elemento
        nivel = NiveisAcesso(nivel[0])
        if nivel not in _niveis_acessos:
            logger.critical(f"Nivel de acesso {nivel} não especificado. "
                            f"Abortando operação.")
            return False
        if not escrita:
            return True
        for permissao in _niveis_acessos[nivel]:
            if tabela in permissao.tabelas:
                return True
        return False

    def cadastrar_bomba(self, nome, numero_sim, max_valvulas, hora_inicio,
                        hora_fim, delay_pressurizacao):
        existe = self.exec("select nome, numero_sim from bombas where nome = ? "
                           "or numero_sim = ?", nome, numero_sim).fetchone()
        if existe:
            msg = f"dados já existentes: {existe}"
            logger.error(msg)
            return msg
        self.exec("insert into bombas values (?, ?, ?, ?, ?, ?)", nome,
                  numero_sim, max_valvulas, hora_inicio, hora_fim,
                  delay_pressurizacao)
        self.conexao.commit()
        logger.info(f"nova bomba cadastrada: {nome}")

    def alterar_bomba(self, nome: str, novo_nome: str, novo_numero_sim: int,
                      novo_max_valvulas: int, novo_hora_inicio: int,
                      novo_hora_fim: int, delay_pressurizacao: int):
        try:
            self.exec("update bombas set nome = ?, numero_sim = ?, "
                      "max_valvulas_simultaneas = ?, auto_irrigacao_inicio = ?,"
                      " auto_irrigacao_fim = ?, delay_pressurizacao = ? "
                      "where nome = ?",
                      novo_nome,  novo_numero_sim, novo_max_valvulas,
                      novo_hora_inicio, novo_hora_fim, delay_pressurizacao,
                      nome)
            self.conexao.commit()
        except sqlite3.IntegrityError:
            logger.error(self.erro_alteracao)
            return self.erro_alteracao

    def deletar_bomba(self, nome: str):
        self.exec("delete from bombas where nome = ?", nome)
        self.conexao.commit()

    def dependencias_bomba(self, nome_bomba):
        resultado = self.exec("select valvula from valvulas where bomba = ?",
                              nome_bomba)
        return len(resultado.fetchall())

    def bombas(self):
        return self.exec("select nome, numero_sim, max_valvulas_simultaneas, "
                         "auto_irrigacao_inicio, auto_irrigacao_fim, "
                         "delay_pressurizacao from bombas "
                         "where nome is not NULL").fetchall()

    def bomba(self, nome_bomba):
        return self.exec("select numero_sim, max_valvulas_simultaneas, "
                         "auto_irrigacao_inicio, auto_irrigacao_fim, "
                         "delay_pressurizacao from bombas where nome = ?",
                         nome_bomba).fetchone()

    def cadastrar_talhao(self, nome_talhao):
        try:
            self.exec("insert into talhoes values (?)", nome_talhao)
            self.conexao.commit()
            logger.info(f"novo talhão cadastrado: {nome_talhao}")
            return
        except sqlite3.IntegrityError:
            msg = f"Talhão com nome \"{nome_talhao}\" já existente"
            logger.error(msg)
            return msg

    def alterar_talhao(self, nome_talhao, novo_nome):
        try:
            self.exec("update talhoes set nome_talhao = ? where nome_talhao=?",
                      novo_nome, nome_talhao)
            self.conexao.commit()
        except sqlite3.IntegrityError as e:
            logger.error(self.erro_alteracao)
            logger.debug(str(e), exc_info=True)
            return self.erro_alteracao

    def dependencias_talhao(self, talhao):
        resultado = self.exec("select count(valvula) from valvulas where "
                              "nome_talhao = ?", talhao)
        return int(resultado.fetchone()[0])

    def deletar_talhao(self, nome_talhao):
        self.exec("delete from talhoes where nome_talhao = ?", nome_talhao)
        self.conexao.commit()

    def talhoes(self):
        resultado = self.exec("select nome_talhao from talhoes "
                              "where nome_talhao is not NULL")
        return [i[0] for i in resultado.fetchall()]

    def cadastrar_valvula(self, numero_sim, nome_bomba, nome_talhao):
        existente = self.exec("select valvula from valvulas where numero_sim=?",
                              numero_sim).fetchone()
        if existente:
            msg = f"Número SIM já cadastrado: {numero_sim}"
            logger.error(msg)
            return msg
        cur = self.conexao.cursor()
        cur.execute("insert into valvulas values (NULL, ?, ?, ?)", (numero_sim,
                    nome_bomba, nome_talhao))
        self.conexao.commit()
        logger.info(f"nova valvula cadastrada: {cur.lastrowid}")
        return

    def alterar_valvula(self, valvula: int, novo_sim, nova_bomba, novo_talhao):
        try:
            self.exec(
                "update valvulas set numero_sim = ?, bomba = ?, nome_talhao=? "
                "where valvula = ?", novo_sim, nova_bomba, novo_talhao, valvula)
            self.conexao.commit()
        except sqlite3.IntegrityError:
            logger.error(self.erro_alteracao)
            return self.erro_alteracao

    def deletar_valvula(self, valvula: int):
        self.exec("delete from valvulas where valvula = ?", valvula)
        self.conexao.commit()

    def valvulas(self):
        return self.exec("select valvula, numero_sim, bomba, nome_talhao from "
                         "valvulas").fetchall()

    def valvula(self, id_valvula):
        return self.exec("select valvula, numero_sim, bomba, nome_talhao from "
                         "valvulas where valvula = ?", id_valvula).fetchone()

    def dependencias_valvula(self, id_valvula):
        resultado = self.exec("select count(valvula) from linhas "
                              "where valvula = ?", id_valvula)
        return int(resultado.fetchone()[0])

    def cadastrar_nome_planta(self, nome):
        dados_existentes = self.exec("select 1 from dados_plantas where "
                                     "nome_planta = ?", nome).fetchone()
        if not dados_existentes:
            self.exec("delete from plantas where nome_planta = ?", nome)
            existente = False
        else:
            existente = self.exec("select nome_planta from plantas "
                                  "where nome_planta = ?", nome).fetchone()
        if existente:
            msg = f"Planta já cadastrada: {nome}"
            logger.error(msg)
            return msg
        self.exec("insert into plantas values (?)", nome)
        self.conexao.commit()
        logger.info(f"nova planta cadastrada: {nome}")

    def alterar_nome_planta(self, nome_antigo, nome_novo):
        if nome_antigo == nome_novo:
            return
        try:
            self.exec("update plantas set nome_planta = ? "
                      "where nome_planta = ?", nome_novo, nome_antigo)
        except sqlite3.IntegrityError:
            erro = f"Erro alterando nome da planta. Planta \"{nome_novo}\" " \
                   "já existente"
            logger.error(erro)
            self.conexao.rollback()
            return erro
        self.conexao.commit()

    def deletar_planta(self, nome_planta):
        self.exec("delete from plantas where nome_planta = ?", nome_planta)
        self.conexao.commit()

    def plantas(self):
        r = self.exec("select nome_planta from plantas").fetchall()
        # é preciso pegar o primeiro elemento da tupla:
        # [('planta1',), ('planta2',)] -> ['planta1', 'planta2']
        return [i[0] for i in r]

    def dependencias_planta(self, nome_planta):
        return self.exec("select count(*) from lotes where nome_planta = ?",
                         nome_planta).fetchone()[0]

    def cadastrar_dados_planta(self, nome_planta, fase, kc, profundidade,
                               proporcao_fase):
        dados_fase_existente = self.exec(
            "select 1 from dados_plantas where nome_planta = ? and fase = ?",
            nome_planta, fase).fetchone()
        if dados_fase_existente:
            msg = f"Dados já existentes: planta {nome_planta} fase {fase} "
            logger.error(msg)
            return msg
        self.conexao.execute("insert into dados_plantas "
                             "values (NULL, ?, ?, ?, ?, ?)",
                             (nome_planta, fase, round(kc, 2), profundidade,
                              round(proporcao_fase, 2)))

    def cadastrar_dados_plantas(self, dados):
        soma_proporcao = 0
        for nome_planta, fase, kc, profundidade, proporcao_fase in dados:
            if self.cadastrar_dados_planta(nome_planta, fase, kc, profundidade,
                                           proporcao_fase):
                self.conexao.rollback()
                break
            soma_proporcao += proporcao_fase
        if soma_proporcao != 100:
            self.conexao.rollback()
            erro = ("Proporção de tempo das fases não iguala a 100% (atual = "
                    f"{soma_proporcao}%)")
            logger.error(erro)
            return erro
        self.conexao.commit()

    def alterar_dados_planta(self, nome_planta, fase, kc, profundidade,
                             proporcao_fase):
        try:
            self.exec("update dados_plantas set kc = ?, "
                      "profundidade_raiz = ?, proporcao_fase = ? "
                      "where nome_planta = ? and fase = ?",
                      round(kc, 2), profundidade, round(proporcao_fase, 2),
                      nome_planta, fase)
        except sqlite3.IntegrityError as e:
            erro = f"Erro alterando dados: planta {nome_planta} fase {fase}"
            logger.error(erro)
            logger.debug(str(e), exc_info=True)
            self.conexao.rollback()
            return erro
        self.conexao.commit()

    def alterar_dados_plantas(self, dados):
        soma_proporcao = 0
        for nome, fase, kc, profundidade, proporcao_fase in dados:
            if self.alterar_dados_planta(nome, fase, kc, profundidade,
                                         proporcao_fase):
                self.conexao.rollback()
                break
            soma_proporcao += proporcao_fase
        if soma_proporcao != 100:
            self.conexao.rollback()
            erro = "Proporção de tempo das fases não iguala a 100%"
            logger.error(erro)
            return erro
        self.conexao.commit()

    def deletar_dados_planta(self, nome_planta, fase):
        self.exec("delete from dados_plantas where nome_planta = ? and fase =?",
                  nome_planta, fase)
        self.conexao.commit()

    def dados_plantas(self):
        return self.exec("select nome_planta, fase, round(kc, 2), "
                         "profundidade_raiz, proporcao_fase from dados_plantas"
                         ).fetchall()

    def __usuario_existente(self, usuario):
        return self.exec("select count(*) from usuarios where nome = ?",
                         usuario).fetchone()[0] > 0

    def cadastrar_usuario(self, usuario, senha, nivel: NiveisAcesso):
        msg = None
        if nivel not in _niveis_acessos:
            msg = f"Nível de usuário não existente: {nivel}"
        if self.__usuario_existente(usuario):
            msg = f"Usuário existente: {usuario}"
        if msg:
            logger.error(msg)
            return msg
        self.exec("insert into usuarios values (?, ?, ?)", usuario,
                  Senhas(senha).chave, nivel.value)
        self.conexao.commit()
        logger.info(f"novo usuario cadastrado: {usuario}")

    def alterar_usuario(self, usuario, nova_senha, senha_antiga=None):
        if not self.__usuario_existente(usuario):
            msg = f"Usuario inexistente: {usuario}"
            logger.error(msg)
            return msg
        if senha_antiga:
            chave = self.exec("select chave from usuarios where nome = ?",
                              usuario).fetchone()[0]
            if not Senhas.verificar(chave, senha_antiga):
                msg = "Senha antiga incorreta"
                logger.error(msg)
                return msg
        self.exec("update usuarios set chave = ? where nome = ?",
                  Senhas(nova_senha).chave, usuario)
        self.conexao.commit()

    def alterar_nivel_usuario(self, usuario, nivel: NiveisAcesso):
        msg = None
        if not self.__usuario_existente(usuario):
            msg = f"Usuario inexistente: {usuario}"
        if nivel not in _niveis_acessos:
            msg = f"Nível de usuário não existente: {nivel}"
        if msg:
            logger.error(msg)
            return msg
        self.exec("update usuarios set nivel_acesso = ? where nome = ?",
                  nivel.value, usuario)
        self.conexao.commit()

    def deletar_usuario(self, usuario):
        msg = None
        if not self.__usuario_existente(usuario):
            msg = f"Usuario inexistente: {usuario}"
        if usuario == "admin":
            msg = "Usuário admin não pode ser apagado"
        if msg:
            logger.error(msg)
            return msg
        self.exec("delete from usuarios where nome = ?", usuario)
        self.conexao.commit()

    def usuarios(self):
        return self.exec("select nome, nivel_acesso from usuarios").fetchall()

    def linhas(self):
        return self.exec("select linha, valvulas.valvula, nome_talhao, "
                         "valvulas.bomba, numero_local, "
                         "capacidade_armazenamento, vazao from linhas inner "
                         "join valvulas on linhas.valvula = valvulas.valvula"
                         ).fetchall()

    def cadastrar_linha(self, valvula, vazao, cap_armz, num_local):
        existe = self.exec("select count(*) from valvulas where valvula = ?",
                           valvula).fetchone()[0]
        if not existe:
            msg = "Valvula associada inexistente"
            logger.error(msg)
            return msg
        cur = self.conexao.cursor()
        cur.execute("insert into linhas values (NULL, ?, ?, ?, ?)", (valvula,
                    num_local, cap_armz, vazao))
        logger.info(f"nova linha cadastrada: {cur.lastrowid}")
        self.conexao.commit()

    def alterar_linha(self, linha, valvula, vazao, cap_armz, num_local):
        try:
            self.exec("update linhas set vazao = ?, "
                      "capacidade_armazenamento = ?, "
                      "numero_local = ?, valvula = ? where linha = ?", vazao,
                      cap_armz, num_local, valvula, linha)
        except sqlite3.OperationalError:
            erro = "Erro alterando linha"
            logger.error(erro)
            self.conexao.rollback()
            return erro
        self.conexao.commit()

    def deletar_linha(self, linha):
        self.exec("delete from linhas where linha = ?", linha)
        self.conexao.commit()

    @staticmethod
    def __validar_dados_lote(estado, datas_transicoes, data_colheita):
        try:
            estado = [i for i in EstadosLote if str.lower(i.name) == estado][0]
        except IndexError:
            # isso provavelmente nunca vai ser executado
            return f"Estado \"{estado}\" inválido"
        data_anterior = 0
        for data in datas_transicoes:
            if data < data_anterior or data > data_colheita:
                # os dados são tratados na entrada, portanto é improvavel que
                # um erro ocorra até esse ponto
                return "Uma ou mais datas de transição de fase inválidas"
            data_anterior = data

        agora = int(datetime.now().timestamp())
        if estado == EstadosLote.Ativo:
            if datas_transicoes[-1] < agora and data_colheita < agora:
                return "Lote não pode estar no estado ativo (data retroativa)"
            elif datas_transicoes[0] > agora:
                return "Lote não pode estar no estado ativo (data futura)"

    def cadastrar_lote(self, lote, planta, linhas: list, datas_transicoes,
                       data_colheita, estado):
        lote_existe = self.exec("select count(*) from lotes where lote = ?",
                                lote).fetchone()[0]
        planta_existente = self.exec("select nome_planta from plantas "
                                     "where nome_planta = ?", planta).fetchone()
        qs = ','.join(['?' for _ in linhas])
        linhas_usadas = self.exec("select linha from estado_sistema "
                                  f"where linha in ({qs})", *linhas).fetchone()
        sem_linhas = not len(linhas)
        dados_invalidos = self.__validar_dados_lote(estado, datas_transicoes,
                                                    data_colheita)
        msg = ""
        if sem_linhas:
            msg = "Pelo menos 1 linha deve ser selecionada\n"
        if linhas_usadas:
            msg += "Uma ou mais linhas já estão sendo usadas em outro lote\n"
        if dados_invalidos:
            msg += dados_invalidos + "\n"
        if lote_existe:
            msg += "Lote já existente\n"
        if not planta_existente:
            msg += "Planta inexistente\n"
        if msg:
            msg = msg[:-1]
            logger.error(msg)
            return msg
        self.exec("insert into lotes values (?, ?, ?, ?)",
                  lote, planta, data_colheita, estado)
        for i, data_transicao in enumerate(datas_transicoes):
            self.exec("insert into fases_lote values (NULL, ?, ?, ?)",
                      lote, i, data_transicao)

        self.conexao.executemany("insert into estado_sistema values (?, ?, ?, "
                                 "?)", ((i, lote, 0, False) for i in linhas))
        self.conexao.executemany("insert into linhas_lote values (NULL, ?, ?)",
                                 ((i, lote) for i in linhas))
        self.conexao.commit()
        logger.info(f"novo lote cadastrado: {lote}")

    def alterar_lote(self, lote, nova_planta, novas_linhas: list,
                     novas_datas_transicoes, nova_data_colheita, novo_estado):
        try:
            self.exec("update lotes set nome_planta = ?, data_colheita = ?, "
                      "estado = ? where lote = ?", nova_planta,
                      nova_data_colheita, novo_estado, lote)
            for i, data_transicao in enumerate(novas_datas_transicoes):
                self.exec("update fases_lote set data_mudanca = ? where "
                          "lote = ? and fase = ?", data_transicao, lote, i)
            self.exec("delete from linhas_lote where lote = ?", lote)
            for nova_linha in novas_linhas:
                self.exec("insert into linhas_lote values (NULL, ?, ?)",
                          nova_linha, lote)
        except sqlite3.IntegrityError:
            erro = self.erro_alteracao
            logger.error(erro)
            return erro
        self.conexao.commit()

    def deletar_lote(self, lote):
        self.exec("delete from lotes where lote = ?", lote)
        self.conexao.commit()

    def lote(self, lote):
        result = self.exec(
            "select nome_talhao, nome_planta, data_mudanca, data_colheita, "
            "estado, fase_atual, total_linhas from dados_lotes "
            "where lote = ?", lote).fetchone()
        return result

    def lotes(self):
        result = self.exec(
            "select lote, nome_planta, data_mudanca, data_colheita, estado, "
            "fase_atual, total_linhas from dados_lotes").fetchall()
        return result

    def linhas_lote(self, lote):
        res = self.exec("select linha from linhas_lote where lote = ?", lote)
        return [i[0] for i in res.fetchall()]

    def fases_lote(self, lote):
        return self.exec("select fase, data_mudanca from fases_lote "
                         "where lote = ?", lote).fetchall()

    def estados_lote(self):
        return [i[0] for i in
                self.exec("select estado from estados_lote").fetchall()]

    def add_metereologia(self, data, et_referencia, chuva):
        existente = self.exec("select data from metereologia where data = ?",
                              data).fetchone()
        if existente:
            data = datetime.fromtimestamp(data).strftime(self.formato_data)
            erro = f"Dados metereológicos do dia {data} já existentes"
            logger.error(erro)
            return erro
        self.exec("insert into metereologia values (?, ?, ?)", data,
                  et_referencia, chuva)
        self.conexao.commit()

    def alterar_metereologia(self, data, nova_et_referencia, nova_chuva):
        if not self.exec("select data from metereologia where data = ?",
                         data).fetchone():
            erro = f"Dados metereológicos da data {data} não existentes"
            logger.error(erro)
            return erro
        self.exec("update metereologia set et_referencia = ?, chuva = ? where "
                  "data = ?", nova_et_referencia, nova_chuva, data)
        self.conexao.commit()

    def metereologia(self):
        return self.exec("select data, et_referencia, chuva "
                         "from metereologia").fetchall()

    def agendamentos(self):
        sql = "select comp_agnd.agendamento, valvula, bomba, " \
              "data_hora_inicio, data_hora_fim, manual, confirmado " \
              "from agendamentos inner join componente_agendamento comp_agnd " \
              "on comp_agnd.agendamento = agendamentos.agendamento"
        return self.exec(sql).fetchall()

    def validar_agendamento(self, inicio, fim, valvulas, bombas):
        sql = "select comp_agnd.agendamento from agendamentos " \
              "inner join componente_agendamento comp_agnd " \
              "  on comp_agnd.agendamento = agendamentos.agendamento " \
              "where ((data_hora_inicio <= ? and data_hora_fim >= ?) " \
              "  or (data_hora_inicio <= ? and data_hora_fim >= ?)) "

        if inicio < int(datetime.now().timestamp()):
            return "Agendamentos não podem iniciar numa data passada"

        multi = ",".join(['?' for _ in range(len(valvulas))]) # [?, ?, ?, ...]

        # verificar se existem intersecções de tempo nas valvulas
        existente = self.exec(sql + f" and valvula in ({multi}) ",
                              inicio, inicio, fim, fim, *valvulas).fetchone()
        if existente:
            return "Um ou mais agendamentos já utilizam o horário especificaco"

        # remova o comentário se for utilizar as bombas das valvulas
        # bombas = self.exec("select bomba from valvulas where bomba in "
        #                    f"({'?' * len(valvulas)})", *valvulas).fetchall()
        # bombas = [i[0] for i in bombas]

        # verificar se uma bomba foi programada num horário ruim
        existente = self.exec(sql + f" and bombas in ({'?' * len(bombas)}) ",
                              inicio, inicio, fim, fim, *bombas).fetchone()
        if existente:
            return "Uma ou mais bombas utilizam uma faixa de horário reservada"

    def add_agendamento(self, inicio, fim, manual, valvulas, bombas):
        # confirmado = quando não houve problema durante a execução da
        # irrigação

        msg = self.validar_agendamento(inicio, fim, valvulas, bombas)
        if msg:
            logger.error(msg)
            return msg
        if valvulas and bombas:
            erro = "Agendamento de válvulas e bombas deve ser separado"
            logger.error(erro)
            return erro
        for valvula in valvulas:
            valvula_existente = self.exec("select 1 from valvulas "
                                          "where valvula = ?",
                                          valvula).fetchone()
            if not valvula_existente:
                erro = f"Valvula {valvula} inexistente"
                logger.error(erro)
                return erro
        for bomba in bombas:
            bomba_existente = self.exec("select 1 from bombas where nome = ?",
                                        bomba).fetchone()
            if not bomba_existente:
                erro = f"Bomba '{bomba}' inexistente"
                logger.error(erro)
                return erro

        cursor = self.conexao.cursor()
        # todo: importante focar no status do agendamento
        cursor.execute("insert into agendamentos values (NULL, ?, ?, ?, ?, ?)",
                       (inicio, fim, manual, False, False))
        id_agendamento = cursor.lastrowid
        if valvulas:
            cursor.executemany(
                "insert into componente_agendamento values (NULL, ?, NULL, ?)",
                [(valvula, id_agendamento) for valvula in valvulas])
        elif bombas:
            cursor.executemany(
                "insert into componente_agendamento values (NULL, NULL, ?, ?)",
                [(bomba, id_agendamento) for bomba in bombas])
        self.conexao.commit()

    def deletar_agendamento(self, id_agendamento):
        self.exec("delete from agendamentos where agendamento = ?",
                  id_agendamento)
        self.conexao.commit()

    def estados_lotes_pendentes(self):
        return self.exec("select linha, valvula, kc, lote, saldo, saldo_maximo,"
                         "ultima_atualizacao "
                         "from estado_sistema_pendente").fetchall()

    def definir_saldo_estado_sistema(self, linha, novo_saldo):
        epoch = int(datetime.now().timestamp())
        self.exec("update estado_sistema set saldo = ?, ultima_atualizacao = ? "
                  "where linha = ?", novo_saldo, epoch, linha)
        self.conexao.commit()

    def backup(self, caminho, restaurar=False):
        if restaurar:
            if not backup.restaurar(self.conexao, caminho):
                return "Erro restaurando backup, alterações não aplicadas."
        else:
            backup.backup(self.conexao, caminho)

    def dados_monitoramento(self, lote):
        return self.exec("select valvula, numero_linha, cap_armz_max, saldo "
                         "from dados_monitoramento "
                         "where lote = ?", lote).fetchall()
