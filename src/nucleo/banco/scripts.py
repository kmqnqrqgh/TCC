criar_db = """
PRAGMA user_version = 4;

CREATE TABLE bombas (
    nome TEXT PRIMARY KEY,
    numero_sim INTEGER,
    max_valvulas_simultaneas INTEGER,
    auto_irrigacao_inicio INTEGER NOT NULL,
    auto_irrigacao_fim INTEGER NOT NULL,
    delay_pressurizacao INTEGER NOT NULL
); 

CREATE TABLE talhoes (
    nome_talhao TEXT PRIMARY KEY
); 

CREATE TABLE valvulas (
    valvula INTEGER PRIMARY KEY,
    numero_sim INTEGER UNIQUE,
    
    bomba INTEGER REFERENCES bombas(nome) 
        ON DELETE SET NULL 
        ON UPDATE CASCADE,
    
    nome_talhao TEXT REFERENCES talhoes(nome_talhao) 
        ON DELETE SET NULL 
        ON UPDATE CASCADE
);

CREATE TABLE linhas (
    linha INTEGER PRIMARY KEY,
    valvula INTEGER REFERENCES valvulas(valvula)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    numero_local INTEGER,
    capacidade_armazenamento REAL NOT NULL,
    vazao REAL NOT NULL
);

CREATE TABLE plantas (
    nome_planta TEXT PRIMARY KEY
);

CREATE TABLE dados_plantas (
    id_dados INTEGER PRIMARY KEY,
    nome_planta TEXT NOT NULL REFERENCES plantas(nome_planta)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    fase INTEGER NOT NULL,
    kc REAL NOT NULL,
    profundidade_raiz REAL NOT NULL,
    proporcao_fase INTEGER NOT NULL,
    UNIQUE (nome_planta, fase)
);

CREATE TABLE estados_lote (
    estado TEXT PRIMARY KEY,
    usar_estado_sistema BOOL NOT NULL
);

CREATE TABLE lotes (
    lote TEXT PRIMARY KEY,
    nome_planta TEXT REFERENCES plantas(nome_planta)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    data_colheita INTEGER NOT NULL,
    estado TEXT NOT NULL REFERENCES estados_lote(estado)
);

CREATE TABLE linhas_lote (
    id_linhas_lote INTEGER PRIMARY KEY,
    linha INTEGER REFERENCES linhas(linha),
    lote TEXT REFERENCES lotes(lote)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE fases_lote (
    id_fases_lote INTEGER PRIMARY KEY,
    lote TEXT REFERENCES lotes(lote)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    fase INTEGER NOT NULL,
    data_mudanca INTEGER NOT NULL
);

-- dados dessa tabela são alterados constantemente. os lotes usados nessa tabela
-- devem sempre estar como "ativo", ou um subtestado com usar_estado_sistema
CREATE TABLE estado_sistema (
    linha INTEGER PRIMARY KEY REFERENCES linhas(linha)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    lote TEXT REFERENCES lotes(lote)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    saldo REAL NOT NULL,
    ultima_atualizacao INTEGER NOT NULL
);

CREATE TABLE niveis_usuarios(
    nivel TEXT PRIMARY KEY
);

CREATE TABLE usuarios (
    nome TEXT PRIMARY KEY,
    chave TEXT NOT NULL UNIQUE,
    nivel_acesso TEXT NOT NULL REFERENCES niveis_usuarios(nivel)
        ON UPDATE CASCADE
        ON DELETE SET NULL
);

CREATE TABLE metereologia (
    data INTEGER PRIMARY KEY,
    et_referencia REAL NOT NULL,
    chuva REAL NOT NULL
);

CREATE TABLE agendamentos (
    agendamento INTEGER PRIMARY KEY,
    data_hora_inicio INTEGER NOT NULL,
    data_hora_fim INTEGER NOT NULL,
    manual BOOLEAN NOT NULL,
    enviado BOOLEAN NOT NULL,
    confirmado BOOLEAN NOT NULL
);

CREATE TABLE componente_agendamento (
    id_componentes INTEGER PRIMARY KEY,
    valvula INTEGER REFERENCES valvulas(valvula)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    bomba INTEGER REFERENCES bombas(nome)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    agendamento INTEGER REFERENCES agendamentos(agendamento)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TABLE tipos_eventos (
    evento INTEGER PRIMARY KEY,
    descricao TEXT
);

CREATE TABLE historico (
    data INTEGER PRIMARY KEY,
    evento INTEGER REFERENCES tipos_eventos(evento),
    descricao TEXT
);

CREATE VIEW IF NOT EXISTS lotes_fase_atual AS
    SELECT lotes.lote as lote, MAX(fase) AS fase FROM fases_lote
        INNER JOIN lotes ON lotes.lote = fases_lote.lote 
    WHERE STRFTIME('%s', 'now') > data_mudanca
    GROUP BY lotes.lote
    UNION 
    SELECT lotes.lote as lote, NULL AS fase FROM fases_lote
        INNER JOIN lotes ON lotes.lote = fases_lote.lote 
    WHERE STRFTIME('%s', 'now') < data_mudanca and fases_lote.fase = 0;

CREATE VIEW IF NOT EXISTS dados_lotes AS
    SELECT fases_lote.lote, nome_talhao, nome_planta, data_mudanca, data_colheita, estado,
            lotes_fase_atual.fase as fase_atual, 
        COUNT(DISTINCT linhas.linha) AS total_linhas
    FROM lotes 
        INNER JOIN fases_lote ON fases_lote.lote = lotes.lote
        INNER JOIN linhas_lote ON linhas_lote.lote = lotes.lote
        INNER JOIN lotes_fase_atual ON lotes_fase_atual.lote = lotes.lote
        INNER JOIN linhas on linhas.linha = linhas_lote.linha
        INNER JOIN valvulas on valvulas.valvula = linhas.valvula
    WHERE fases_lote.fase = 0
        GROUP BY lotes.lote;

CREATE VIEW IF NOT EXISTS estado_sistema_pendente AS
    SELECT linhas.linha, linhas.valvula, dados_plantas.nome_planta, dados_plantas.kc, 
                lotes.lote as lote, saldo, 
                -- não calcular déficit! precisa ser usado o saldo atualizado
                dados_plantas.profundidade_raiz * linhas.capacidade_armazenamento AS saldo_maximo, 
                lotes_fase_atual.fase AS fase, 
                (select max(data) from metereologia 
                    where data < STRFTIME('%s', 'now')) as data_metereolgia, 
                estado_sistema.ultima_atualizacao
        FROM estado_sistema
            INNER JOIN lotes ON lotes.lote = estado_sistema.lote 
            INNER JOIN lotes_fase_atual ON lotes_fase_atual.lote = lotes.lote
            INNER JOIN dados_plantas ON dados_plantas.nome_planta = lotes.nome_planta
            INNER JOIN linhas on estado_sistema.linha = linhas.linha
            INNER JOIN estados_lote on estados_lote.estado = lotes.estado
        WHERE dados_plantas.fase = lotes_fase_atual.fase 
            AND estado_sistema.ultima_atualizacao < data_metereolgia
            AND estados_lote.usar_estado_sistema = 1;

CREATE VIEW IF NOT EXISTS dados_monitoramento AS
    select lotes.lote, valvula, numero_local as numero_linha, 
        capacidade_armazenamento * dados_plantas.profundidade_raiz as cap_armz_max, saldo
    from linhas 
    inner join estado_sistema on estado_sistema.linha = linhas.linha
    inner join lotes on lotes.lote = estado_sistema.lote
    inner join dados_plantas on dados_plantas.nome_planta = lotes.nome_planta
    inner join lotes_fase_atual on lotes_fase_atual.lote = lotes.lote
    where dados_plantas.fase = lotes_fase_atual.fase;

CREATE TABLE propriedades_internas (
    id PRIMARY KEY,
    criacao_ok BOOL NOT NULL,
    ultimo_acesso INTEGER NOT NULL
);

INSERT INTO propriedades_internas VALUES(0, 0, STRFTIME('%s', 'now'));

"""
