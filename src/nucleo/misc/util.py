import logging
import os
import tempfile

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
_raiz = os.path.expanduser("~")


def arquivo_config(arquivo, pasta="", criar_arquivo=False, conteudo_inicial='',
                   sempre_obter_arquivo=True):
    """
    Obtém arquivo na pasta de configurações

    :param arquivo: nome do arquivo
    :param pasta: nome da pasta, ex: ~/SisIrrig/temas/arquivo
    :param criar_arquivo: tenta criar arquivo destino se não existir
    :param conteudo_inicial: conteudo inicial ao criar arquivo
    :param sempre_obter_arquivo: obter nome de arquivo mesmo se não existir
    :return: caminho completo para o arquivo
    """
    # todo: mudar "projeto_tcc" pro nome do sistema
    caminho = os.path.join(_raiz, "projeto_tcc", pasta)
    config = os.path.join(caminho, arquivo)
    if not os.path.exists(caminho):
        try:
            os.makedirs(caminho, exist_ok=1)
        except Exception as e:
            logger.error(f"tentativa falha de criar pastas {caminho}:", e)
            return
    if criar_arquivo and not os.path.isfile(config):
        try:
            with open(config, 'w') as f:
                f.write(conteudo_inicial)
                return config
        except Exception as e:
            logger.error(f"tentativa falha ao criar conteudo para {config}:", e)
    elif sempre_obter_arquivo:
        return config
    return
