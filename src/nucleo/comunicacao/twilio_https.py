import http.client
import json
import logging
from urllib.parse import urlencode
import base64

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class TwilioHttps:
    timeout = 10000

    def __init__(self):
        self.__account_sid = "AC242df0f41f2047056e74a4da5b00a9bd"
        self.numero_conta = "+15619034535"

    @property
    def __auth_token(self):
        return base64.a85decode(b"1-&l)1,``T2.U/O0k!(R2)f#'1hL?20OZjn@5DLQ")

    @staticmethod
    def __conectar():
        try:
            return http.client.HTTPSConnection("api.twilio.com")
        except ConnectionError as er:
            erro = f"Erro de conexão: {er}"
            logger.error(erro)
            return erro

    def __requisicao(self, param):
        conn = self.__conectar()
        if isinstance(conn, str):
            # quando acontece um erro, conn é string
            return conn
        conn.timeout = self.timeout
        logger.debug(f"__requisicao({param})")
        sid = self.__account_sid
        auth_str = sid + ":" + self.__auth_token.decode()
        auth = base64.b64encode(auth_str.encode()).decode()
        # noinspection PyBroadException
        try:
            conn.request("POST",
                         f"/2010-04-01/Accounts/{sid}/Messages.json", param,
                         {"Content-type": "application/x-www-form-urlencoded",
                          "Authorization": f"Basic {auth}"})
            ret = conn.getresponse()
            conn.sock.close()
        except Exception:
            ret = "Erro ao conectar-se com o servidor"
            logger.error(ret)
        return ret

    @staticmethod
    def __lidar_com_erro(data):
        logger.error("Erro ao enviar mensagem. "
                     f"código={data['code']}, msg={data['message']}")
        return {
            21614: "Número recipiente inválido",
            21608: "Número recipiente não verificado",
            21606: "Número de envio inválido",
            21602: "Conteúdo da mensagem requerido",
            20003: "Dados de autenticação inválidos",
            21211: "Número de envio inválido",
            0: ""
        }.get(data["code"], f"Erro desconhecido: {data['code']}\n"
                            f"Mensagem: {data['message']}")

    def enviar_mensagem(self, numero, mensagem):
        """
        :param numero: numero do recipiente
        :param mensagem: conteúdo da mensagem
        :return: str com mensagem do erro se houver, caso contrario dict com
        dados de retorno do servidor
        """
        resp = self.__requisicao(urlencode({"Body": mensagem,
                                            "From": self.numero_conta,
                                            "To": numero}))
        if isinstance(resp, str):
            return resp
        data = json.loads(resp.read().decode())
        if "body" not in data and "code" in data:
            return self.__lidar_com_erro(data)
        return data
