import logging
from typing import List, Union, Tuple

from src.nucleo.comunicacao.twilio_https import TwilioHttps
from src.nucleo.banco.banco import Banco, NiveisAcesso
from src.nucleo.processamento.processamento import Processamento

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def validar_acesso(tabela, escrita=False):
    """
    wrapper de uso interno para verificar permissões de usuarios, se o usuario
    atual não tiver acesso, a função não é executada.
    """
    # criar um decorator dentro de uma classe é problematico, por isso fica aqui
    def decorator(func):
        def wrapper(*args, **kwargs):
            self = args[0]
            if not self.usuario:
                return
            if self.verificar_permissao_tabela(tabela, escrita):
                return func(*args, **kwargs)
            else:
                logger.debug(f"Acesso negado: {func}({str(args[1:])}")
                logger.warning("Acesso negado")
                return "Acesso negado"
        return wrapper
    return decorator


class Controle:
    def __init__(self):
        self.__banco = Banco()
        self.__usuario_atual = ""
        self.__logado = False
        self.__processamento = Processamento(self)

    def finalizar(self):
        self.__processamento.parar()

    @property
    def redefinicao_senha_admin_necessaria(self):
        return self.__banco.redefinicao_necessaria

    @property
    def nome_usuario_admin(self):
        return "admin"

    @property
    def usuario(self):
        return self.__usuario_atual

    def verificar_permissao_tabela(self, tabela, escrita):
        return self.__banco.verificar_permissao(self.__usuario_atual, tabela,
                                                escrita)

    def login(self, usuario: str, senha: str) -> Tuple[bool, str]:
        # noinspection PyBroadException
        try:
            resultado = self.__banco.verificar_credenciais(usuario, senha)
        except Exception:
            return False, "Erro interno"
        self.__usuario_atual = ""
        self.__logado = False
        if not resultado:
            return False, "Usuario ou senha inválidos"
        self.__logado = True
        self.__usuario_atual = usuario
        return True, ""

    def logout(self) -> None:
        self.__usuario_atual = ""

    def definir_senha_inicial(self, senha: str) -> Union[bool, None]:
        return self.__banco.definir_admin(senha)

    @property
    def comandos_para_valvulas(self):
        return {
            "Teste": "T",
            "Abrir": "A",
            "Fechar": "F"
        }

    @property
    def comandos_para_bombas(self):
        return {
            "Ligar": "1",
            "Desligar": "0"
        }

    # usuarios avançados podem enviar mensagens, mas normais não
    @validar_acesso(tabela=Banco.Tabelas.agendamentos, escrita=True)
    def enviar_mensagem(self, id_: int, comando: str, bomba: bool) -> \
            Union[str, None]:
        """
        tenta enviar um comando para o elemento especificado
        :param id_: identificador
        :param comando: string com a mensagem a ser enviada
        :return: msg de erro, se houver
        :param bomba: se é ou não uma bomba
        """
        if not bomba:
            id_ = int(id_)
            sim = [num_sim for id_valvula, num_sim, _, _
                   in self.listar_valvulas() if id_ == id_valvula]
        else:
            sim = [num_sim for bomba, num_sim, *_
                   in self.listar_bombas() if id_ == bomba]
        if not sim:
            return "Dados inválidos"
        resp = TwilioHttps().enviar_mensagem(
            sim[0], self.comandos_para_valvulas[comando])
        return resp if isinstance(resp, str) else None

    @validar_acesso(tabela=Banco.Tabelas.bombas, escrita=True)
    def cadastrar_bomba(self, bomba: str, numero_sim: int,
                        max_valvulas_simultaneas: int, hora_inicio: int,
                        hora_fim: int,
                        delay_pressurizacao: int) -> Union[str, None]:
        """
        :param bomba:
        :param numero_sim:
        :param max_valvulas_simultaneas: valvulas abertas ao mesmo tempo
        :param hora_inicio: horario inicial pro agendamento automático
        :param hora_fim: horario final pro agendamento automático
        :param delay_pressurizacao: atraso da pressurização do cano
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_bomba(bomba, numero_sim,
                                            max_valvulas_simultaneas,
                                            hora_inicio, hora_fim,
                                            delay_pressurizacao)

    @validar_acesso(tabela=Banco.Tabelas.bombas, escrita=True)
    def alterar_bomba(self, nome: str, novo_nome: str, novo_numero_sim: int,
                      max_valvulas_simultaneas: int, nova_hora_inicio: int,
                      nova_hora_fim: int, delay_pressurizacao: int) -> \
            Union[str, None]:
        """
        :param nome:
        :param novo_nome:
        :param novo_numero_sim:
        :param max_valvulas_simultaneas: valvulas abertas ao mesmo tempo
        :param nova_hora_inicio: horario inicial pro agendamento automático
        :param nova_hora_fim: horario final pro agendamento automático
        :param delay_pressurizacao: atraso da pressurização do cano
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_bomba(
            nome, novo_nome, novo_numero_sim, max_valvulas_simultaneas,
            nova_hora_inicio, nova_hora_fim, delay_pressurizacao)

    @validar_acesso(tabela=Banco.Tabelas.bombas, escrita=True)
    def deletar_bomba(self, nome: str) -> None:
        """
        :param nome:
        :return: None
        """
        return self.__banco.deletar_bomba(nome)

    @validar_acesso(tabela=Banco.Tabelas.bombas)
    def listar_bombas(self) -> List[Tuple[str, int, int, int, int, int]]:
        """
        :return: [(nome, numero_sim, max_valvulas_simultaneas,
        auto_irrigacao_hora_inicio, auto_irrigacao_hora_fim,
        delay_pressurizacao)]
        """
        return self.__banco.bombas()

    @validar_acesso(tabela=Banco.Tabelas.bombas)
    def dados_bomba(self, nome_bomba: str) -> List[Tuple[int, int, int, int,
                                                         int]]:
        """
        :param nome_bomba:
        :return: [(numero_sim, max_valvulas_simultaneas, auto_irrigacao_inicio,
        auto_irrigacao_fim, delay_pressurizacao)]
        """
        return self.__banco.bomba(nome_bomba)

    @validar_acesso(tabela=Banco.Tabelas.bombas)
    def dependencias_bomba(self, nome_bomba: str) -> int:
        """
        Retorna numero de valvulas que dependem dessa nome_bomba
        :param nome_bomba:
        :return: int
        """
        return self.__banco.dependencias_bomba(nome_bomba)

    @validar_acesso(tabela=Banco.Tabelas.talhoes, escrita=True)
    def cadastrar_talhao(self, nome_talhao: str) -> Union[str, None]:
        """
        :param nome_talhao:
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_talhao(nome_talhao)

    @validar_acesso(tabela=Banco.Tabelas.talhoes, escrita=True)
    def alterar_talhao(self, nome_talhao: str, novo_nome: str) -> Union[str,
                                                                        None]:
        """
        :param nome_talhao:
        :param novo_nome:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_talhao(nome_talhao, novo_nome)

    @validar_acesso(tabela=Banco.Tabelas.talhoes, escrita=True)
    def deletar_talhao(self, nome_talhao: str) -> None:
        """
        :param nome_talhao:
        :return: None
        """
        return self.__banco.deletar_talhao(nome_talhao)

    @validar_acesso(tabela=Banco.Tabelas.talhoes)
    def listar_talhoes(self) -> List[str]:
        """
        :return: [talhao]
        """
        return self.__banco.talhoes()

    @validar_acesso(tabela=Banco.Tabelas.talhoes)
    def dependencias_talhao(self, nome_talhao: str) -> int:
        """
        Retorna numero de valvulas associadas ao nome_talhao
        :param nome_talhao:
        :return: int
        """
        return self.__banco.dependencias_talhao(nome_talhao)

    @validar_acesso(tabela=Banco.Tabelas.valvulas, escrita=True)
    def cadastrar_valvula(self, numero_sim: int, nome_bomba: str,
                          nome_talhao: str) -> Union[str, None]:
        """
        :param numero_sim:
        :param nome_bomba:
        :param nome_talhao:
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_valvula(numero_sim, nome_bomba,
                                              nome_talhao)

    @validar_acesso(tabela=Banco.Tabelas.valvulas, escrita=True)
    def alterar_valvula(self, id_valvula: int, novo_sim: int, nova_bomba: str,
                        novo_talhao: str) -> Union[str, None]:
        """
        :param id_valvula:
        :param novo_sim:
        :param nova_bomba:
        :param novo_talhao:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_valvula(id_valvula, novo_sim, nova_bomba,
                                            novo_talhao)

    @validar_acesso(tabela=Banco.Tabelas.valvulas)
    def dependencias_valvula(self, id_valvula: int) -> int:
        """
        Retorn numero de linhas associadas a valvula id_valvula
        :param id_valvula:
        :return: int
        """
        return self.__banco.dependencias_valvula(id_valvula)

    @validar_acesso(tabela=Banco.Tabelas.valvulas, escrita=True)
    def deletar_valvula(self, id_valvula: int) -> None:
        """
        :param id_valvula:
        :return: None
        """
        return self.__banco.deletar_valvula(id_valvula)

    @validar_acesso(tabela=Banco.Tabelas.valvulas)
    def listar_valvulas(self) -> List[Tuple[int, int, str, str]]:
        """
        :return: lista de tupla: [(valvula, numero_sim, bomba, nome_talhao)]
        """
        return self.__banco.valvulas()

    @validar_acesso(tabela=Banco.Tabelas.valvulas)
    def dados_valvula(self, id_valvula: int) -> Tuple[int, int, str, str]:
        """
        :param id_valvula:
        :return: (valvula, numero_sim, bomba, nome_talhao)
        """
        return self.__banco.valvula(id_valvula)

    @validar_acesso(tabela=Banco.Tabelas.plantas, escrita=True)
    def cadastrar_planta(self, nome_planta: str) -> Union[str, None]:
        """
        :param nome_planta:
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_nome_planta(nome_planta)

    @validar_acesso(tabela=Banco.Tabelas.plantas, escrita=True)
    def alterar_nome_planta(self, nome_planta: str,
                            novo_nome: str) -> Union[str, None]:
        """
        :param nome_planta:
        :param novo_nome:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_nome_planta(nome_planta, novo_nome)

    @validar_acesso(tabela=Banco.Tabelas.plantas, escrita=True)
    def excluir_planta(self, nome_planta: str) -> None:
        """
        :param nome_planta:
        :return: None
        """
        return self.__banco.deletar_planta(nome_planta)

    @validar_acesso(tabela=Banco.Tabelas.plantas)
    def dependencias_planta(self, nome_planta: str) -> int:
        """
        Retorna numero de dependencias
        :param nome_planta:
        :return: (int)
        """
        return self.__banco.dependencias_planta(nome_planta)

    @validar_acesso(tabela=Banco.Tabelas.plantas)
    def listar_plantas(self) -> List[str]:
        """
        :return: [nome_planta]
        """
        return self.__banco.plantas()

    @validar_acesso(tabela=Banco.Tabelas.dados_plantas, escrita=True)
    def cadastrar_dados_planta(self, nome_planta: str, fase: int, kc: float,
                               profundidade: float,
                               proporcao_fase: float) -> Union[str, None]:
        """
        :param nome_planta:
        :param fase:
        :param kc:
        :param profundidade:
        :param proporcao_fase:
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_dados_planta(nome_planta, fase, kc,
                                                   profundidade, proporcao_fase)

    @validar_acesso(tabela=Banco.Tabelas.dados_plantas, escrita=True)
    def cadastrar_dados_plantas(self, dados: [(str, int, int, float,
                                               int)]) -> Union[str, None]:
        """
        :param dados: lista de tupla: [(nome_planta, fase, kc, profundidade,
                                        proporcao_fase)]
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_dados_plantas(dados)

    @validar_acesso(tabela=Banco.Tabelas.dados_plantas, escrita=True)
    def alterar_dados_planta(self, nome_planta: str, fase: int, kc: float,
                             profundidade: float,
                             proporcao_fase: float) -> Union[str, None]:
        """
        :param proporcao_fase:
        :param nome_planta:
        :param fase:
        :param kc:
        :param profundidade:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_dados_planta(nome_planta, fase, kc,
                                                 profundidade, proporcao_fase)

    @validar_acesso(tabela=Banco.Tabelas.dados_plantas, escrita=True)
    def alterar_dados_plantas(self, dados: List[Tuple[str, int, float, int,
                                                float]]) -> Union[str, None]:
        """
        :param dados:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_dados_plantas(dados)

    @validar_acesso(tabela=Banco.Tabelas.dados_plantas, escrita=True)
    def excluir_dados_planta(self, nome_planta, fase) -> None:
        """
        :param nome_planta:
        :param fase:
        :return: None
        """
        return self.__banco.deletar_dados_planta(nome_planta, fase)

    @validar_acesso(tabela=Banco.Tabelas.dados_plantas)
    def listar_dados_plantas(self) -> List[Tuple[str, int, float, float,
                                                 float]]:
        """
        :return: [(nome_planta, fase, kc, profundidade_raiz, proporcao_fase)]
        """
        return self.__banco.dados_plantas()

    @validar_acesso(tabela=Banco.Tabelas.usuarios, escrita=True)
    def cadastrar_usuario_operador(self, usuario: str,
                                   senha: str) -> Union[str, None]:
        """
        :param usuario:
        :param senha:
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_usuario(usuario, senha,
                                              NiveisAcesso.operador)

    @validar_acesso(tabela=Banco.Tabelas.usuarios, escrita=True)
    def cadastrar_usuario_operador_especial(self, usuario: str,
                                            senha: str) -> Union[str, None]:
        """
        :param usuario:
        :param senha:
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_usuario(usuario, senha,
                                              NiveisAcesso.operador_especial)

    # usuarios comuns podem alterar a propria senha, porisso permissão de
    # leitura para sempre permitir o acesso dessa função
    @validar_acesso(tabela=Banco.Tabelas.usuarios)
    def alterar_senha_usuario_atual(self, nova_senha: str,
                                    antiga_senha: str) -> Union[str, None]:
        """
        :param nova_senha:
        :param antiga_senha:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_usuario(self.__usuario_atual, nova_senha,
                                            antiga_senha)

    # operador=False = operador especial
    @validar_acesso(tabela=Banco.Tabelas.usuarios, escrita=True)
    def alterar_nivel_acesso(self, usuario: str,
                             operador: bool=True) -> Union[str, None]:
        """
        :param usuario:
        :param operador:
        :return: msg de erro, se houver
        """
        nivel = NiveisAcesso.operador if operador \
            else NiveisAcesso.operador_especial
        return self.__banco.alterar_nivel_usuario(usuario, nivel)

    # somente o admin pode fazer isso
    @validar_acesso(tabela=Banco.Tabelas.usuarios, escrita=True)
    def redefinir_senha(self, usuario: str, nova_senha: str) -> Union[str,
                                                                      None]:
        """
        :param usuario:
        :param nova_senha:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_usuario(usuario, nova_senha)

    @validar_acesso(tabela=Banco.Tabelas.usuarios, escrita=True)
    def excluir_usuario(self, usuario: str) -> Union[str, None]:
        """
        :param usuario:
        :return: msg de erro, se houver
        """
        return self.__banco.deletar_usuario(usuario)

    @validar_acesso(tabela=Banco.Tabelas.usuarios)
    def listar_usuarios(self) -> List[Tuple[str, str]]:
        """
        :return: [(nome, nivel_acesso)]
        """
        return self.__banco.usuarios()

    @validar_acesso(tabela=Banco.Tabelas.linhas, escrita=True)
    def cadastrar_linha(self, id_valvula: int, vazao: float, cap_armz: float,
                        num_local: int) -> Union[str, None]:
        """
        :param id_valvula: valvula associada à linha
        :param vazao: vazao do bico
        :param cap_armz: capacidade de armazenamento
        :param num_local: numero local
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_linha(id_valvula, vazao, cap_armz,
                                            num_local)

    @validar_acesso(tabela=Banco.Tabelas.linhas, escrita=True)
    def alterar_linha(self, linha: int, valvula: int, vazao: float,
                      cap_armz: float, num_local: int) -> Union[str, None]:
        """
        :param linha: id da linha a ser alterada
        :param valvula: valvula associada à linha
        :param vazao: vazao do bico
        :param cap_armz: capacidade de armazenamento
        :param num_local: numero local
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_linha(linha, valvula, vazao, cap_armz,
                                          num_local)

    @validar_acesso(tabela=Banco.Tabelas.linhas, escrita=True)
    def excluir_linha(self, linha: int) -> None:
        """
        :param linha:
        :return: None
        """
        self.__banco.deletar_linha(linha)

    @validar_acesso(tabela=Banco.Tabelas.linhas)
    def listar_linhas(self) -> List[Tuple[int, int, str, str, int, float,
                                          float]]:
        """
        :return: lista de tupla: [(linha, valvula, nome_talhao, bomba,
        numero_local, capacidade_armazenamento, vazao)]
        """
        return self.__banco.linhas()

    @validar_acesso(tabela=Banco.Tabelas.lotes, escrita=True)
    def cadastrar_lote(self, nome_lote: str, planta: str, linhas: List[int],
                       datas_transicoes: List[int],
                       data_colheita: int, estado: str) -> Union[str, None]:
        """
        :param nome_lote: id do lote
        :param planta: nome da planta do lote
        :param linhas: lista com ids das linhas
        :param datas_transicoes:
        :param data_colheita:
        :param estado:
        :return: msg de erro, se houver
        """
        return self.__banco.cadastrar_lote(nome_lote, planta, linhas,
                                           datas_transicoes, data_colheita,
                                           estado)

    @validar_acesso(tabela=Banco.Tabelas.lotes)
    def listar_lotes(self) -> List[Tuple[str, str, int, int, str, int, int]]:
        """
        :return: lista de tupla: [(lote, planta, data_plantio, data_colheita,
        estado_atual, fase_atual, total_linhas)]
        """
        return self.__banco.lotes()

    @validar_acesso(tabela=Banco.Tabelas.lotes)
    def dados_lote(self, lote) -> Tuple[str, str, int, int, str, int, int]:
        """
        :return: tupla: (talhao, planta, data_plantio, data_colheita,
        estado_atual, fase_atual, total_linhas)
        """
        return self.__banco.lote(lote)

    @validar_acesso(tabela=Banco.Tabelas.lotes, escrita=True)
    def alterar_lote(self, lote: str, nova_planta: str, novas_linhas: List[int],
                     novas_datas_transicoes: List[int], nova_data_colheita: int,
                     novo_estado: str) -> Union[str, None]:
        """
        :param lote: id do lote
        :param nova_planta: nome da planta
        :param novas_linhas:
        :param novas_datas_transicoes:
        :param nova_data_colheita:
        :param novo_estado:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_lote(lote, nova_planta, novas_linhas,
                                         novas_datas_transicoes,
                                         nova_data_colheita, novo_estado)

    @validar_acesso(tabela=Banco.Tabelas.lotes)
    def linhas_lote(self, lote: str) -> List[int]:
        """
        :param lote: id do lote
        :return: lista de linhas associadas ao lote
        """
        return self.__banco.linhas_lote(lote)

    @validar_acesso(tabela=Banco.Tabelas.lotes)
    def fases_lote(self, lote: str) -> List[Tuple[int, int]]:
        """
        retorna datas das fases de um lote
        :param lote:
        :return: [(fase, data_mudanca)]
        """
        return self.__banco.fases_lote(lote)

    @validar_acesso(tabela=Banco.Tabelas.lotes)
    def listar_estados_lote(self, ) -> List[str]:
        """
        retorna lista de estados dos lotes (ativo, finalizado, cancelado)
        :return: [estado_lote]
        """
        return self.__banco.estados_lote()

    @validar_acesso(tabela=Banco.Tabelas.lotes, escrita=True)
    def excluir_lote(self, lote):
        self.__banco.deletar_lote(lote)

    @validar_acesso(tabela=Banco.Tabelas.metereologia)
    def listar_metereologia(self) -> List[Tuple[int, float, float]]:
        """
        :return: [(data, et_referencia, chuva)]
        """
        return self.__banco.metereologia()

    @validar_acesso(tabela=Banco.Tabelas.metereologia, escrita=True)
    def adicionar_metereologia(self, data: int, et_referencia: float,
                               chuva: float) -> Union[str, None]:
        """
        :param data: unix epoch
        :param et_referencia: evapotranspiração referencia em mm
        :param chuva: milímetros de chuva
        :return: msg de erro, se houver
        """
        return self.__banco.add_metereologia(data, et_referencia, chuva)

    @validar_acesso(tabela=Banco.Tabelas.metereologia, escrita=True)
    def alterar_metereologia(self, data: int, nova_et_referencia: float,
                             nova_chuva: float) -> Union[str, None]:
        """
        :param data:
        :param nova_et_referencia:
        :param nova_chuva:
        :return: msg de erro, se houver
        """
        return self.__banco.alterar_metereologia(data, nova_et_referencia,
                                                 nova_chuva)

    @validar_acesso(tabela=Banco.Tabelas.agendamentos)
    def listar_agendamentos(self) -> List[Tuple[int, int, str, int, int, bool,
                                                bool]]:
        """
        nota: mais de 1 elemento deve retornar com a mesma id_agendamento por
        conta de várias válvulas serem ativadas no mesmo agendamento

        :return: [(id_agendamento, valvula, bomba, hora_inicio, hora_fim,
                  agendamento_manual, confirmado)]
        """
        return self.__banco.agendamentos()

    @validar_acesso(tabela=Banco.Tabelas.agendamentos, escrita=True)
    def adicionar_agendamento(self, inicio: int, fim: int, valvulas: List[int],
                              bombas: List[str]) -> Union[str, None]:
        """
        nota: válvulas e bombas não devem ser adicionados simultaneamente
        :param inicio: data e hora de inicio
        :param fim: data e hora do término
        :param valvulas:
        :param bombas:
        :return: msg de erro, se houver
        """
        return self.__banco.add_agendamento(inicio, fim, True, valvulas, bombas)

    @validar_acesso(tabela=Banco.Tabelas.agendamentos, escrita=True)
    def excluir_agendamento(self, id_agendamento: int) -> None:
        """
        :param id_agendamento:
        :return: None
        """
        self.__banco.deletar_agendamento(id_agendamento)

    @validar_acesso(tabela=Banco.Tabelas.todas, escrita=True)
    def salvar_backup(self, caminho_arquivo) -> None:
        """
        :param caminho_arquivo: caminho onde o arquivo de backup será salvo
        :return: None
        """
        self.__banco.backup(caminho_arquivo)

    @validar_acesso(tabela=Banco.Tabelas.todas, escrita=True)
    def restaurar_backup(self, caminho_arquivo) -> None:
        """
        :param caminho_arquivo: caminho do arquivo a ser carregado
        :return:
        """
        self.__banco.backup(caminho_arquivo, True)

    @validar_acesso(tabela=Banco.Tabelas.todas)
    def dados_monitoramento(self, lote) -> List[Tuple[int, int, float, float]]:
        """
        :param lote: lote a ser exibido os dados de monitoramento
        :return: lista de tupla:[(valvula, numero_linha, cap_armz, saldo)]
        """
        return self.__banco.dados_monitoramento(lote)
