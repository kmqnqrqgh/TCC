import base64
import hashlib

import bcrypt
from Cryptodome import Random
from Cryptodome.Cipher import AES


class AESCipher(object):
    def __init__(self, key):
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return iv + cipher.encrypt(raw.encode())

    def decrypt(self, enc):
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode()

    def encrypt_bytes(self, raw_bytes):
        string = base64.b64encode(raw_bytes).decode()
        return self.encrypt(string)

    def decode_bytes(self, enc):
        return base64.b64decode(self.decrypt(enc))

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) \
               * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


class Senhas:
    """
    Gera chaves com salt para o armazenamento seguro de senhas
    https://en.wikipedia.org/wiki/Password#Form_of_stored_passwords
    """
    def __init__(self, senha):
        self.__chave = bcrypt.hashpw(senha.encode(), bcrypt.gensalt(12))

    @property
    def chave(self):
        return self.__chave

    @staticmethod
    def verificar(chave, senha):
        if bcrypt.checkpw(senha.encode(), chave):
            return True
