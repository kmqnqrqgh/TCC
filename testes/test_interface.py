import tempfile
import unittest
import os
import time

from PyQt5.QtWidgets import QApplication

from src.interface.cadastros.add_bomba import AddBomba
from src.interface.cadastros.add_talhao import AddTalhao
from src.interface.cadastros.add_valvula import AddValvula
from src.interface.alteracoes.alterar_bomba import AlterarBomba
from src.interface.alteracoes.alterar_talhao import AlterarTalhao
from src.interface.alteracoes.alterar_valvula import AlterarValvula
from src.interface.bombas import Bombas
from src.interface.janela_principal import Principal
from src.interface.login import Login
from src.interface.primeiro_uso import PrimeiroUso
from src.interface.talhoes import Talhoes
from src.interface.ui_util import qtime
from src.interface.valvulas import Valvulas
from src.nucleo.banco.banco import Banco
from src.nucleo.controle import Controle
from src.nucleo.misc import util
from src.interface import ui_util


class TesteInterface(unittest.TestCase):
    senha = "1234"
    talhao_nome_orig = "t1"
    talhao_nome_alt = "t1"

    bomba_nome_orig = "b1"
    bomba_nome_alt = "b2"
    bomba_max_valv_orig = 5
    bomba_max_valv_alt = 4
    bomba_auto_inicio_orig = 79300
    bomba_auto_inicio_alt = 79500
    bomba_auto_fim_orig = 14400
    bomba_auto_fim_alt = 14800
    bomba_delay_orig = 610
    bomba_delay_alt = 620
    bomba_num_sim_orig = "1111"
    bomba_num_sim_alt = "2222"

    valv_num_sim_orig = "3333"
    valv_num_sim_alt = "4444"

    @classmethod
    def tearDownClass(cls):
        # remover arquivo do banco de testes
        os.remove(util.arquivo_config(Banco.nome_arquivo))

    @classmethod
    def setUpClass(cls):
        # pegar caminho da pasta de arquivos temporarios do sistema
        util._raiz = tempfile.gettempdir()
        ui_util._testando = True
        Banco.nome_arquivo = "teste_db"
        try:
            os.remove(util.arquivo_config(Banco.nome_arquivo))
        except FileNotFoundError:
            pass
        # iniciar interface
        cls.app = QApplication([])
        cls.controle = Controle()

        # parar processamento em segundo plano
        cls.controle.finalizar()

        cls.janela_login = Login(cls.controle)
        cls.janela_primeiro_uso = PrimeiroUso(cls.controle)

        # definir senha da janela de primeiro uso, como se fosse o usuário
        cls.janela_primeiro_uso.txtSenha.setText(cls.senha)
        cls.janela_primeiro_uso.txtSenha_2.setText(cls.senha)
        cls.janela_primeiro_uso.btnDefinir.click()

        cls.login_ok = False

        def confirmar_login():
            cls.login_ok = True

        cls.janela_login.txtUsuario.setText("admin")
        cls.janela_login.txtSenha.setText(cls.senha)
        cls.janela_login.login_ok.connect(confirmar_login)

        # fazer login como se fosse o usuário
        cls.janela_login.btnEntrar.click()

        # esperar evento ser processado pela thread do Qt
        time.sleep(0.1)

        if not cls.login_ok:
            raise Exception("login com erro")

        # instanciar janelas pra usar nas funções de cada teste
        cls.janela_principal = Principal(cls.controle)
        cls.janela_bombas = Bombas(None, cls.controle)
        cls.janela_add_bomba = AddBomba(None, cls.controle)
        cls.janela_talhoes = Talhoes(None, cls.controle)
        cls.janela_add_talhao = AddTalhao(None, cls.controle)
        cls.janela_valvulas = Valvulas(None, cls.controle)
        cls.janela_add_valvula = AddValvula(None, cls.controle)

    # 1° teste: testar cadastramento de bomba
    def teste_0_0_cadastrar_bomba(self):
        print("cadastrando bomba", end='')
        self.janela_add_bomba.txtNome.setText(self.bomba_nome_orig)
        self.janela_add_bomba.txtNumeroSIM.setText(self.bomba_num_sim_orig)
        self.janela_add_bomba.spMaxValvulas.setValue(self.bomba_max_valv_orig)

        self.janela_add_bomba.teDelay.setTime(qtime(self.bomba_delay_orig))
        self.janela_add_bomba.teInicio.setTime(qtime(self.bomba_auto_inicio_orig))
        self.janela_add_bomba.teFim.setTime(qtime(self.bomba_auto_fim_orig))

        self.janela_add_bomba.btnConfirmar.click()
        self.janela_bombas.atualizar()
        self.assertGreater(self.janela_bombas.tbBombas.model().rowCount(), 0)
        print("..ok")

    # 2° teste: alterar bomba
    def teste_0_1_alterar_bomba(self):
        print("alterando bomba", end='')
        janela_alt_bomba = AlterarBomba(None, self.controle,
                                        self.bomba_nome_orig)
        janela_alt_bomba.txtNome.setText(self.bomba_nome_alt)
        janela_alt_bomba.txtNumeroSIM.setText(self.bomba_num_sim_alt)
        janela_alt_bomba.teDelay.setTime(qtime(self.bomba_delay_alt))
        janela_alt_bomba.teInicio.setTime(qtime(self.bomba_auto_inicio_alt))
        janela_alt_bomba.teFim.setTime(qtime(self.bomba_auto_fim_alt))
        janela_alt_bomba.btnConfirmar.click()
        self.janela_bombas.atualizar()

        # pegar dados da janela é mais facil do que da lista
        janela_alt_bomba = AlterarBomba(None, self.controle,
                                        self.bomba_nome_alt)

        delay = janela_alt_bomba.teDelay.time().msecsSinceStartOfDay() / 1000
        inicio = janela_alt_bomba.teInicio.time().msecsSinceStartOfDay() / 1000
        fim = janela_alt_bomba.teFim.time().msecsSinceStartOfDay() / 1000

        lst1 = [int(delay), int(inicio), int(fim)]
        lst2 = [self.bomba_delay_alt, self.bomba_auto_inicio_alt,
                self.bomba_auto_fim_alt]

        self.assertListEqual(lst1, lst2)
        print("..ok")

    # 3° teste
    def teste_1_0_cadastrar_talhao(self):
        print("cadastrando talhao", end='')
        self.janela_add_talhao.txtNomeTalhao.setText(self.talhao_nome_orig)
        self.janela_add_talhao.btnConfirmar.click()
        self.janela_talhoes.atualizar()
        self.assertGreater(self.janela_talhoes.lstTalhoes.count(), 0)
        print("..ok")

    # 4° teste
    def teste_1_1_alterar_talhao(self):
        print("alterando talhao", end='')
        janela_alt_talhao = AlterarTalhao(None, self.controle,
                                          self.talhao_nome_orig)
        janela_alt_talhao.txtNomeTalhao.setText(self.talhao_nome_alt)
        janela_alt_talhao.btnConfirmar.click()
        self.assertGreater(self.janela_talhoes.lstTalhoes.count(), 0)
        print("..ok")

    # 5° teste
    def teste_2_0_cadastrar_valvula(self):
        print("cadastrando valvula", end='')
        self.janela_add_valvula = AddValvula(None, self.controle)
        self.janela_add_valvula.txtNumeroSIM.setText(self.valv_num_sim_orig)
        self.janela_add_valvula.cbTalhao.setCurrentText(self.talhao_nome_alt)
        self.janela_add_valvula.cbBomba.setCurrentIndex(0)
        self.janela_add_valvula.btnConfirmar.click()
        self.janela_valvulas.atualizar()
        self.assertGreater(
            self.janela_valvulas.tbValvulas.model().rowCount(), 0)
        print("..ok")

    # 6º teste
    def teste_2_1_alterar_valvula(self):
        print("alterando valvula", end='')
        id_valvula = int(self.janela_valvulas.tbValvulas.model().item(0).text())
        janela_alt_valvula = AlterarValvula(None, self.controle, id_valvula)
        janela_alt_valvula.txtNumeroSIM.setText(self.valv_num_sim_alt)
        janela_alt_valvula.btnConfirmar.click()
        print("..ok")
    # todo: escrever restante dos testes, um teste pra cada funcionalidade
