import unittest

from src.nucleo.criptografia import cripto


class TesteCriptografia(unittest.TestCase):
    frase = "teste 12345"
    senha = "*****"

    def test_criptografar(self):
        # essa criptografia não é usada de verdade, só existe aqui caso venha
        # ser util no futuro, talvez criptografar o backup antes de salvar

        print("testando criptografia")
        cifrador = cripto.AESCipher(self.senha)

        # criptografar frase
        encriptado = cifrador.encrypt(self.frase)
        print("frase:", self.frase,
              "\nencriptado:", encriptado)

        # descriptografar frase
        decriptado = cifrador.decrypt(encriptado)

        # certificar que frase descriptografada é igual a fase criptografada
        self.assertEqual(decriptado, self.frase)

    def test_senhas(self):
        print("testando chave de senha")

        # criar chave usando a senha
        chave = cripto.Senhas(self.senha).chave
        print("chave:", chave)

        # certificar que o método verifica a senha e funciona
        self.assertTrue(cripto.Senhas.verificar(chave, self.senha))
