import unittest
import json

from src.nucleo.comunicacao.twilio_https import TwilioHttps


class TesteTwilioHttps(unittest.TestCase):
    recipiente = "+5515_________"  # colocar seu número SIM para testar
    mensagem = "teste"

    # testa envio de um sms, só testa o funcionamento de envio
    # pra confirmar o recebimento, é preciso verificar manualmente
    def test_enviar_msg(self):
        print("enviando sms")
        twilio = TwilioHttps()
        print()

        # enviar mensagem
        resp = twilio.enviar_mensagem(self.recipiente, self.mensagem)

        # mostrar resposta do servidor
        print(json.dumps(resp, indent=2, ensure_ascii=False))
        if isinstance(resp, dict):
            self.assertTrue(True)
        else:
            self.fail(resp)
