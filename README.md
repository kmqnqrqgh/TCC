# TCC

Gerenciador do sistema de irrigação

### Como executar
#### No windows:
##### Com o Pycharm:
1. Baixe e instale o [git](https://git-scm.com/download/win)
2. Instale o [Pycharm](https://www.jetbrains.com/pycharm/download/download-thanks.html?platform=windows&code=PCC)
3. Selecione __Check out from Version Control__
4. No campo URL escreva: https://gitlab.com/rsaeqxzp2dj/TCC
5. Coloque o login e senha para acessar o repositório, e clique na opção para salvar a senha.
6. Após o repositório ser clonado, clique na parte inferior direita onde está escrito "Git: master", e selecione "origin/dev -> Checkout As", dê o nome do branch como "dev" e clique em OK.
7. Em seguida vá ao menu: **File > Settings > Project: TCC > Project Interpreter**. 
8. Pressione o botão **+** no canto direito da janela para instalar as dependências do projeto.
9. Selecione e instale os pacotes: **PyQt5**, **pycryptodomex** e **bcrypt**
10. Clique em OK e aguarde a atualização dos índices no pycharm, pode demorar.
11. Execute o script **atualizar.bat**, isso deverá criar o arquivo **qrc.py** na pasta _src\interface_
12. Selecione o arquivo **executar.py** com o botão direito e clique em "**run executar.py**"
##### Sem o Pycharm:
- Baixe e instale o [git](https://git-scm.com/download/win)
- Abra o **cmd** e execute o comando para baixar a ultima versão do código: `git clone https://gitlab.com/rsaeqxzp2dj/TCC` ou acesse [este link](https://gitlab.com/rsaeqxzp2dj/TCC/tree/dev) e clique para baixar em arquivo _zip_ (a versão mais atual é sempre no branch **dev**)
- Se tiver usado o comando `git clone` será necessário alterar o *branch* atual para **dev**, usando o comando: `git fetch origin` seguido de `git branch dev`
- Instale o [Python 3.6](https://www.python.org/ftp/python/3.6.6/python-3.6.6-amd64.exe)
- Instale as dependências do projeto usando o _cmd_: `pip3 install pycryptodomex PyQt5 bcrypt`
- Entre na pasta onde o código foi clonado e execute pelo _cmd_: `python executar.py`
  > se na linha de comando **python** não funcionar, tente **python3**